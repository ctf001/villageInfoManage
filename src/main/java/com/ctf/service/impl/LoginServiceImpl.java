package com.ctf.service.impl;

import com.ctf.bean.User;
import com.ctf.dao.impl.UserDaoImpl;
import com.ctf.service.LoginService;

import java.sql.SQLException;

public class LoginServiceImpl implements LoginService {

    private UserDaoImpl userDao = new UserDaoImpl();

    @Override
    public User login(String username, String password) throws SQLException {
        return userDao.queryAUser(username,password);
    }

}
