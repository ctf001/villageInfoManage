package com.ctf.service.impl;

import com.ctf.Exception.FileOperateException;
import com.ctf.bean.baseinfo.*;
import com.ctf.dao.impl.*;
import com.ctf.service.BaseInfoService;
import com.ctf.utils.FileUtils;
import com.ctf.utils.PropertiedUtils;
import com.ctf.utils.ZipUtils;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.*;

public class BaseInfoServiceImpl  implements BaseInfoService{
    //所用到的dao------------------------------------------------------------------//
    private TownDaoImpl townDao  = new TownDaoImpl();
    private VillageDaoImpl villageDao  = new VillageDaoImpl();
    private UCPVillageDaoImpl ucpVillageDao  = new UCPVillageDaoImpl();
    private UnionFamilyDaoImpl unionFamilyDao  = new UnionFamilyDaoImpl();
    private FamilyDaoImpl familyDao  = new FamilyDaoImpl();

    /*全局公共方法*/
    private static String changePhotoSrc(String originSrc) throws IOException, ClassNotFoundException {
        String newSrc = "/image" + originSrc;
        return newSrc;
    }

    //处理乡镇基本信息相关业务------------------------------------------------------------------//
    @Override
    /*
     * @Description ：新增一个乡镇addATown(Town newTownInfo)
     * @Param newTownInfo:  新增的乡镇信息
     * @Return ：int  新增操作的状态码
     *  1:Add success.新增成功
     *  -1:town is null,乡镇对象为空
     *  -2:town's info have some errors,乡镇信息存在错误
     * @Author: CTF
     * @Date ：2022/3/7 18:46
     */
    public int addATown(Town newTownInfo) throws SQLException {
        //判空
        if(newTownInfo==null){
            return -1;
        }
        //执行新增操作
        return townDao.addATown(newTownInfo);
    }

    @Override
    /*
     * @Description ：根据乡镇id删除一个乡镇deleteATownByTownId(TownId)
     * @Param townID: 乡镇id
     * @Return ：int  删除乡镇信息操作的状态
     * @Author: CTF
     * @Date ：2022/3/7 18:46
     */
    public int deleteATownByTownId(int townID) throws SQLException {
        return townDao.deleteATownByTownId(townID);
    }

    @Override
    /*
     * @Description ：修改乡镇信息updateTownInfo（Town newTownInfo）
     * @Param newTownInfo: 需要更新的乡镇信息
     * @Return ：int 更新乡镇信息操作的状态
     * @Author: CTF
     * @Date ：2022/3/7 18:06
     */
    public int updateTownInfo(Town newTownInfo) throws SQLException {
        //判空
        if(newTownInfo==null){
            return -1;
        }
        //执行新增操作
        int code = townDao.updateTownInfo(newTownInfo);
        return code;
    }

    @Override
    /*
     * @Description ：查询所有乡镇信息queryAllTownInfo(PageNo,PageSize)，注意导出功能配置
     * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
     * @Param pageSize: 如果不为null，则需要分页，此项表示分页时的数据量
     * @Return ：List<Town> 查询返回的Town的list集合
     * @Author: CTF
     * @Date ：2022/3/7 18:04
     */
    public List<Town> queryAllTownInfo(Integer pageNo, Integer pageSize) throws SQLException {
        return townDao.queryAllTownInfo(pageNo,pageSize);
    }

    @Override
    /*
     * @Description ：根据条件查询乡镇信息querySomeTownInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
     * @Param queryParams: 查询条件map
     * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
     * @Param pageSize: 如果不为null，则需要分页，此项表示分页时的数据量
     * @Return ：List<Town> 查询返回的Town的list集合
     * @Author: CTF
     * @Date ：2022/3/7 18:34
     */
    public List<Town> querySomeTownInfo(Map<String, String[]> queryParams, Integer pageNo, Integer pageSize) throws SQLException {
        return townDao.querySomeTownInfo(queryParams,pageNo,pageSize);
    }

    @Override
    /*
     * @Description ：根据乡镇编号查询乡镇信息queryTownInfoByTownId(TownId)，注意导出功能配置
     * @Param townID: 乡镇编号
     * @Return ：Town 查询出的town对象
     * @Author: CTF
     * @Date ：2022/3/7 18:35
     */
    public Town queryTownInfoByTownId(int townID) throws SQLException {
        return townDao.queryTownInfoByTownId(townID);
    }


    //处理村基本信息相关业务------------------------------------------------------------------//
    @Override
    /*
     * @Description ：新增一个村庄addAVillage(VillageInfo)
     * @Param village: 新增的乡镇信息
     * @Return ：int 新增操作的状态码
     * @Author: CTF
     * @Date ：2022/3/15 20:19
     */
    public int addAVillage(Village village) throws SQLException {
        //判空
        if(village==null){
            return -1;
        }
        //执行新增操作
        int statusCode = villageDao.addAVillage(village);
        return statusCode;
    }

    @Override
    /*
     * @Description ：删除一个村庄deleteAVillageByVillageId(VillageId)
     * @Param VillageId: 需要删除的村庄编号
     * @Return ：int 删除操作的状态码
     * @Author: CTF
     * @Date ：2022/3/15 20:20
     */
    public int deleteAVillageByVillageId(int villageId) throws SQLException {
        return villageDao.deleteAVillageByID(villageId);
    }

    @Override
    /*
     * @Description ：修改村庄信息updateVillageInfo（newVillage）
     * @Param newVillage: 需要更新的村庄信息，封装为Village对象(Village newVillage)
     * @Return ：int 更新操作的状态码
     * @Author: CTF
     * @Date ：2022/3/15 20:20
     */
    public int updateVillageInfo(Village newVillage) throws SQLException {
        //判空
        if(newVillage==null){
            return -1;
        }
        int statusCode = villageDao.updateVillageInfo(newVillage);
        return statusCode;
    }

    @Override
    /*
     * @Description ：查询所有村庄信息queryAllVillageInfo(PageNo,PageSize)，注意导出功能配置
     * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
     * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
     * @Return ：List<Village> 查询出的村庄List
     * @Author: CTF
     * @Date ：2022/3/15 20:20
     */
    public List<HashMap<String,Object>> queryAllVillageInfo(Integer pageNo, Integer pageSize) throws SQLException {
        List<Village> villages = villageDao.queryAllVillage(pageNo, pageSize);
        return getResultMapFromList_village_town(villages);
    }

    @Override
    /*
     * @Description ： 根据条件查询村庄信息querySomeVillageInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
     * @Param queryParams: 查询条件map
     * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
     * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
     * @Return ：List<Village> 查询出的村庄List
     * @Author: CTF
     * @Date ：2022/3/15 20:20
     */
    public List<HashMap<String,Object>> querySomeVillageInfo(Map<String, String[]> queryParams, Integer pageNo, Integer PageSize) throws SQLException {
        List<Village> villages = villageDao.querySomeVillages(queryParams, pageNo, PageSize);
        return getResultMapFromList_village_town(villages);
    }

    @Override
    /*
     * @Description ：根据村庄编号查询村庄信息queryVillageInfoByVillageId(VillageId)，注意导出功能配置
     * @Param VillageId: 村庄编号
     * @Return ：Village 查询出的村庄village对象
     * @Author: CTF
     * @Date ：2022/3/15 20:20
     */
    public Village queryVillageInfoByVillageId(int VillageId) throws SQLException {
        Village village = villageDao.queryVillageInfoByID(VillageId);
        return village;
    }

    /*
     * @Description ：将village对象和town对象合封装成一个map，返回前端
     * @Param village:  原始数据village对象
     * @Return ：HashMap<String,Object>
     * @Author: CTF
     * @Date ：2022/3/15 21:32
     */
    private List<HashMap<String,Object>> getResultMapFromList_village_town(List<Village> villageList) throws SQLException {
        //前端结果处理
        List<HashMap<String,Object>> hashMapList = new ArrayList<>();
        for(Village village:villageList){
            //获取附加信息对象
            Town town =
                    new TownDaoImpl()
                            .queryTownInfoByTownId(village.getTown_id());
            //封装回显数据
            HashMap<String,Object> hashMap = new HashMap<>();
            hashMap.put("village_id", village.getVillage_id());
            hashMap.put("village_name", village.getVillage_name());
            if(town!=null){
                hashMap.put("town", town.getTown_name());
                hashMap.put("town_id", town.getTown_id());
            }else {
                hashMap.put("town",  "该乡镇不存在，请检查基础信息");
                hashMap.put("town_id", "该乡镇不存在，请检查基础信息");
            }
            hashMap.put("apart_from_town", village.getApart_from_town());
            hashMap.put("plough_area", village.getPlough_area());
            hashMap.put("elevation", village.getElevation());
            hashMap.put("meadow_area", village.getMeadow_area());
            hashMapList.add(hashMap);
        }
        return hashMapList;
    }

    //处理自然村基本信息相关业务------------------------------------------------------------------//
    // 新增一个自然村 addAnUCPVillage(UCPVillageInfo)
    // 删除一个自然村 deleteAnUCPVillageByUCPVillageId(UCPVillageId)
    // 修改自然村信息 updateUCPVillageInfo(UCPVillageInfo)
    // 查询所有自然村信息 queryAllUCPVillageInfo(PageNo,PageSize)，注意导出功能配置
    // 根据条件查询自然村信息 querySomeUCPVillageInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    // 根据自然村编号查询自然村信息 queryUCPVillageInfoByUCPVillageId(UCPVillageId)，注意导出功能配置
    // 根据自然村编号为自然村绑定图片bindUCPVillagePhotoByID(photoSrc,UCPVillageId)
    // 根据自然村编号查询自然村图片并打包压缩，返回压缩文件存放路径ZipUCPVillagePhotoByUCPVillageId(UCPVillageId)
    // 根据自然村编号与需要删除的图片编号删除该图片deleteUCPVillagePhotoByPhotoId(UCPVillageId,PhotoId)

    @Override
    /*
     * @Description ：新增一个自然村
     * @Param UCPVillageInfo: 新增的自然村信息，封装为UCPVillage对象(UCPVillage UCPVillageInfo)
     * @Return ：HashMap<String,Object> 由于需要将新增后的自然村UCPVillage对象返回给前端，以便执行图片上传工作，
     *                                  所以采用map形式，既可以包含UCPVillage对象，也可以包含操作状态码
     * @Author: CTF
     * @Date ：2022/3/18 22:10
     */
    public int addAnUCPVillage(UCPVillage UCPVillageInfo) throws SQLException, IOException, ClassNotFoundException {
        //判空
        if(UCPVillageInfo==null){
            return -1;
        }
        //执行新增操作
        //为该自然村创建图片文件夹
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindowsUcpVillage = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindowsUcpVillage",
                String.class);

        String photoDirPath = fileSaveDocPathWindows + photoSaveDocPathWindows + photoSaveDocPathWindowsUcpVillage
                + File.separator + UCPVillageInfo.getUcp_village_id();

        //检验需要创建的文件夹是否存在
        File photoDirectory = new File(photoDirPath);
        if(!photoDirectory.exists()){
            //文件夹不存在，创建文件夹
            photoDirectory.mkdirs();
        }

        return ucpVillageDao.addAnUCPVillage(UCPVillageInfo);
    };

    @Override
    /*
     * @Description ：删除一个自然村
     * @Param UCPVillageId:  需要删除的自然村编号
     * @Return ：int 删除操作的状态码
     * @Author: CTF
     * @Date ：2022/3/18 14:46
     */
    public int deleteAnUCPVillageByUCPVillageId(int UCPVillageId) throws IOException, ClassNotFoundException, SQLException, FileOperateException {
        /*删除该自然村的所有图片信息*/
        //根据该自然村的编号获取其图片文件夹路径
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindowsUcpVillage = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindowsUcpVillage",
                String.class);
        String path = fileSaveDocPathWindows + photoSaveDocPathWindows + photoSaveDocPathWindowsUcpVillage
                + File.separator + UCPVillageId;
        //删除文件夹
        boolean flag = FileUtils.deleteIfExists(path);

        int delete_ucpvillagephoto_Code = 0;
        int delete_ucpvillagebaseinfo_Code = 0;
        if(flag){
            //图片文件已经删除
            /*删除数据库中的图片信息*/
            delete_ucpvillagephoto_Code = ucpVillageDao.deleteUCPVillagePhotoByUCPVillageID(UCPVillageId);
            /*删除数据库中的基础信息*/
            delete_ucpvillagebaseinfo_Code = ucpVillageDao.deleteAnUCPVillageByID(UCPVillageId);
        }else {
            throw new FileOperateException("图片文件删除失败");
        }

        return delete_ucpvillagephoto_Code + delete_ucpvillagebaseinfo_Code;
    }

    @Override
    /*
     * @Description ：修改自然村信息
     * @Param UCPVillageInfo: 需要更新的村庄信息，封装为UCPVillage对象(UCPVillage UCPVillageInfo)
     * @Return ：int 更新操作的状态码
     * @Author: CTF
     * @Date ：2022/3/18 14:46
     */
    public int updateUCPVillageInfo(UCPVillage UCPVillageInfo) throws SQLException {
        //判空
        if(UCPVillageInfo==null){
            return -1;
        }
        int statusCode = ucpVillageDao.updateUCPVillageInfo(UCPVillageInfo);
        if(statusCode != 1){
            return -2;
        }
        return statusCode;
    }

    @Override
    /*
     * @Description ：查询所有自然村信息，注意导出功能配置
     * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
     * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
     * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含封装为UCPVillage对象，可能还包含其他对象，
     *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
     * @Author: CTF
     * @Date ：2022/3/18 14:42
     */
    public List<HashMap<String,Object>> queryAllUCPVillageInfo(Integer pageNo, Integer PageSize) throws SQLException, IOException, ClassNotFoundException {
        List<UCPVillage> ucpVillages = ucpVillageDao.queryAllUCPVillage(pageNo, PageSize);
        return getResultMapFromList_ucpvillage_village(ucpVillages);
    }

    @Override
    /*
     * @Description ：根据条件查询自然村信息，注意导出功能配置
     * @Param queryParams: 查询条件map
     * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
     * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
     * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含封装为UCPVillage对象，可能还包含其他对象，
     *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
     * @Author: CTF
     * @Date ：2022/3/18 14:45
     */
    public List<HashMap<String,Object>> querySomeUCPVillageInfo(Map<String, String[]> queryParams,Integer pageNo,Integer PageSize) throws SQLException, IOException, ClassNotFoundException {
        List<UCPVillage> ucpVillages = ucpVillageDao.querySomeUCPVillages(queryParams, pageNo, PageSize);
        return getResultMapFromList_ucpvillage_village(ucpVillages);
    }

    @Override
    /*
     * @Description ：根据自然村编号查询自然村信息，注意导出功能配置
     * @Param UCPVillageId: 自然村编号
     * @Return ：UCPVillage 查询出的自然村UCPVillage对象
     * @Author: CTF
     * @Date ：2022/3/18 14:47
     */
    public HashMap<String,Object> queryUCPVillageInfoByUCPVillageId(int UCPVillageId) throws SQLException, IOException, ClassNotFoundException {
        UCPVillage ucpVillage = ucpVillageDao.queryUCPVillageByUCPVillageID(UCPVillageId);
        return getResultMapFromBean_ucpvillage_village(ucpVillage);
    }

    /*
     * @Description ：根据自然村编号为自然村绑定图片
     * @Param photoSrc: 上传的图片存放路径
     * @Param UCPVillageId: 需要绑定的自然村编号
     * @Return ：int 操作码
     * @Author: CTF
     * @Date ：2022/3/18 22:06
     */
    public int bindUCPVillagePhotoByID(String photoSrc,int UCPVillageId) throws SQLException {
        return ucpVillageDao.bindUCPVillagePhotoByID(photoSrc,UCPVillageId);
    }

    @Override
     /*
      * @Description ：根据自然村编号查询自然村图片
      * @Param UCPVillageId: 需要查询图片信息的自然村编号
      * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
      * @Author: CTF
      * @Date ：2022/3/22 11:36
      */
    public List<HashMap<String,Object>> queryUCPVillagePhotoByUCPVillageId(int UCPVillageId) throws SQLException {
        //获取附加信息对象
        UCPVillage ucpVillage =
                ucpVillageDao.queryUCPVillageByUCPVillageID(UCPVillageId);

        List<UCPVillagePhoto> ucpVillagePhotos
                = ucpVillageDao.queryUCPVillagePhotoListByUCPVillageID(UCPVillageId);

        List<HashMap<String,Object>> hashMapList = new ArrayList<>();

        for(UCPVillagePhoto ucpVillagePhoto : ucpVillagePhotos){
            //重命名图片显示文件名
            String ucpVillage_picture_src = ucpVillagePhoto.getUCPVillage_picture_src();
            int separatorIndex = ucpVillage_picture_src.lastIndexOf(File.separator);
            int dotIndex = ucpVillage_picture_src.lastIndexOf(".");
            String photo_name = ucpVillage_picture_src.substring(separatorIndex+1,dotIndex);

            //封装回显数据
            HashMap<String,Object> hashMap = new HashMap<>();
            hashMap.put("ucp_village_id", ucpVillage.getUcp_village_id());
            hashMap.put("photo_name", photo_name);
            hashMap.put("ucp_village_name",ucpVillage.getUcp_village_name());
            hashMap.put("apart_from_village",ucpVillage.getApart_from_village());
            hashMap.put("plough_area", ucpVillage.getPlough_area());
            hashMap.put("elevation", ucpVillage.getElevation());
            hashMap.put("meadow_area", ucpVillage.getMeadow_area());

            hashMap.put("photoID",ucpVillagePhoto.getPhotoID());
            hashMap.put("UCPVillage_picture_src", ucpVillagePhoto.getUCPVillage_picture_src());

            hashMapList.add(hashMap);
        }

        return hashMapList;
    }

    @Override
    /*
     * @Description ：根据自然村编号查询自然村图片并打包压缩
     * @Param UCPVillageId: 自然村编号
     * @Return ：String 获取到相关图片打包压缩后的压缩文件存放地址
     * @Author: CTF
     * @Date ：2022/3/23 21:04
     */
    public String ZipUCPVillagePhotoByUCPVillageId(int UCPVillageId) throws SQLException, IOException, ClassNotFoundException {
        //获取自然村信息
        UCPVillage ucpVillage = ucpVillageDao.queryUCPVillageByUCPVillageID(UCPVillageId);

        //读取本系统文件存放的路径
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        //读取所有图片存放的路径
        String picture_path = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindows",
                String.class);
        //读取自然村图片存放的路径
        String ucpvillage_picture_path = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindowsUcpVillage",
                String.class);
        //定义本次需要下载的自然村的图片所在的文件夹的地址
        String realFilePath =fileSaveDocPathWindows +
                picture_path +
                ucpvillage_picture_path + File.separator + UCPVillageId;

        //获取临时文件存放的路径
        String tempFileSavePath = PropertiedUtils.getValue("system.properties",
                "tempFileSavePath",
                String.class);
        //获取临时压缩文件存放的路径
        String tempFileSavePathZIP = PropertiedUtils.getValue("system.properties",
                "tempFileSavePathZIP",
                String.class);
        //合成压缩文件临时存放的绝对路径
        String tempFileSaveRealPath = fileSaveDocPathWindows + tempFileSavePath + tempFileSavePathZIP;
        //生成压缩文件的文件名
        String zipName = tempFileSaveRealPath + File.separator +
                "自然村-"+ucpVillage.getUcp_village_name() + "-图片"
                + ".zip";

        //检查压缩文件临时存放地址是否存在
        File file = new File(tempFileSaveRealPath);
        if(!file.exists()){
            //不存在，则创建文件夹
            file.mkdirs();
        }

        //执行压缩程序
        ZipUtils zipUtils = new ZipUtils(zipName);
        zipUtils.compressExe(realFilePath);

        boolean flag = false;
        if(new File(zipName).exists()){
            //压缩成功，已检测到指定地址存在
            flag = true;
        }

        if(flag){
            return zipName;
        }

        return null;
    }

    @Override
    /*
     * @Description ：根据自然村编号与需要删除的图片编号删除该图片
     * @Param UCPVillageId: 自然村编号
     * @Param PhotoId: 需要删除的图片编号
     * @Return ：int 删除操作的状态码
     * @Author: CTF
     * @Date ：2022/3/26 11:02
     */
    public int deleteUCPVillagePhotoByUCPVillageIdAndPhotoId(int UCPVillageId, int PhotoId) throws SQLException, IOException, ClassNotFoundException, FileOperateException {
        /*删除该自然村的指定图片编号所对应的文件信息*/
        //获取自然村图片对象
        UCPVillagePhoto ucpVillagePhoto
                = ucpVillageDao.queryUCPVillagePhotoByUCPVillageIdAndPhotoId(UCPVillageId, PhotoId);
        //获取需要删除的图片路径
        String photoPath = ucpVillagePhoto.getUCPVillage_picture_src();
        //获取系统文件存放根路径
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        //合成文件真正路径
        String realPath = fileSaveDocPathWindows + photoPath;
        //删除文件
        boolean flag = FileUtils.deleteIfExists(realPath);

        int delete_photo_Code = 0;
        if(flag){
            //图片文件已经删除
            /*删除数据库中的图片信息*/
            delete_photo_Code = ucpVillageDao.deleteUCPVillagePhotoByUCPVillageIdAndPhotoId(UCPVillageId,PhotoId);
        }else {
            throw new FileOperateException("图片文件删除失败");
        }

        return delete_photo_Code;
    }

    @Override
    /*
     * @Description ：根据行政村id查询所有该行政村下所有自然村的信息
     * @Param village_id: 行政村idList<HashMap<String,Object>> 符合前端显示格式的由map组成的listHashMap<String,Object> 符合条件的自然村对象UCPVillage对象
     * @Author: CTF
     * @Date ：2022/3/30 17:40
     */
    public List<HashMap<String, Object>> queryUCPVillageByVillageID(int village_id) throws SQLException, IOException, ClassNotFoundException {
        List<UCPVillage> ucpVillages = ucpVillageDao.queryUCPVillageByVillageID(village_id);
        return getResultMapFromList_ucpvillage_village(ucpVillages);
    }

    /*
      * @Description ：将ucpvillage对象和village对象合封装成一个map，并添加到list，返回前端
      * @Param ucpVillageList: 原始数据ucpvillage对象的list
      * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
      * @Author: CTF
      * @Date ：2022/3/18 17:28
      */
    private List<HashMap<String,Object>> getResultMapFromList_ucpvillage_village(List<UCPVillage> ucpVillageList) throws SQLException, IOException, ClassNotFoundException {
        //前端结果处理
        List<HashMap<String,Object>> hashMapList = new ArrayList<>();
        for(UCPVillage ucpVillage:ucpVillageList){
            HashMap<String, Object> resultMapFromBean_ucpvillage_village
                    = getResultMapFromBean_ucpvillage_village(ucpVillage);
            hashMapList.add(resultMapFromBean_ucpvillage_village);

        }
        return hashMapList;
    }

     /*
      * @Description ：将ucpvillage对象和village对象合封装成一个map，返回前端
      * @Param ucpVillage: 原始数据ucpvillage对象
      * @Return ：HashMap<String,Object> 符合前端显示格式的map
      * @Author: CTF
      * @Date ：2022/3/18 18:05
      */
    private HashMap<String,Object> getResultMapFromBean_ucpvillage_village(UCPVillage ucpVillage) throws SQLException, IOException, ClassNotFoundException {

        //获取附加信息对象
        Village village = new VillageDaoImpl()
                .queryVillageInfoByID(ucpVillage.getVillage_id());

        List<UCPVillagePhoto> ucpVillagePhotos
                = new UCPVillageDaoImpl().queryUCPVillagePhotoListByUCPVillageID(ucpVillage.getUcp_village_id());

        String ucp_village_name = ucpVillage.getUcp_village_name();

        //封装回显数据
        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("ucp_village_id", ucpVillage.getUcp_village_id());
        hashMap.put("ucp_village_name",ucp_village_name);
        hashMap.put("apart_from_village",ucpVillage.getApart_from_village());
        if(village!=null){
            hashMap.put("village", village.getVillage_name());
            hashMap.put("village_id", village.getVillage_id());
        }else {
            hashMap.put("village", "该村庄不存在，请检查基础信息");
            hashMap.put("village_id", "该村庄不存在，请检查基础信息");
        }
        hashMap.put("plough_area", ucpVillage.getPlough_area());
        hashMap.put("elevation", ucpVillage.getElevation());
        hashMap.put("meadow_area", ucpVillage.getMeadow_area());
        hashMap.put("UCPVillage_picture", getPhotoMapFromUCPVillagePhotoList(ucpVillagePhotos,ucp_village_name));

        return hashMap;
    }

    /*
      * @Description ：将图像的src包装为符合前端需求的json格式
      * @Param photoSrcList: 需要包装的图片src的list
      * @Return ：String 包装好的json
      * @Author: CTF
      * @Date ：2022/3/18 17:29
      */
    private static HashMap<String,Object> getPhotoMapFromUCPVillagePhotoList(
            List<UCPVillagePhoto> UCPVillagePhotoList,String album_title) throws IOException, ClassNotFoundException {
          /*
            格式
            {
                "title": "", //相册标题
                "id": 123, //相册id
                "start": 0, //初始显示的图片序号，默认0
                "data": [   //相册包含的图片，数组格式
                    {
                      "alt": "图片名",
                      "pid": 666, //图片id
                      "src": "", //原图地址
                      "thumb": "" //缩略图地址
                    }
                    ]
            }
          */

        List<HashMap<String,Object>> dataMapList = new ArrayList<>();
        int index = 0;
        for(UCPVillagePhoto ucpVillagePhoto : UCPVillagePhotoList){
            index++;
            //重命名图片显示文件名
            String ucpVillage_picture_src = ucpVillagePhoto.getUCPVillage_picture_src();
            int separatorIndex = ucpVillage_picture_src.lastIndexOf(File.separator);
            int dotIndex = ucpVillage_picture_src.lastIndexOf(".");
            String fileName = "#" +index + "-" +
                    ucpVillage_picture_src.substring(separatorIndex+1,dotIndex);
            //生成前端显示要求的map
            HashMap<String,Object> dataMap = new HashMap<>();
            dataMap.put("alt",fileName);
            dataMap.put("pid",index);
            dataMap.put("src",changePhotoSrc(ucpVillagePhoto.getUCPVillage_picture_src()));
            dataMap.put("thumb",changePhotoSrc(ucpVillagePhoto.getUCPVillage_picture_src()) );
            //添加到list中
            dataMapList.add(dataMap);
        }

        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("title",album_title);
        hashMap.put("id",album_title+new Date().getTime());
        hashMap.put("start",0);
        hashMap.put("data",dataMapList);

        return hashMap;
    }

    //处理双联户基本信息相关业务------------------------------------------------------------------//
    //新增一个双联户 addAnUnionFamily(UnionFamilyInfo)
    //根据双联户编号删除一个双联户 deleteAnUnionFamilyByUnionFamilyID(UnionFamilyID)
    //修改双联户基础信息 updateUnionFamilyBaseInfo(UnionFamilyInfo)
    //查询所有双联户信息 queryAllUnionFamilyInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询双联户信息 querySomeUnionFamilyInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据双联户编号查询双联户信息 queryUnionFamilyInfoByUnionFamilyID(UnionFamilyID)，注意导出功能配置
    //根据双联户编号为双联户绑定图片 bindUnionFamilyPhotoByUnionFamilyID(photoSrc,UnionFamilyID)
    //根据双联户编号查询双联户图片 queryUnionFamilyPhotoByUnionFamilyID(UnionFamilyID)
    //根据双联户编号查询双联户图片并打包压缩，返回压缩文件存放路径 ZipUnionFamilyPhotoByUnionFamilyID(UnionFamilyID)
    //根据双联户编号与需要删除的图片编号删除该图片 deleteUnionFamilyPhotoByUnionFamilyIDAndPhotoId(UnionFamilyID,PhotoId)
    //根据行政村id查询所有该行政村下所有双联户的信息queryUnionFamilesByVillageID(village_id)

    @Override
    /*
     * @Description ：新增一个双联户
     * @Param UnionFamilyInfo: 新增的双联户信息，封装为UnionFamily对象(UnionFamily UnionFamilyInfo)
     * @Return ：int新增操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
     * @Author: CTF
     * @Date ：2022/3/27 16:48
     */
    public int addAnUnionFamily(UnionFamily UnionFamilyInfo) throws SQLException, IOException, ClassNotFoundException{
        //判空
        if(UnionFamilyInfo==null){
            return -1;
        }
        //执行新增操作
        //为该双联户创建图片文件夹
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindowsUnionFamilyLeader = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindowsUnionFamilyLeader", String.class);

        String photoDirPath = fileSaveDocPathWindows + photoSaveDocPathWindows + photoSaveDocPathWindowsUnionFamilyLeader
                + File.separator + UnionFamilyInfo.getUnionfamily_id();

        //检验需要创建的文件夹是否存在
        File photoDirectory = new File(photoDirPath);
        if(!photoDirectory.exists()){
            //文件夹不存在，创建文件夹
            photoDirectory.mkdirs();
        }
        return unionFamilyDao.addAnUnionFamily(UnionFamilyInfo);
    }

    @Override
    /*
     * @Description ：根据双联户编号删除一个双联户
     * @Param UnionFamilyID: 需要删除的双联户编号
     * @Return ：int删除操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
     * @Author: CTF
     * @Date ：2022/3/27 16:48
     */
    public int deleteAnUnionFamilyByUnionFamilyID(String UnionFamilyID) throws Exception{
        /*删除该自然村的所有图片信息*/
        //根据该自然村的编号获取其图片文件夹路径
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindowsUnionFamilyLeader = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindowsUnionFamilyLeader",
                String.class);

        String path = fileSaveDocPathWindows + photoSaveDocPathWindows + photoSaveDocPathWindowsUnionFamilyLeader
                + File.separator + UnionFamilyID;

        //删除文件夹
        boolean flag = FileUtils.deleteIfExists(path);

        int delete_unionfamilyphoto_Code = 0;
        int delete_unionfamilybaseinfo_Code = 0;
        if(flag){
            //图片文件已经删除
            /*删除数据库中的图片信息*/
            delete_unionfamilyphoto_Code = unionFamilyDao.deleteUnionFamilyLeaderPhotoByUnionFamilyID(UnionFamilyID);
            /*删除数据库中的基础信息*/
            delete_unionfamilybaseinfo_Code = unionFamilyDao.deleteAnUnionFamilyByUnionFamilyID(UnionFamilyID);
        }else {
            throw new FileOperateException("图片文件删除失败");
        }

        return delete_unionfamilyphoto_Code + delete_unionfamilybaseinfo_Code;
    }

    @Override
    /*
     * @Description ：修改双联户基础信息
     * @Param UnionFamilyInfo: 需要更新的双联户信息，封装为UnionFamilye对象(UnionFamily UnionFamilyInfo)
     * @Return ：int 更新操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
     * @Author: CTF
     * @Date ：2022/3/27 16:49
     */
    public int updateUnionFamilyBaseInfo(UnionFamily UnionFamilyInfo) throws SQLException{
        //判空
        if(UnionFamilyInfo==null){
            return -1;
        }
        int statusCode = unionFamilyDao.updateUnionFamilyBaseinfo(UnionFamilyInfo);
        if(statusCode != 1){
            return -2;
        }
        return statusCode;
    }

    @Override
    /*
     * @Description ：查询所有双联户信息，注意导出功能配置
     * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
     * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
     * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含UnionFamily对象，可能还包含其他对象，
     *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
     * @Author: CTF
     * @Date ：2022/3/27 16:50
     */
    public List<HashMap<String,Object>> queryAllUnionFamilyInfo(Integer pageNo, Integer PageSize) throws SQLException, IOException, ClassNotFoundException{
        List<UnionFamily> unionFamilies = unionFamilyDao.queryAllUnionFamily(pageNo, PageSize);
        return getResultMapFromList_unionfamily_village(unionFamilies);
    }

    @Override
    /*
     * @Description ：根据条件查询双联户信息，注意导出功能配置
     * @Param queryParams: 查询条件map
     * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
     * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
     * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含UnionFamily对象，可能还包含其他对象，
     *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
     * @Author: CTF
     * @Date ：2022/3/27 16:50
     */
    public List<HashMap<String,Object>> querySomeUnionFamilyInfo(Map<String, String[]> queryParams,Integer pageNo,Integer PageSize) throws SQLException, IOException, ClassNotFoundException{
        List<UnionFamily> unionFamilies = unionFamilyDao.querySomeUnionFamiles(queryParams,pageNo, PageSize);
        return getResultMapFromList_unionfamily_village(unionFamilies);
    }

    @Override
    /*
     * @Description ：根据双联户编号查询双联户信息，注意导出功能配置
     * @Param UnionFamilyID:双联户编号
     * @Return ：HashMap<String,Object>将符合条件的UnionFamily对象封装成map
     * @Author: CTF
     * @Date ：2022/3/27 16:51
     */
    public HashMap<String,Object> queryUnionFamilyInfoByUnionFamilyID(String UnionFamilyID) throws SQLException, IOException, ClassNotFoundException{
        UnionFamily unionFamily = unionFamilyDao.queryAnUnionFamilyByID(UnionFamilyID);
        return getResultMapFromBean_unionfamily_village(unionFamily);
    }

    @Override
    /*
     * @Description ：根据双联户编号为双联户绑定图片
     * @Param photoSrc: 上传的图片存放路径
     * @Param UnionFamilyID: 需要绑定的双联户编号
     * @Return ：int 操作码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
     * @Author: CTF
     * @Date ：2022/3/27 16:51
     */
    public int bindUnionFamilyLeaderPhotoByUnionFamilyID(String photoSrc,String UnionFamilyID) throws SQLException{
        return unionFamilyDao.bindUnionFamilyLeaderPhotoByUnionFamilyID(photoSrc,UnionFamilyID);
    }

    @Override
    /*
     * @Description ：根据双联户编号查询双联户户长图片
     * @Param UnionFamilyID: 需要查询图片信息的双联户编号
     * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
     * @Author: CTF
     * @Date ：2022/3/27 16:52
     */
    public List<HashMap<String,Object>> queryUnionFamilyLeaderPhotoByUnionFamilyID(String UnionFamilyID) throws SQLException{
        List<UnionFamilyLeaderPhoto> unionFamilyLeaderPhotos
                = unionFamilyDao.queryUnionFamilyLeaderPhotoListByUnionFamilyID(UnionFamilyID);

        //获取附加信息对象
        UnionFamily unionFamily
                = unionFamilyDao.queryAnUnionFamilyByID(UnionFamilyID);

        List<HashMap<String,Object>> hashMapList = new ArrayList<>();
        for(UnionFamilyLeaderPhoto unionFamilyLeaderPhoto : unionFamilyLeaderPhotos){
            //重命名图片显示文件名
            String unionfamily_leader_photo_src = unionFamilyLeaderPhoto.getUNIONFAMILY_photo_leader_src();
            int separatorIndex = unionfamily_leader_photo_src.lastIndexOf(File.separator);
            int dotIndex = unionfamily_leader_photo_src.lastIndexOf(".");
            String photo_name = unionfamily_leader_photo_src.substring(separatorIndex+1,dotIndex);

            //封装回显数据
            HashMap<String,Object> hashMap = new HashMap<>();
            hashMap.put("photo_name", photo_name);
            hashMap.put("photoID",unionFamilyLeaderPhoto.getPhotoID());
            hashMap.put("unionfamily_leader_photo_src", unionfamily_leader_photo_src);
            hashMap.put("unionfamily_id", unionFamily.getUnionfamily_id());
            hashMap.put("unionfamily_leader_name", unionFamily.getUf_leader_name());

            hashMapList.add(hashMap);
        }

        return hashMapList;
    }

    @Override
    /*
     * @Description ：根据双联户编号查询双联户户长图片并打包压缩
     * @Param UnionFamilyID: 双联户编号
     * @Return ：String 获取到相关图片打包压缩后的压缩文件存放地址
     * @Author: CTF
     * @Date ：2022/3/27 16:53
     */
    public String ZipUnionFamilyLeaderPhotoByUnionFamilyID(String UnionFamilyID) throws SQLException, IOException, ClassNotFoundException{
        //获取双联户信息
        UnionFamily unionFamily
                = unionFamilyDao.queryAnUnionFamilyByID(UnionFamilyID);

        //读取本系统文件存放的路径
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        //读取所有图片存放的路径
        String picture_path = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindows",
                String.class);
        //读取自然村图片存放的路径
        String photoSaveDocPathWindowsUnionFamilyLeader = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindowsUnionFamilyLeader",
                String.class);
        //定义本次需要下载的自然村的图片所在的文件夹的地址
        String realFilePath =fileSaveDocPathWindows +
                picture_path +
                photoSaveDocPathWindowsUnionFamilyLeader + File.separator + UnionFamilyID;

        //获取临时文件存放的路径
        String tempFileSavePath = PropertiedUtils.getValue("system.properties",
                "tempFileSavePath",
                String.class);
        //获取临时压缩文件存放的路径
        String tempFileSavePathZIP = PropertiedUtils.getValue("system.properties",
                "tempFileSavePathZIP",
                String.class);
        //合成压缩文件临时存放的绝对路径
        String tempFileSaveRealPath = fileSaveDocPathWindows + tempFileSavePath + tempFileSavePathZIP;

        //生成压缩文件的文件名
        String zipName = tempFileSaveRealPath + File.separator +
                "双联户-"+unionFamily.getUnionfamily_id() + "-图片"
                + ".zip";

        //检查压缩文件临时存放地址是否存在
        File file = new File(tempFileSaveRealPath);
        if(!file.exists()){
            //不存在，则创建文件夹
            file.mkdirs();
        }

        //执行压缩程序
        ZipUtils zipUtils = new ZipUtils(zipName);
        zipUtils.compressExe(realFilePath);

        boolean flag = false;
        if(new File(zipName).exists()){
            //压缩成功，已检测到指定地址存在
            flag = true;
        }

        if(flag){
            return zipName;
        }

        return null;
    }

    @Override
    /*
     * @Description ： 根据双联户编号与需要删除的图片编号删除该图片
     * @Param UnionFamilyID:  双联户编号
     * @Param PhotoId: 需要删除的图片编号
     * @Return ：int 删除操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
     * @Author: CTF
     * @Date ：2022/3/27 16:53
     */
    public int deleteUnionFamilyLeaderPhotoByUnionFamilyIDAndPhotoId(String UnionFamilyID,int PhotoId) throws SQLException, IOException, ClassNotFoundException, FileOperateException{
        /*删除该双联户的指定图片编号所对应的文件信息*/
        //根据图片编号获取双联户图片对象
        UnionFamilyLeaderPhoto unionFamilyLeaderPhoto
                = unionFamilyDao.queryUnionFamilyLeaderPhotoByUnionFamilyIdAndPhotoId(UnionFamilyID, PhotoId);
        //获取需要删除的图片路径
        String photoPath = unionFamilyLeaderPhoto.getUNIONFAMILY_photo_leader_src();
        //获取系统文件存放根路径
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        //合成文件真正路径
        String realPath = fileSaveDocPathWindows + photoPath;
        //删除文件
        boolean flag = FileUtils.deleteIfExists(realPath);

        int delete_photo_Code = 0;
        if(flag){
            //图片文件已经删除
            /*删除数据库中的图片信息*/
            delete_photo_Code = unionFamilyDao.deleteUnionFamilyLeaderPhotoByUnionFamilyIdAndPhotoId(UnionFamilyID,PhotoId);
        }else {
            throw new FileOperateException("图片文件删除失败");
        }

        return delete_photo_Code;
    }

    @Override
    /*
     * @Description ：根据行政村id查询所有该行政村下所有自然村的信息
     * @Param village_id: 行政村id
     * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
     * @Author: CTF
     * @Date ：2022/3/30 17:40
     */
    public List<HashMap<String, Object>> queryUnionFamilesByVillageID(int village_id) throws SQLException, IOException, ClassNotFoundException {
        List<UnionFamily> unionFamilies
                = unionFamilyDao.queryUnionFamilesByVillageID(village_id);
        return getResultMapFromList_unionfamily_village(unionFamilies);
    }

    /*
      * @Description ：将UnionFamily对象与其他其中包含的附属信息相关对象封装成一个map，并添加到list，返回前端
      * @Param unionFamilyList: 原始数据UnionFamily对象的list
      * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
      * @Author: CTF
      * @Date ：2022/3/30 16:31
      */
    private List<HashMap<String,Object>> getResultMapFromList_unionfamily_village
    (List<UnionFamily> unionFamilyList) throws SQLException, IOException, ClassNotFoundException {
        //前端结果处理
        List<HashMap<String,Object>> hashMapList = new ArrayList<>();

        for(UnionFamily unionFamily : unionFamilyList){

            HashMap<String, Object> resultMapFromBean_unionfamily_village
                    = getResultMapFromBean_unionfamily_village(unionFamily);

            hashMapList.add(resultMapFromBean_unionfamily_village);

        }

        return hashMapList;
    }

    /*
     * @Description ：将UnionFamily对象与其他其中包含的附属信息相关对象封装成一个map，返回前端
     * @Param family: 原始数据UnionFamily对象
     * @Return ：HashMap<String,Object> 符合前端显示格式的map
     * @Author: CTF
     * @Date ：2022/3/30 16:27
     */
    private HashMap<String,Object> getResultMapFromBean_unionfamily_village
    (UnionFamily unionFamily) throws SQLException, IOException, ClassNotFoundException {

        //获取附加信息对象
        Village village = new VillageDaoImpl()
                .queryVillageInfoByID(unionFamily.getVillage_id());

        List<UnionFamilyLeaderPhoto> unionFamilyLeaderPhotos
                = new UnionFamilyDaoImpl().queryUnionFamilyLeaderPhotoListByUnionFamilyID(unionFamily.getUnionfamily_id());

        //封装回显数据
        HashMap<String,Object> hashMap = new HashMap<>();


        hashMap.put("unionfamily_id", unionFamily.getUnionfamily_id());
        hashMap.put("uf_attribute", unionFamily.getUf_attribute());
        if(village!=null){
            hashMap.put("village", village.getVillage_name());
            hashMap.put("village_id", village.getVillage_id());
        }else {
            hashMap.put("village", "该村庄不存在，请检查基础信息");
            hashMap.put("village_id", "该村庄不存在，请检查基础信息");
        }
        hashMap.put("uf_leader_name", unionFamily.getUf_leader_name());
        hashMap.put("uf_leader_idcard_num", unionFamily.getUf_leader_idcard_num());
        hashMap.put("uf_leader_phone_num", unionFamily.getUf_leader_phone_num());
        hashMap.put("uf_leader_bankcard_num", unionFamily.getUf_leader_bankcard_num());
        hashMap.put("photo_uf_leader",
                getPhotoMapFromUnionFamilyPhotoList(
                        unionFamilyLeaderPhotos,
                        unionFamily.getUnionfamily_id()));

        return hashMap;
    }

     /*
      * @Description ：将图像的src包装为符合前端需求的map
      * @Param unionFamilyLeaderPhotoList: 需要包装的图片src的list
      * @Param album_title: 相册名称
      * @Return ：HashMap<String,Object> 符合前端显示的格式组成的map
      * @Author: CTF
      * @Date ：2022/3/30 16:33
      */
    private static HashMap<String,Object> getPhotoMapFromUnionFamilyPhotoList(
            List<UnionFamilyLeaderPhoto> unionFamilyLeaderPhotoList,String album_title) throws IOException, ClassNotFoundException {
          /*
            格式
            {
                "title": "", //相册标题
                "id": 123, //相册id
                "start": 0, //初始显示的图片序号，默认0
                "data": [   //相册包含的图片，数组格式
                    {
                      "alt": "图片名",
                      "pid": 666, //图片id
                      "src": "", //原图地址
                      "thumb": "" //缩略图地址
                    }
                    ]
            }
          */

        List<HashMap<String,Object>> dataMapList = new ArrayList<>();
        int index = 0;
        for(UnionFamilyLeaderPhoto unionFamilyLeaderPhoto : unionFamilyLeaderPhotoList){
            index++;
            //重命名图片显示文件名
            String unionfamily_photo_leader_src = unionFamilyLeaderPhoto.getUNIONFAMILY_photo_leader_src();
            int separatorIndex = unionfamily_photo_leader_src.lastIndexOf(File.separator);
            int dotIndex = unionfamily_photo_leader_src.lastIndexOf(".");
            String fileName = "#" +index + "-" +
                    unionfamily_photo_leader_src.substring(separatorIndex+1,dotIndex);
            //生成前端显示要求的map
            HashMap<String,Object> dataMap = new HashMap<>();
            dataMap.put("alt",fileName);
            dataMap.put("pid",index);
            dataMap.put("src",changePhotoSrc(unionFamilyLeaderPhoto.getUNIONFAMILY_photo_leader_src()));
            dataMap.put("thumb",changePhotoSrc(unionFamilyLeaderPhoto.getUNIONFAMILY_photo_leader_src()) );
            //添加到list中
            dataMapList.add(dataMap);
        }

        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("title",album_title);
        hashMap.put("id",album_title+new Date().getTime());
        hashMap.put("start",0);
        hashMap.put("data",dataMapList);

        return hashMap;
    }

    //处理家庭基本信息相关业务-------------------------------------------------------------------//
    // 新增一个家庭 addAFamily(FamilyInfo)
    //根据家庭编号删除一个家庭 deleteAFamilyByFamilyID(FamilyID)
    //修改家庭基础信息 updateFamilyBaseInfo(FamilyInfo)
    //查询所有家庭信息 queryAllFamilyInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询家庭信息 querySomeFamilyInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据家庭编号查询家庭信息 queryFamilyInfoByFamilyID(FamilyID)，注意导出功能配置
    //根据家庭编号为其绑定户口本首页图片 bindFamilyRBPhotoByFamilyID(photoSrc,FamilyID)
    //根据家庭编号查询户口本首页图片 queryFamilyRBPhotoByFamilyID(FamilyID)
    //根据家庭编号查询户口本首页图片并打包压缩，返回压缩文件存放路径 ZipFamilyRBPhotoByFamilyID(FamilyID)
    //根据家庭编号与需要删除的户口本首页图片编号删除该图片 deleteFamilyRBPhotoByFamilyIDAndPhotoId(FamilyID,PhotoId)

    @Override
    /*
     * @Description ：新增一个家庭
     * @Param FamilyInfo: 新增的家庭信息，封装为Family对象(Family FamilyInfo)
     * @Return ：int 新增操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
     * @Author: CTF
     * @Date ：2022/3/29 23:09
     */
    public int addAFamily(Family FamilyInfo) throws SQLException, IOException, ClassNotFoundException {
        //判空
        if(FamilyInfo==null){
            return -1;
        }
        //执行新增操作
        //为该双联户创建图片文件夹
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindowsFamilyResidenceBooklet = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindowsFamilyResidenceBooklet", String.class);

        String photoDirPath = fileSaveDocPathWindows + photoSaveDocPathWindows + photoSaveDocPathWindowsFamilyResidenceBooklet
                + File.separator + FamilyInfo.getFamily_num();

        //检验需要创建的文件夹是否存在
        File photoDirectory = new File(photoDirPath);
        if(!photoDirectory.exists()){
            //文件夹不存在，创建文件夹
            photoDirectory.mkdirs();
        }
        return familyDao.addAFamily(FamilyInfo);
    }

    @Override
    /*
     * @Description ：根据家庭编号删除一个家庭
     * @Param FamilyNum: 需要删除的家庭编号(户号，非系统编号)
     * @Return ：int 删除操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
     * @Author: CTF
     * @Date ：2022/3/29 23:11
     */
    public int deleteAFamilyByFamilyNum(String FamilyNum) throws Exception {
        /*删除该家庭的所有图片信息*/
        //根据该家庭的编号获取其图片文件夹路径
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindows",
                String.class);
        String photoSaveDocPathWindowsFamilyResidenceBooklet = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindowsFamilyResidenceBooklet",
                String.class);

        String path = fileSaveDocPathWindows + photoSaveDocPathWindows + photoSaveDocPathWindowsFamilyResidenceBooklet
                + File.separator + FamilyNum;

        //删除文件夹
        boolean flag = FileUtils.deleteIfExists(path);

        int delete_family_rbphoto_Code = 0;
        int delete_familybaseinfo_Code = 0;
        if(flag){
            //图片文件已经删除
            /*删除数据库中的图片信息*/
            delete_family_rbphoto_Code = familyDao.deleteFamilyRBPhotoByFamilyNum(FamilyNum);
            /*删除数据库中的基础信息*/
            delete_familybaseinfo_Code = familyDao.deleteAFamilyByFamilyNum(FamilyNum);
        }else {
            throw new FileOperateException("图片文件删除失败");
        }

        return delete_family_rbphoto_Code + delete_familybaseinfo_Code;
    }

    @Override
    /*
     * @Description ：修改家庭基础信息
     * @Param FamilyInfo: 需要更新的家庭信息，封装为Family对象(Family FamilyInfo)
     * @Return ：int 更新操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
     * @Author: CTF
     * @Date ：2022/3/29 23:11
     */
    public int updateFamilyBaseInfo(Family FamilyInfo) throws SQLException {
        //判空
        if(FamilyInfo==null){
            return -1;
        }
        int statusCode = familyDao.updateFamilyBaseinfo(FamilyInfo);
        if(statusCode != 1){
            return -2;
        }
        return statusCode;
    }

    @Override
    /*
     * @Description ：查询所有家庭信息，注意导出功能配置
     * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
     * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
     * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含Family对象，可能还包含其他对象，
     *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
     * @Author: CTF
     * @Date ：2022/3/29 23:12
     */
    public List<HashMap<String, Object>> queryAllFamilyInfo(Integer pageNo, Integer PageSize) throws SQLException, IOException, ClassNotFoundException {
        List<Family> families = familyDao.queryAllFamily(pageNo,PageSize);
        return getResultMapFromList_family_other(families);
    }

    @Override
    /*
     * @Description ：根据条件查询家庭信息，注意导出功能配置
     * @Param queryParams: 查询条件map
     * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
     * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
     * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含Family对象，可能还包含其他对象，
     *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
     * @Author: CTF
     * @Date ：2022/3/29 23:13
     */
    public List<HashMap<String, Object>> querySomeFamilyInfo(Map<String, String[]> queryParams, Integer pageNo, Integer PageSize) throws SQLException, IOException, ClassNotFoundException {
        List<Family> families = familyDao.querySomeFamiles(queryParams, pageNo, PageSize);
        return getResultMapFromList_family_other(families);
    }

    @Override
    /*
     * @Description ：根据家庭编号查询家庭信息，注意导出功能配置
     * @Param FamilyNum: 家庭编号(户号，非系统编号)
     * @Return ：HashMap<String,Object> 将符合条件的Family对象封装成方便前端显示的map
     * @Author: CTF
     * @Date ：2022/3/29 23:13
     */
    public HashMap<String, Object> queryFamilyInfoByFamilyNum(String FamilyNum) throws SQLException, IOException, ClassNotFoundException {
        Family family = familyDao.queryAFamilyByFamilyNum(FamilyNum);
        return getResultMapFromBean_family_other(family);
    }

    @Override
     /*
      * @Description ：根据家庭编号为其绑定户口本首页图片
      * @Param photoSrc: 上传的图片存放路径
      * @Param FamilyNum: 需要绑定的家庭编号(户号，非系统编号)
      * @Return ：int 操作码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/30 11:47
      */
    public int bindFamilyRBPhotoByFamilyNum(String photoSrc, String FamilyNum) throws SQLException {
        return familyDao.bindFamilyRBPhotoByFamilyNum(FamilyNum,photoSrc);
    }

    @Override
    /*
     * @Description ：根据家庭编号查询户口本首页图片
     * @Param FamilyNum: 需要查询图片信息的家庭编号(户号，非系统编号)
     * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
     * @Author: CTF
     * @Date ：2022/3/29 23:15
     */
    public List<HashMap<String, Object>> queryFamilyRBPhotoByFamilyNum(String FamilyNum) throws SQLException {

        List<FamilyRBPhoto> familyRBPhotos = familyDao.queryFamilyRBPhotoListByFamilyNum(FamilyNum);

        //获取附加信息对象
        Family family = familyDao.queryAFamilyByFamilyNum(FamilyNum);
        String familyName = family.getFamily_name();
        String familyNum = family.getFamily_num();

        List<HashMap<String,Object>> hashMapList = new ArrayList<>();
        for(FamilyRBPhoto familyRBPhoto : familyRBPhotos){
            //重命名图片显示文件名
            //获取路径
            String family_photo_residence_booklet_src = familyRBPhoto.getFamily_photo_residence_booklet_src();
            int separatorIndex = family_photo_residence_booklet_src.lastIndexOf(File.separator);
            int dotIndex = family_photo_residence_booklet_src.lastIndexOf(".");
            String photo_name = family_photo_residence_booklet_src.substring(separatorIndex+1,dotIndex);

            //封装回显数据
            HashMap<String,Object> hashMap = new HashMap<>();
            hashMap.put("photo_name", photo_name);
            hashMap.put("photoID",familyRBPhoto.getPhotoID());
            hashMap.put("family_rb_photo_src",familyRBPhoto.getFamily_photo_residence_booklet_src());
            hashMap.put("family_name",familyName);
            hashMap.put("family_num",familyNum);

            hashMapList.add(hashMap);
        }

        return hashMapList;
    }

    @Override
    /*
     * @Description ：根据家庭编号查询户口本首页图片并打包压缩
     * @Param FamilyID: 家庭编号(户号，非系统编号)
     * @Return ：String 获取到相关图片打包压缩后的压缩文件存放地址
     * @Author: CTF
     * @Date ：2022/3/29 23:15
     */
    public String ZipFamilyRBPhotoByFamilyNum(String FamilyNum) throws SQLException, IOException, ClassNotFoundException {
        //获取家庭信息
        Family family = familyDao.queryAFamilyByFamilyNum(FamilyNum);

        //读取本系统文件存放的路径
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        //读取所有图片存放的路径
        String picture_path = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindows",
                String.class);
        //读取自然村图片存放的路径
        String photoSaveDocPathWindowsFamilyResidenceBooklet = PropertiedUtils.getValue("system.properties",
                "photoSaveDocPathWindowsFamilyResidenceBooklet",
                String.class);
        //定义本次需要下载的自然村的图片所在的文件夹的地址
        String realFilePath =fileSaveDocPathWindows +
                picture_path +
                photoSaveDocPathWindowsFamilyResidenceBooklet + File.separator + FamilyNum;

        //获取临时文件存放的路径
        String tempFileSavePath = PropertiedUtils.getValue("system.properties",
                "tempFileSavePath",
                String.class);
        //获取临时压缩文件存放的路径
        String tempFileSavePathZIP = PropertiedUtils.getValue("system.properties",
                "tempFileSavePathZIP",
                String.class);
        //合成压缩文件临时存放的绝对路径
        String tempFileSaveRealPath = fileSaveDocPathWindows + tempFileSavePath + tempFileSavePathZIP;

        //生成压缩文件的文件名
        String zipName = tempFileSaveRealPath + File.separator +
                "家庭-【"+family.getFamily_num()+"】"+family.getFamily_name() + "-图片"
                + ".zip";

        //检查压缩文件临时存放地址是否存在
        File file = new File(tempFileSaveRealPath);
        if(!file.exists()){
            //不存在，则创建文件夹
            file.mkdirs();
        }

        //执行压缩程序
        ZipUtils zipUtils = new ZipUtils(zipName);
        zipUtils.compressExe(realFilePath);

        boolean flag = false;
        if(new File(zipName).exists()){
            //压缩成功，已检测到指定地址存在
            flag = true;
        }

        if(flag){
            return zipName;
        }

        return null;
    }

    @Override
    /*
     * @Description ：根据家庭编号与需要删除的户口本首页图片编号删除该图片
     * @Param FamilyNum: 家庭编号(户号，非系统编号)
     * @Param PhotoID: 需要删除的图片编号
     * @Return ：int 删除操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
     * @Author: CTF
     * @Date ：2022/3/29 23:16
     */
    public int deleteFamilyRBPhotoByFamilyNumAndPhotoID(String FamilyNum, int PhotoID) throws SQLException, IOException, ClassNotFoundException, FileOperateException {
        /*删除该家庭的指定图片编号所对应的文件信息*/
        //根据图片编号获取家庭图片对象
        FamilyRBPhoto familyRBPhoto
                = familyDao.queryFamilyRBPhotoByFamilyNumAndPhotoID(FamilyNum, PhotoID);

        //获取需要删除的图片路径
        String photoPath = familyRBPhoto.getFamily_photo_residence_booklet_src();
        //获取系统文件存放根路径
        String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                "fileSaveDocPathWindows",
                String.class);
        //合成文件真正路径
        String realPath = fileSaveDocPathWindows + photoPath;
        //删除文件
        boolean flag = FileUtils.deleteIfExists(realPath);

        int delete_photo_Code = 0;
        if(flag){
            //图片文件已经删除
            /*删除数据库中的图片信息*/
            delete_photo_Code = familyDao.deleteFamilyRBPhotoByFamilyNumAndPhotoID(FamilyNum,PhotoID);
        }else {
            throw new FileOperateException("图片文件删除失败");
        }

        return delete_photo_Code;
    }

     /*
      * @Description ：将family对象与其他其中包含的附属信息相关对象封装成一个map，并添加到list，返回前端
      * @Param familyList: 原始数据family对象的list
      * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
      * @Author: CTF
      * @Date ：2022/3/30 16:28
      */
    private List<HashMap<String,Object>> getResultMapFromList_family_other
    (List<Family> familyList) throws SQLException, IOException, ClassNotFoundException {
        //前端结果处理
        List<HashMap<String,Object>> hashMapList = new ArrayList<>();

        for(Family family : familyList){

            HashMap<String, Object> resultMapFromBean_family_other
                    = getResultMapFromBean_family_other(family);

            hashMapList.add(resultMapFromBean_family_other);

        }

        return hashMapList;
    }

     /*
      * @Description ：将family对象与其他其中包含的附属信息相关对象封装成一个map，返回前端
      * @Param family: 原始数据Family对象
      * @Return ：HashMap<String,Object> 符合前端显示格式的map
      * @Author: CTF
      * @Date ：2022/3/30 16:27
      */
    private HashMap<String,Object> getResultMapFromBean_family_other
    (Family family) throws SQLException, IOException, ClassNotFoundException {

        //获取附加信息对象:所属行政村、所属自然村、所属双联户
        Village village = new VillageDaoImpl()
                .queryVillageInfoByID(family.getVillage_id());
        UCPVillage ucpVillage = new UCPVillageDaoImpl()
                .queryUCPVillageByUCPVillageID(family.getUcp_village_id());
        UnionFamily unionFamily = new UnionFamilyDaoImpl().
                queryAnUnionFamilyByID(family.getUnionfamily_id());

        List<FamilyRBPhoto> familyRBPhotos
                = new FamilyDaoImpl().queryFamilyRBPhotoListByFamilyNum(family.getFamily_num());

        //封装回显数据
        HashMap<String,Object> hashMap = new HashMap<>();

        hashMap.put("family_num", family.getFamily_num());
        hashMap.put("family_id", family.getFamily_id());
        hashMap.put("family_name", family.getFamily_name());
        hashMap.put("family_type", family.getFamily_type());
        hashMap.put("address", family.getAddress());

        if(village!=null){
            hashMap.put("village", village.getVillage_name());
            hashMap.put("village_id", village.getVillage_id());
        }else {
            hashMap.put("village", "无");
            hashMap.put("village_id", "无");
        }

        if(ucpVillage!=null){
            hashMap.put("ucp_village", ucpVillage.getUcp_village_name());
            hashMap.put("ucp_village_id", ucpVillage.getUcp_village_id());
        }else {
            hashMap.put("ucp_village", "无");
            hashMap.put("ucp_village_id", "无");
        }

        if(unionFamily!=null){
            hashMap.put("unionfamily", unionFamily.getUnionfamily_id());
        }else {
            hashMap.put("unionfamily", "无");
        }

        hashMap.put("photo_family_rb",
                getPhotoMapFromFamilyRBPhotoList(
                        familyRBPhotos,
                        family.getFamily_name()));

        return hashMap;
    }

     /*
      * @Description ：将图像的src包装为符合前端需求的map
      * @Param familyRBPhotoList: 需要包装的图片src的list
      * @Param album_title: 相册名称
      * @Return ：HashMap<String,Object> 符合前端显示的格式组成的map
      * @Author: CTF
      * @Date ：2022/3/30 16:26
      */
    private static HashMap<String,Object> getPhotoMapFromFamilyRBPhotoList(
            List<FamilyRBPhoto> familyRBPhotoList,String album_title) throws IOException, ClassNotFoundException {
          /*
            格式
            {
                "title": "", //相册标题
                "id": 123, //相册id
                "start": 0, //初始显示的图片序号，默认0
                "data": [   //相册包含的图片，数组格式
                    {
                      "alt": "图片名",
                      "pid": 666, //图片id
                      "src": "", //原图地址
                      "thumb": "" //缩略图地址
                    }
                    ]
            }
          */

        List<HashMap<String,Object>> dataMapList = new ArrayList<>();
        int index = 0;
        for(FamilyRBPhoto familyRBPhoto : familyRBPhotoList){
            index++;
            //重命名图片显示文件名
            //获取路径
            String family_photo_residence_booklet_src = familyRBPhoto.getFamily_photo_residence_booklet_src();
            int separatorIndex = family_photo_residence_booklet_src.lastIndexOf(File.separator);
            int dotIndex = family_photo_residence_booklet_src.lastIndexOf(".");
            String fileName = "#" +index + "-" +
                    family_photo_residence_booklet_src.substring(separatorIndex+1,dotIndex);
            //生成前端显示要求的map
            HashMap<String,Object> dataMap = new HashMap<>();
            dataMap.put("alt",fileName);
            dataMap.put("pid",index);
            dataMap.put("src",changePhotoSrc(familyRBPhoto.getFamily_photo_residence_booklet_src()));
            dataMap.put("thumb",changePhotoSrc(familyRBPhoto.getFamily_photo_residence_booklet_src()) );
            //添加到list中
            dataMapList.add(dataMap);
        }

        HashMap<String,Object> hashMap = new HashMap<>();
        hashMap.put("title",album_title);
        hashMap.put("id",album_title+new Date().getTime());
        hashMap.put("start",0);
        hashMap.put("data",dataMapList);

        return hashMap;
    }


    //处理人员基本信息相关业务------------------------------------------------------------------//


}
