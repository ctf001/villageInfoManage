package com.ctf.service;

import com.ctf.bean.*;

import java.sql.SQLException;
import java.util.List;

public interface SystemDataService {
    //查询nation 民族名单
    List<Nation> queryNaiton();

    //修改短信提醒天数
    int updateSmsAlertDays(int newDays) throws SQLException;
    //修改短信发送对象
    int updateSendObj(String objName,int status) throws SQLException;
    //查询发送对象状态码
    int querySendObjStatusCode(String objName) throws SQLException;



    //新增用户（创建用户）
    int addAAccount(User user);
    //删除指定用户
    int deleteAAccount(int userId);
    //修改指定用户登录密码
    int updatePasswordByUserID(int id, String newPWD,String oldPWD);
    //重置密码
    int resetPasswordByUserID(int id);
    //修改指定用户信息
    int updateUserInfo(User newUserInfo);
    //查询所有用户信息
    List<User> queryAccountInfo(Integer pageNo, Integer pageSize);
    //查询所有角色信息
    List<Role> queryRoleInfo(Integer pageNo, Integer pageSize);
    //查询短信提醒天数
    int querySmsAlertDays() throws SQLException;
}
