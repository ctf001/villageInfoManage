package com.ctf.service;

import com.ctf.Exception.FileOperateException;
import com.ctf.bean.baseinfo.*;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface BaseInfoService {
    //处理乡镇基本信息相关业务------------------------------------------------------------------//
    //新增一个乡镇addATown(newTownInfo)
    //删除一个乡镇deleteATownByTownId(TownId)
    //修改乡镇信息updateTownInfo（Town newTownInfo）
    //查询所有乡镇信息queryAllTownInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询乡镇信息querySomeTownInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据乡镇编号查询乡镇信息queryTownInfoByTownId(TownId)，注意导出功能配置

     /*
      * @Description ：新增一个乡镇addATown(Town newTownInfo)
      * @Param newTownInfo:  新增的乡镇信息
      * @Return ：int  新增操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/7 18:46
      */
    int addATown(Town newTownInfo) throws SQLException;
     /*
      * @Description ：根据乡镇id删除一个乡镇deleteATownByTownId(TownId)
      * @Param townID: 乡镇id
      * @Return ：int  删除乡镇信息操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/7 18:46
      */
    int deleteATownByTownId(int townID) throws SQLException;
      /*
       * @Description ：修改乡镇信息updateTownInfo（Town newTownInfo）
       * @Param newTownInfo: 需要更新的乡镇信息
       * @Return ：int 更新乡镇信息操作的状态，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
       * @Author: CTF
       * @Date ：2022/3/7 18:06
       */
    int updateTownInfo(Town newTownInfo) throws SQLException;
     /*
      * @Description ：查询所有乡镇信息queryAllTownInfo(PageNo,PageSize)，注意导出功能配置
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param pageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<Town> 查询返回的Town的list集合
      * @Author: CTF
      * @Date ：2022/3/7 18:04
      */
    List<Town> queryAllTownInfo(Integer pageNo,Integer pageSize) throws SQLException;
     /*
      * @Description ：根据条件查询乡镇信息querySomeTownInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
      * @Param queryParams: 查询条件map
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param pageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<Town> 查询返回的Town的list集合
      * @Author: CTF
      * @Date ：2022/3/7 18:34
      */
    List<Town> querySomeTownInfo(Map<String, String[]> queryParams,Integer pageNo, Integer pageSize) throws SQLException;
     /*
      * @Description ：根据乡镇编号查询乡镇信息queryTownInfoByTownId(TownId)，注意导出功能配置
      * @Param townID: 乡镇编号
      * @Return ：Town 查询出的town对象
      * @Author: CTF
      * @Date ：2022/3/7 18:35
      */
    Town queryTownInfoByTownId(int townID) throws SQLException;
    //处理村基本信息相关业务------------------------------------------------------------------//
    //新增一个村庄addAVillage(VillageInfo)
    //删除一个村庄deleteAVillageByVillageId(VillageId)
    //修改村庄信息updateVillageInfo（newVillage）
    //查询所有村庄信息queryAllVillageInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询村庄信息querySomeVillageInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据村庄编号查询村庄信息queryVillageInfoByVillageId(VillageId)，注意导出功能配置

     /*
      * @Description ：新增一个村庄addAVillage(VillageInfo)
      * @Param village: 新增的村庄信息
      * @Return ：int 新增操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/15 20:19
      */
    int addAVillage(Village village) throws SQLException;
     /*
      * @Description ：删除一个村庄deleteAVillageByVillageId(VillageId)
      * @Param VillageId: 需要删除的村庄编号
      * @Return ：int 删除操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/15 20:20
      */
    int deleteAVillageByVillageId(int VillageId) throws SQLException;
     /*
      * @Description ：修改村庄信息updateVillageInfo（newVillage）
      * @Param newVillage: 需要更新的村庄信息，封装为Village对象(Village newVillage)
      * @Return ：int 更新操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/15 20:20
      */
    int updateVillageInfo(Village newVillage) throws SQLException;
     /*
      * @Description ：查询所有村庄信息queryAllVillageInfo(PageNo,PageSize)，注意导出功能配置
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含封装为Village对象，可能还包含其他对象，
      *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
      * @Author: CTF
      * @Date ：2022/3/18 14:44
      */
     List<HashMap<String,Object>> queryAllVillageInfo(Integer pageNo, Integer PageSize) throws SQLException;

     /*
      * @Description ：根据条件查询村庄信息querySomeVillageInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
      * @Param queryParams: 查询条件map
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<HashMap<String,Object>>  返回的结果不只是包含封装为Village对象，可能还包含其他对象，
      *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
      * @Author: CTF
      * @Date ：2022/3/18 14:43
      */
    List<HashMap<String,Object>> querySomeVillageInfo(Map<String, String[]> queryParams,Integer pageNo,Integer PageSize) throws SQLException;
     /*
      * @Description ：根据村庄编号查询村庄信息queryVillageInfoByVillageId(VillageId)，注意导出功能配置
      * @Param VillageId: 村庄编号
      * @Return ：Village 查询出的村庄village对象
      * @Author: CTF
      * @Date ：2022/3/15 20:20
      */
    Village queryVillageInfoByVillageId(int VillageId) throws SQLException;

    //处理自然村基本信息相关业务------------------------------------------------------------------//
    //新增一个自然村 addAnUCPVillage(UCPVillageInfo)
    //删除一个自然村 deleteAnUCPVillageByUCPVillageId(UCPVillageId)
    //修改自然村信息 updateUCPVillageInfo(UCPVillageInfo)
    //查询所有自然村信息 queryAllUCPVillageInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询自然村信息 querySomeUCPVillageInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据自然村编号查询自然村信息 queryUCPVillageInfoByUCPVillageId(UCPVillageId)，注意导出功能配置
    //根据自然村编号为自然村绑定图片bindUCPVillagePhotoByID(photoSrc,UCPVillageId)
    //根据自然村编号查询自然村图片queryUCPVillagePhoto(UCPVillageId)
    //根据自然村编号查询自然村图片并打包压缩，返回压缩文件存放路径ZipUCPVillagePhotoByUCPVillageId(UCPVillageId)
    //根据自然村编号与需要删除的图片编号删除该图片deleteUCPVillagePhotoByPhotoId(UCPVillageId,PhotoId)
    //根据行政村id查询所有该行政村下所有自然村的信息queryUCPVillageByVillageID(village_id)

    /*
      * @Description ：新增一个自然村
      * @Param UCPVillageInfo: 新增的自然村信息，封装为UCPVillage对象(UCPVillage UCPVillageInfo)
      * @Return ：int 新增操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/22 11:14
      */
    int addAnUCPVillage(UCPVillage UCPVillageInfo) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：删除一个自然村
      * @Param UCPVillageId:  需要删除的自然村编号
      * @Return ：int 删除操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/18 14:46
      */
    int deleteAnUCPVillageByUCPVillageId(int UCPVillageId) throws Exception;

     /*
      * @Description ：修改自然村信息
      * @Param UCPVillageInfo: 需要更新的村庄信息，封装为UCPVillage对象(UCPVillage UCPVillageInfo)
      * @Return ：int 更新操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/18 14:46
      */
    int updateUCPVillageInfo(UCPVillage UCPVillageInfo) throws SQLException;

     /*
      * @Description ：查询所有自然村信息，注意导出功能配置
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含封装为UCPVillage对象，可能还包含其他对象，
      *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
      * @Author: CTF
      * @Date ：2022/3/18 14:42
      */
    List<HashMap<String,Object>> queryAllUCPVillageInfo(Integer pageNo, Integer PageSize) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据条件查询自然村信息，注意导出功能配置
      * @Param queryParams: 查询条件map
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含封装为UCPVillage对象，可能还包含其他对象，
      *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
      * @Author: CTF
      * @Date ：2022/3/18 14:45
      */
    List<HashMap<String,Object>> querySomeUCPVillageInfo(Map<String, String[]> queryParams,Integer pageNo,Integer PageSize) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据自然村编号查询自然村信息，注意导出功能配置
      * @Param UCPVillageId: 自然村编号
      * @Return ：HashMap<String,Object> 将符合条件的UCPVillage对象封装成map
      * @Author: CTF
      * @Date ：2022/3/18 18:06
      */
     HashMap<String,Object> queryUCPVillageInfoByUCPVillageId(int UCPVillageId) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据自然村编号为自然村绑定图片
      * @Param photoSrc: 上传的图片存放路径
      * @Param UCPVillageId: 需要绑定的自然村编号
      * @Return ：int 操作码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/18 22:06
      */
     int bindUCPVillagePhotoByID(String photoSrc,int UCPVillageId) throws SQLException;

    /*
     * @Description ：根据自然村编号查询自然村图片
     * @Param UCPVillageId: 需要查询图片信息的自然村编号
     * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
     * @Author: CTF
     * @Date ：2022/3/22 11:36
     */
    List<HashMap<String,Object>> queryUCPVillagePhotoByUCPVillageId(int UCPVillageId) throws SQLException;

     /*
      * @Description ：根据自然村编号查询自然村图片并打包压缩
      * @Param UCPVillageId: 自然村编号
      * @Return ：String 获取到相关图片打包压缩后的压缩文件存放地址
      * @Author: CTF
      * @Date ：2022/3/23 21:04
      */
    String ZipUCPVillagePhotoByUCPVillageId(int UCPVillageId) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据自然村编号与需要删除的图片编号删除该图片
      * @Param UCPVillageId: 自然村编号
      * @Param PhotoId: 需要删除的图片编号
      * @Return ：int 删除操作的状态码
      * @Author: CTF
      * @Date ：2022/3/26 11:02
      */
    int deleteUCPVillagePhotoByUCPVillageIdAndPhotoId(int UCPVillageId,int PhotoId) throws SQLException, IOException, ClassNotFoundException, FileOperateException;

     /*
      * @Description ：根据行政村id查询所有该行政村下所有自然村的信息
      * @Param village_id: 行政村id
      * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
      * @Author: CTF
      * @Date ：2022/3/30 17:40
      */
     List<HashMap<String,Object>> queryUCPVillageByVillageID(int village_id) throws SQLException, IOException, ClassNotFoundException;

    //处理双联户基本信息相关业务------------------------------------------------------------------//
    //新增一个双联户 addAnUnionFamily(UnionFamilyInfo)
    //根据双联户编号删除一个双联户 deleteAnUnionFamilyByUnionFamilyID(UnionFamilyID)
    //修改双联户基础信息 updateUnionFamilyBaseInfo(UnionFamilyInfo)
    //查询所有双联户信息 queryAllUnionFamilyInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询双联户信息 querySomeUnionFamilyInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据双联户编号查询双联户信息 queryUnionFamilyInfoByUnionFamilyID(UnionFamilyID)，注意导出功能配置
    //根据双联户编号为双联户户长绑定图片 bindUnionFamilyLeaderPhotoByUnionFamilyID(photoSrc,UnionFamilyID)
    //根据双联户编号查询双联户户长图片 queryUnionFamilyLeaderPhotoByUnionFamilyID(UnionFamilyID)
    //根据双联户编号查询双联户户长图片并打包压缩，返回压缩文件存放路径 ZipUnionFamilyLeaderPhotoByUnionFamilyID(UnionFamilyID)
    //根据双联户编号与需要删除的图片编号删除该图片 deleteUnionFamilyLeaderPhotoByUnionFamilyIDAndPhotoId(UnionFamilyID,PhotoId)
    //根据行政村id查询所有该行政村下所有双联户的信息queryUnionFamilesByVillageID(village_id)

    /*
      * @Description ：新增一个双联户
      * @Param UnionFamilyInfo: 新增的双联户信息，封装为UnionFamily对象(UnionFamily UnionFamilyInfo)
      * @Return ：int新增操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/27 16:48
      */
    int addAnUnionFamily(UnionFamily UnionFamilyInfo) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据双联户编号删除一个双联户
      * @Param UnionFamilyID: 需要删除的双联户编号
      * @Return ：int删除操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/27 16:48
      */
    int deleteAnUnionFamilyByUnionFamilyID(String UnionFamilyID) throws Exception;

     /*
      * @Description ：修改双联户基础信息
      * @Param UnionFamilyInfo: 需要更新的双联户信息，封装为UnionFamilye对象(UnionFamily UnionFamilyInfo)
      * @Return ：int 更新操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/27 16:49
      */
    int updateUnionFamilyBaseInfo(UnionFamily UnionFamilyInfo) throws SQLException;

     /*
      * @Description ：查询所有双联户信息，注意导出功能配置
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含UnionFamily对象，可能还包含其他对象，
      *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
      * @Author: CTF
      * @Date ：2022/3/27 16:50
      */
    List<HashMap<String,Object>> queryAllUnionFamilyInfo(Integer pageNo, Integer PageSize) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据条件查询双联户信息，注意导出功能配置
      * @Param queryParams: 查询条件map
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含UnionFamily对象，可能还包含其他对象，
      *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
      * @Author: CTF
      * @Date ：2022/3/27 16:50
      */
    List<HashMap<String,Object>> querySomeUnionFamilyInfo(Map<String, String[]> queryParams,Integer pageNo,Integer PageSize) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据双联户编号查询双联户信息，注意导出功能配置
      * @Param UnionFamilyID:双联户编号
      * @Return ：HashMap<String,Object>将符合条件的UnionFamily对象封装成map
      * @Author: CTF
      * @Date ：2022/3/27 16:51
      */
    HashMap<String,Object> queryUnionFamilyInfoByUnionFamilyID(String UnionFamilyID) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据双联户编号为双联户户长绑定图片
      * @Param photoSrc: 上传的图片存放路径
      * @Param UnionFamilyID: 需要绑定的双联户编号
      * @Return ：int 操作码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/27 16:51
      */
    int bindUnionFamilyLeaderPhotoByUnionFamilyID(String photoSrc,String UnionFamilyID) throws SQLException;

     /*
      * @Description ：根据双联户编号查询双联户户长图片
      * @Param UnionFamilyID: 需要查询图片信息的双联户编号
      * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
      * @Author: CTF
      * @Date ：2022/3/27 16:52
      */
    List<HashMap<String,Object>> queryUnionFamilyLeaderPhotoByUnionFamilyID(String UnionFamilyID) throws SQLException;

     /*
      * @Description ：根据双联户编号查询双联户户长图片并打包压缩
      * @Param UnionFamilyID: 双联户编号
      * @Return ：String 获取到相关图片打包压缩后的压缩文件存放地址
      * @Author: CTF
      * @Date ：2022/3/27 16:53
      */
    String ZipUnionFamilyLeaderPhotoByUnionFamilyID(String UnionFamilyID) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ： 根据双联户编号与需要删除的图片编号删除该图片
      * @Param UnionFamilyID:  双联户编号
      * @Param PhotoId: 需要删除的图片编号
      * @Return ：int 删除操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/27 16:53
      */
    int deleteUnionFamilyLeaderPhotoByUnionFamilyIDAndPhotoId(String UnionFamilyID,int PhotoId) throws SQLException, IOException, ClassNotFoundException, FileOperateException;

     /*
      * @Description ：根据行政村id查询所有该行政村下所有双联户的信息
      * @Param village_id: 行政村id
      * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
      * @Author: CTF
      * @Date ：2022/3/30 18:21
      */
    List<HashMap<String,Object>> queryUnionFamilesByVillageID(int village_id) throws SQLException, IOException, ClassNotFoundException;

    //处理家庭基本信息相关业务-------------------------------------------------------------------//
    //新增一个家庭 addAFamily(FamilyInfo)
    //根据家庭编号删除一个家庭 deleteAFamilyByFamilyID(FamilyID)
    //修改家庭基础信息 updateFamilyBaseInfo(FamilyInfo)
    //查询所有家庭信息 queryAllFamilyInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询家庭信息 querySomeFamilyInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据家庭编号查询家庭信息 queryFamilyInfoByFamilyID(FamilyID)，注意导出功能配置
    //根据家庭编号为其绑定户口本首页图片 bindFamilyRBPhotoByFamilyID(photoSrc,FamilyID)
    //根据家庭编号查询户口本首页图片 queryFamilyRBPhotoByFamilyID(FamilyID)
    //根据家庭编号查询户口本首页图片并打包压缩，返回压缩文件存放路径 ZipFamilyRBPhotoByFamilyID(FamilyID)
    //根据家庭编号与需要删除的户口本首页图片编号删除该图片 deleteFamilyRBPhotoByFamilyIDAndPhotoId(FamilyID,PhotoId)

     /*
      * @Description ：新增一个家庭
      * @Param FamilyInfo: 新增的家庭信息，封装为Family对象(Family FamilyInfo)
      * @Return ：int 新增操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/29 23:09
      */
    int addAFamily(Family FamilyInfo) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据家庭编号删除一个家庭
      * @Param FamilyNum: 需要删除的家庭编号(户号，非系统编号)
      * @Return ：int 删除操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/29 23:11
      */
    int deleteAFamilyByFamilyNum(String FamilyNum) throws Exception;

     /*
      * @Description ：修改家庭基础信息
      * @Param FamilyInfo: 需要更新的家庭信息，封装为Family对象(Family FamilyInfo)
      * @Return ：int 更新操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/29 23:11
      */
    int updateFamilyBaseInfo(Family FamilyInfo) throws SQLException;

     /*
      * @Description ：查询所有家庭信息，注意导出功能配置
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含Family对象，可能还包含其他对象，
      *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
      * @Author: CTF
      * @Date ：2022/3/29 23:12
      */
    List<HashMap<String,Object>> queryAllFamilyInfo(Integer pageNo, Integer PageSize) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据条件查询家庭信息，注意导出功能配置
      * @Param queryParams: 查询条件map
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param PageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<HashMap<String,Object>> 返回的结果不只是包含Family对象，可能还包含其他对象，
      *                                        为了方便前端显示，直接将组合内容包装为Map，并装填到list中
      * @Author: CTF
      * @Date ：2022/3/29 23:13
      */
    List<HashMap<String,Object>> querySomeFamilyInfo(Map<String, String[]> queryParams,Integer pageNo,Integer PageSize) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据家庭编号查询家庭信息，注意导出功能配置
      * @Param FamilyNum: 家庭编号(户号，非系统编号)
      * @Return ：HashMap<String,Object> 将符合条件的Family对象封装成方便前端显示的map
      * @Author: CTF
      * @Date ：2022/3/29 23:13
      */
    HashMap<String,Object> queryFamilyInfoByFamilyNum(String FamilyNum) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据家庭编号为其绑定户口本首页图片
      * @Param photoSrc: 上传的图片存放路径
      * @Param FamilyNum: 需要绑定的家庭编号(户号，非系统编号)
      * @Return ：int 操作码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/29 23:14
      */
    int bindFamilyRBPhotoByFamilyNum(String photoSrc,String FamilyNum) throws SQLException;

     /*
      * @Description ：根据家庭编号查询户口本首页图片
      * @Param FamilyNum: 需要查询图片信息的家庭编号(户号，非系统编号)
      * @Return ：List<HashMap<String,Object>> 符合前端显示格式的由map组成的list
      * @Author: CTF
      * @Date ：2022/3/29 23:15
      */
    List<HashMap<String,Object>> queryFamilyRBPhotoByFamilyNum(String FamilyNum) throws SQLException;

     /*
      * @Description ：根据家庭编号查询户口本首页图片并打包压缩
      * @Param FamilyNum: 家庭编号(户号，非系统编号)
      * @Return ：String 获取到相关图片打包压缩后的压缩文件存放地址
      * @Author: CTF
      * @Date ：2022/3/29 23:15
      */
    String ZipFamilyRBPhotoByFamilyNum(String FamilyNum) throws SQLException, IOException, ClassNotFoundException;

     /*
      * @Description ：根据家庭编号与需要删除的户口本首页图片编号删除该图片
      * @Param FamilyNum: 家庭编号
      * @Param PhotoID:需要删除的图片编号
      * @Return ：int 删除操作的状态码，若遇到异常，将异常抛出给servlet处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/29 23:16
      */
    int deleteFamilyRBPhotoByFamilyNumAndPhotoID(String FamilyNum,int PhotoID) throws SQLException, IOException, ClassNotFoundException, FileOperateException;


    //处理人员基本信息相关业务------------------------------------------------------------------//


}
