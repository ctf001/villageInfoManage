package com.ctf.bean.baseinfo;

public class Village {
    //村编号
    private int village_id;
    //村名
    private String village_name;
    //所属乡id
    private int town_id;
    //距离乡政府公里数（公里）
    private float apart_from_town;
    //耕地面积（亩）
    private float plough_area;
    //平均海拔（米）
    private float elevation;
    //草场面积（亩）
    private float meadow_area;

    public Village() {
    }

    public Village(int village_id, String village_name, int town_id,
                   float apart_from_town, float plough_area, float elevation,
                   float meadow_area) {
        this.village_id = village_id;
        this.village_name = village_name;
        this.town_id = town_id;
        this.apart_from_town = apart_from_town;
        this.plough_area = plough_area;
        this.elevation = elevation;
        this.meadow_area = meadow_area;
    }

    @Override
    public String toString() {
        return "Village{" +
                "village_id=" + village_id +
                ", village_name='" + village_name + '\'' +
                ", town_id=" + town_id +
                ", apart_from_town=" + apart_from_town +
                ", plough_area=" + plough_area +
                ", elevation=" + elevation +
                ", meadow_area=" + meadow_area +
                '}';
    }

    public int getVillage_id() {
        return village_id;
    }

    public void setVillage_id(int village_id) {
        this.village_id = village_id;
    }

    public String getVillage_name() {
        return village_name;
    }

    public void setVillage_name(String village_name) {
        this.village_name = village_name;
    }

    public int getTown_id() {
        return town_id;
    }

    public void setTown_id(int town_id) {
        this.town_id = town_id;
    }

    public float getApart_from_town() {
        return apart_from_town;
    }

    public void setApart_from_town(float apart_from_town) {
        this.apart_from_town = apart_from_town;
    }

    public float getPlough_area() {
        return plough_area;
    }

    public void setPlough_area(float plough_area) {
        this.plough_area = plough_area;
    }

    public float getElevation() {
        return elevation;
    }

    public void setElevation(float elevation) {
        this.elevation = elevation;
    }

    public float getMeadow_area() {
        return meadow_area;
    }

    public void setMeadow_area(float meadow_area) {
        this.meadow_area = meadow_area;
    }
}
