package com.ctf.bean.baseinfo;

public class PersonIDCardPhoto extends Person{
    //身份证号
    private String id_number_self;
    //人员身份证图像存储地址
    private String photo_src;
    //人员身份证图像编号
    private int photoID;
    //图像属性:1代表身份证号人像面，2代表身份证号国徽面
    private int photo_type;

    public PersonIDCardPhoto() {
    }

    public PersonIDCardPhoto(String id_number_self, String photo_src, int photoID, int photo_type) {
        this.id_number_self = id_number_self;
        this.photo_src = photo_src;
        this.photoID = photoID;
        this.photo_type = photo_type;
    }

    @Override
    public String toString() {
        return "PersonIDCardPhoto{" +
                "id_number_self='" + id_number_self + '\'' +
                ", photo_src='" + photo_src + '\'' +
                ", photoID=" + photoID +
                ", photo_type=" + photo_type +
                '}';
    }

    public String getId_number_self() {
        return id_number_self;
    }

    public void setId_number_self(String id_number_self) {
        this.id_number_self = id_number_self;
    }

    public String getPhoto_src() {
        return photo_src;
    }

    public void setPhoto_src(String photo_src) {
        this.photo_src = photo_src;
    }

    public int getPhotoID() {
        return photoID;
    }

    public void setPhotoID(int photoID) {
        this.photoID = photoID;
    }

    public int getPhoto_type() {
        return photo_type;
    }

    public void setPhoto_type(int photo_type) {
        this.photo_type = photo_type;
    }
}
