package com.ctf.bean.baseinfo;
//户口本首页照片
public class FamilyRBPhoto {
    //户号（户口本记录）
    private String family_num;
    //家庭户口本首页图像存储地址
    private String family_photo_residence_booklet_src;
    //家庭户口本首页图像编号
    private int photoID;

    public FamilyRBPhoto() {
    }

    public FamilyRBPhoto(String family_num, String family_photo_residence_booklet_src, int photoID) {
        this.family_num = family_num;
        this.family_photo_residence_booklet_src = family_photo_residence_booklet_src;
        this.photoID = photoID;
    }

    @Override
    public String toString() {
        return "FamilyRBPhoto{" +
                "family_num='" + family_num + '\'' +
                ", family_photo_residence_booklet_src='" + family_photo_residence_booklet_src + '\'' +
                ", photoID=" + photoID +
                '}';
    }

    public String getFamily_num() {
        return family_num;
    }

    public void setFamily_num(String family_num) {
        this.family_num = family_num;
    }

    public String getFamily_photo_residence_booklet_src() {
        return family_photo_residence_booklet_src;
    }

    public void setFamily_photo_residence_booklet_src(String family_photo_residence_booklet_src) {
        this.family_photo_residence_booklet_src = family_photo_residence_booklet_src;
    }

    public int getPhotoID() {
        return photoID;
    }

    public void setPhotoID(int photoID) {
        this.photoID = photoID;
    }
}
