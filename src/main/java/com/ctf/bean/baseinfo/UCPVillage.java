package com.ctf.bean.baseinfo;

public class UCPVillage {
    //自然村编号
    private int ucp_village_id;
    //自然村名称
    private String ucp_village_name;
    //所属村id
    private int village_id;
    //距离村委会距离
    private double apart_from_village;
    //耕地面积（亩）
    private double plough_area;
    //平均海拔（米）
    private double elevation;
    //草场面积（亩）
    private double meadow_area;

    public UCPVillage() {
    }

    public UCPVillage(int ucp_village_id, String ucp_village_name, int village_id,
                      double apart_from_village, double plough_area, double elevation,
                      double meadow_area) {
        this.ucp_village_id = ucp_village_id;
        this.ucp_village_name = ucp_village_name;
        this.village_id = village_id;
        this.apart_from_village = apart_from_village;
        this.plough_area = plough_area;
        this.elevation = elevation;
        this.meadow_area = meadow_area;
    }

    @Override
    public String toString() {
        return "UCPVillage{" +
                "ucp_village_id=" + ucp_village_id +
                ", ucp_village_name='" + ucp_village_name + '\'' +
                ", village_id=" + village_id +
                ", apart_from_village=" + apart_from_village +
                ", plough_area=" + plough_area +
                ", elevation=" + elevation +
                ", meadow_area=" + meadow_area +
                '}';
    }

    public int getUcp_village_id() {
        return ucp_village_id;
    }

    public void setUcp_village_id(int ucp_village_id) {
        this.ucp_village_id = ucp_village_id;
    }

    public String getUcp_village_name() {
        return ucp_village_name;
    }

    public void setUcp_village_name(String ucp_village_name) {
        this.ucp_village_name = ucp_village_name;
    }

    public int getVillage_id() {
        return village_id;
    }

    public void setVillage_id(int village_id) {
        this.village_id = village_id;
    }

    public double getApart_from_village() {
        return apart_from_village;
    }

    public void setApart_from_village(double apart_from_village) {
        this.apart_from_village = apart_from_village;
    }

    public double getPlough_area() {
        return plough_area;
    }

    public void setPlough_area(double plough_area) {
        this.plough_area = plough_area;
    }

    public double getElevation() {
        return elevation;
    }

    public void setElevation(double elevation) {
        this.elevation = elevation;
    }

    public double getMeadow_area() {
        return meadow_area;
    }

    public void setMeadow_area(double meadow_area) {
        this.meadow_area = meadow_area;
    }
}
