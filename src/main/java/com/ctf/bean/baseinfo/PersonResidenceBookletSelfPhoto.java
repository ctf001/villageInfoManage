package com.ctf.bean.baseinfo;

public class PersonResidenceBookletSelfPhoto extends Person{
    //身份证号
    private String id_number_self;
    //人员自身户口本页图像存储地址
    private String photo_src;
    //人员自身户口本页图像编号
    private int photoID;

    public PersonResidenceBookletSelfPhoto() {
    }

    public PersonResidenceBookletSelfPhoto(String id_number_self, String photo_src, int photoID) {
        this.id_number_self = id_number_self;
        this.photo_src = photo_src;
        this.photoID = photoID;
    }

    @Override
    public String toString() {
        return "PersonResidenceBookletSelfPhoto{" +
                "id_number_self='" + id_number_self + '\'' +
                ", photo_src='" + photo_src + '\'' +
                ", photoID=" + photoID +
                '}';
    }

    public String getPhoto_src() {
        return photo_src;
    }

    public void setPhoto_src(String photo_src) {
        this.photo_src = photo_src;
    }

    public String getId_number_self() {
        return id_number_self;
    }

    public void setId_number_self(String id_number_self) {
        this.id_number_self = id_number_self;
    }

    public int getPhotoID() {
        return photoID;
    }

    public void setPhotoID(int photoID) {
        this.photoID = photoID;
    }
}
