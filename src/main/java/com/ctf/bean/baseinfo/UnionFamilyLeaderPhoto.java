package com.ctf.bean.baseinfo;

public class UnionFamilyLeaderPhoto {
    //双联户户长图像编号
    private int photoID;
    //双联户编号
    private String UNIONFAMILY_id;
    //双联户户长图像存储地址
    private String UNIONFAMILY_photo_leader_src;

    public UnionFamilyLeaderPhoto() {
    }

    public UnionFamilyLeaderPhoto(int photoID, String UNIONFAMILY_id, String UNIONFAMILY_photo_leader_src) {
        this.photoID = photoID;
        this.UNIONFAMILY_id = UNIONFAMILY_id;
        this.UNIONFAMILY_photo_leader_src = UNIONFAMILY_photo_leader_src;
    }

    @Override
    public String toString() {
        return "UnionFamilyLeaderPhoto{" +
                "photoID=" + photoID +
                ", UNIONFAMILY_id='" + UNIONFAMILY_id + '\'' +
                ", UNIONFAMILY_photo_leader_src='" + UNIONFAMILY_photo_leader_src + '\'' +
                '}';
    }

    public String getUNIONFAMILY_id() {
        return UNIONFAMILY_id;
    }

    public void setUNIONFAMILY_id(String UNIONFAMILY_id) {
        this.UNIONFAMILY_id = UNIONFAMILY_id;
    }

    public int getPhotoID() {
        return photoID;
    }

    public void setPhotoID(int photoID) {
        this.photoID = photoID;
    }



    public String getUNIONFAMILY_photo_leader_src() {
        return UNIONFAMILY_photo_leader_src;
    }

    public void setUNIONFAMILY_photo_leader_src(String UNIONFAMILY_photo_leader_src) {
        this.UNIONFAMILY_photo_leader_src = UNIONFAMILY_photo_leader_src;
    }
}
