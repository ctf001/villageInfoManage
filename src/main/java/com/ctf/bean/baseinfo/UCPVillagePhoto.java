package com.ctf.bean.baseinfo;

public class UCPVillagePhoto {
    //图片编号
    private int photoID;
    //自然村编号
    private int ucp_village_id;
    //村庄图片
    private String UCPVillage_picture_src;

    public UCPVillagePhoto() {
    }

    public UCPVillagePhoto(int photoID, int ucp_village_id, String UCPVillage_picture_src) {
        this.photoID = photoID;
        this.ucp_village_id = ucp_village_id;
        this.UCPVillage_picture_src = UCPVillage_picture_src;
    }

    @Override
    public String toString() {
        return "UCPVillagePhoto{" +
                "photoID=" + photoID +
                ", ucp_village_id=" + ucp_village_id +
                ", UCPVillage_picture_src='" + UCPVillage_picture_src + '\'' +
                '}';
    }

    public int getPhotoID() {
        return photoID;
    }

    public void setPhotoID(int photoID) {
        this.photoID = photoID;
    }

    public int getUcp_village_id() {
        return ucp_village_id;
    }

    public void setUcp_village_id(int ucp_village_id) {
        this.ucp_village_id = ucp_village_id;
    }

    public String getUCPVillage_picture_src() {
        return UCPVillage_picture_src;
    }

    public void setUCPVillage_picture_src(String UCPVillage_picture_src) {
        this.UCPVillage_picture_src = UCPVillage_picture_src;
    }
}