package com.ctf.bean.baseinfo;

public class Town {
    //乡编号
    private Integer town_id;
    //乡镇名称
    private String town_name;
    //所属县
    private String county;

    public Town() {
    }

    public Town(Integer town_id, String town_name, String county) {
        this.town_id = town_id;
        this.town_name = town_name;
        this.county = county;
    }

    @Override
    public String toString() {
        return "Town{" +
                "town_id=" + town_id +
                ", town_name='" + town_name + '\'' +
                ", county='" + county + '\'' +
                '}';
    }

    public Integer getTown_id() {
        return town_id;
    }

    public void setTown_id(Integer town_id) {
        this.town_id = town_id;
    }

    public String getTown_name() {
        return town_name;
    }

    public void setTown_name(String town_name) {
        this.town_name = town_name;
    }

    public String getCounty() {
        return county;
    }

    public void setCounty(String county) {
        this.county = county;
    }
}
