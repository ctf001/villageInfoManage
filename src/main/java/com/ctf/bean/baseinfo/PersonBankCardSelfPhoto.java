package com.ctf.bean.baseinfo;

import java.util.Date;

public class PersonBankCardSelfPhoto extends Person{
    //身份证号
    private String id_number_self;
    //人员银行卡图像存储地址
    private String photo_src;
    //人员银行卡图像编号
    private int photoID;

    public PersonBankCardSelfPhoto() {
    }

    public PersonBankCardSelfPhoto(String id_number_self, String photo_src, int photoID) {
        this.id_number_self = id_number_self;
        this.photo_src = photo_src;
        this.photoID = photoID;
    }

    @Override
    public String toString() {
        return "PersonBankCardSelfPhoto{" +
                "id_number_self='" + id_number_self + '\'' +
                ", photo_src='" + photo_src + '\'' +
                ", photoID=" + photoID +
                '}';
    }

    public String getId_number_self() {
        return id_number_self;
    }

    public void setId_number_self(String id_number_self) {
        this.id_number_self = id_number_self;
    }

    public String getPhoto_src() {
        return photo_src;
    }

    public void setPhoto_src(String photo_src) {
        this.photo_src = photo_src;
    }

    public int getPhotoID() {
        return photoID;
    }

    public void setPhotoID(int photoID) {
        this.photoID = photoID;
    }
}
