package com.ctf.bean.baseinfo;

import java.util.Date;

public class Person {
    //人员编号
    private int person_id;
    // 户号（户口本记录）
    private String family_num;
    //姓名
    private String name;
    //性别
    private String sex;
    //本人身份证号
    private String id_number_self;
    //出生日期
    private Date birthday;
    //年龄
    private int age;
    //籍贯
    private String  native_place;
    //现居住地
    private String address;
    //联系电话
    private String phone;
    //人员类型
    private String person_type;
    //微信
    private String wechat_id;
    //文化程度
    private String education;
    //毕业院校
    private  String schooltag;
    //所学专业
    private String major;
    //宗教信仰
    private String religion;
    //政治面貌
    private String politics_status;
    //工作单位
    private String office;
    //职务
    private String job_title;
    //婚姻状态
    private String marry_status;
    //配偶姓名
    private String name_spouse;
    //配偶身份证号
    private String id_number_spouse;
    //是否为户主
    private String is_master;
    //与户主关系
    private String relation_with_master;
    //是否为劳动力
    private String is_labor;
    //是否人户分离
    private String is_separate;
    //社保卡号
    private String social_security_card_num;
    //银行卡号
    private String bank_card_num;

    public Person() {
    }

    public Person(int person_id, String family_num, String name, String sex,
                  String id_number_self, Date birthday, int age, String native_place,
                  String address, String phone, String person_type, String wechat_id,
                  String education, String schooltag, String major, String religion,
                  String politics_status, String office, String job_title,
                  String marry_status, String name_spouse, String id_number_spouse,
                  String is_master, String relation_with_master, String is_labor,
                  String is_separate, String social_security_card_num,
                  String bank_card_num) {
        this.person_id = person_id;
        this.family_num = family_num;
        this.name = name;
        this.sex = sex;
        this.id_number_self = id_number_self;
        this.birthday = birthday;
        this.age = age;
        this.native_place = native_place;
        this.address = address;
        this.phone = phone;
        this.person_type = person_type;
        this.wechat_id = wechat_id;
        this.education = education;
        this.schooltag = schooltag;
        this.major = major;
        this.religion = religion;
        this.politics_status = politics_status;
        this.office = office;
        this.job_title = job_title;
        this.marry_status = marry_status;
        this.name_spouse = name_spouse;
        this.id_number_spouse = id_number_spouse;
        this.is_master = is_master;
        this.relation_with_master = relation_with_master;
        this.is_labor = is_labor;
        this.is_separate = is_separate;
        this.social_security_card_num = social_security_card_num;
        this.bank_card_num = bank_card_num;
    }

    @Override
    public String toString() {
        return "Person{" +
                "person_id=" + person_id +
                ", family_num='" + family_num + '\'' +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", id_number_self='" + id_number_self + '\'' +
                ", birthday=" + birthday +
                ", age=" + age +
                ", native_place='" + native_place + '\'' +
                ", address='" + address + '\'' +
                ", phone='" + phone + '\'' +
                ", person_type='" + person_type + '\'' +
                ", wechat_id='" + wechat_id + '\'' +
                ", education='" + education + '\'' +
                ", schooltag='" + schooltag + '\'' +
                ", major='" + major + '\'' +
                ", religion='" + religion + '\'' +
                ", politics_status='" + politics_status + '\'' +
                ", office='" + office + '\'' +
                ", job_title='" + job_title + '\'' +
                ", marry_status='" + marry_status + '\'' +
                ", name_spouse='" + name_spouse + '\'' +
                ", id_number_spouse='" + id_number_spouse + '\'' +
                ", is_master='" + is_master + '\'' +
                ", relation_with_master='" + relation_with_master + '\'' +
                ", is_labor='" + is_labor + '\'' +
                ", is_separate='" + is_separate + '\'' +
                ", social_security_card_num='" + social_security_card_num + '\'' +
                ", bank_card_num='" + bank_card_num + '\'' +
                '}';
    }

    public int getPerson_id() {
        return person_id;
    }

    public void setPerson_id(int person_id) {
        this.person_id = person_id;
    }

    public String getFamily_num() {
        return family_num;
    }

    public void setFamily_num(String family_num) {
        this.family_num = family_num;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getId_number_self() {
        return id_number_self;
    }

    public void setId_number_self(String id_number_self) {
        this.id_number_self = id_number_self;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getNative_place() {
        return native_place;
    }

    public void setNative_place(String native_place) {
        this.native_place = native_place;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPerson_type() {
        return person_type;
    }

    public void setPerson_type(String person_type) {
        this.person_type = person_type;
    }

    public String getWechat_id() {
        return wechat_id;
    }

    public void setWechat_id(String wechat_id) {
        this.wechat_id = wechat_id;
    }

    public String getEducation() {
        return education;
    }

    public void setEducation(String education) {
        this.education = education;
    }

    public String getSchooltag() {
        return schooltag;
    }

    public void setSchooltag(String schooltag) {
        this.schooltag = schooltag;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getPolitics_status() {
        return politics_status;
    }

    public void setPolitics_status(String politics_status) {
        this.politics_status = politics_status;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getJob_title() {
        return job_title;
    }

    public void setJob_title(String job_title) {
        this.job_title = job_title;
    }

    public String getMarry_status() {
        return marry_status;
    }

    public void setMarry_status(String marry_status) {
        this.marry_status = marry_status;
    }

    public String getName_spouse() {
        return name_spouse;
    }

    public void setName_spouse(String name_spouse) {
        this.name_spouse = name_spouse;
    }

    public String getId_number_spouse() {
        return id_number_spouse;
    }

    public void setId_number_spouse(String id_number_spouse) {
        this.id_number_spouse = id_number_spouse;
    }

    public String getIs_master() {
        return is_master;
    }

    public void setIs_master(String is_master) {
        this.is_master = is_master;
    }

    public String getRelation_with_master() {
        return relation_with_master;
    }

    public void setRelation_with_master(String relation_with_master) {
        this.relation_with_master = relation_with_master;
    }

    public String getIs_labor() {
        return is_labor;
    }

    public void setIs_labor(String is_labor) {
        this.is_labor = is_labor;
    }

    public String getIs_separate() {
        return is_separate;
    }

    public void setIs_separate(String is_separate) {
        this.is_separate = is_separate;
    }

    public String getSocial_security_card_num() {
        return social_security_card_num;
    }

    public void setSocial_security_card_num(String social_security_card_num) {
        this.social_security_card_num = social_security_card_num;
    }

    public String getBank_card_num() {
        return bank_card_num;
    }

    public void setBank_card_num(String bank_card_num) {
        this.bank_card_num = bank_card_num;
    }
}
