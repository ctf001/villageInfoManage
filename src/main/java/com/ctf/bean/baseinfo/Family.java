package com.ctf.bean.baseinfo;

public class Family {
    //家庭编号
    private int family_id;
    //户号（户口本记录）
    private String family_num;
    //户名
    private String family_name;
    //户别（家庭类型）
    private String family_type;
    //住址
    private String address;
    //双联户编号
    private String unionfamily_id;
    //所属村id
    private int ucp_village_id;
    //所在自然村id
    private int village_id;

    public Family() {
    }

    public Family(int family_id, String family_num, String family_name, String family_type,
                  String address, String unionfamily_id, int ucp_village_id,
                  int village_id) {
        this.family_id = family_id;
        this.family_num = family_num;
        this.family_name = family_name;
        this.family_type = family_type;
        this.address = address;
        this.unionfamily_id = unionfamily_id;
        this.ucp_village_id = ucp_village_id;
        this.village_id = village_id;
    }

    @Override
    public String toString() {
        return "Family{" +
                "family_id=" + family_id +
                ", family_num='" + family_num + '\'' +
                ", family_name='" + family_name + '\'' +
                ", family_type='" + family_type + '\'' +
                ", address='" + address + '\'' +
                ", unionfamily_id='" + unionfamily_id + '\'' +
                ", ucp_village_id=" + ucp_village_id +
                ", village_id=" + village_id +
                '}';
    }

    public int getUcp_village_id() {
        return ucp_village_id;
    }

    public void setUcp_village_id(int ucp_village_id) {
        this.ucp_village_id = ucp_village_id;
    }

    public int getVillage_id() {
        return village_id;
    }

    public void setVillage_id(int village_id) {
        this.village_id = village_id;
    }

    public int getFamily_id() {
        return family_id;
    }

    public void setFamily_id(int family_id) {
        this.family_id = family_id;
    }

    public String getFamily_num() {
        return family_num;
    }

    public void setFamily_num(String family_num) {
        this.family_num = family_num;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getFamily_type() {
        return family_type;
    }

    public void setFamily_type(String family_type) {
        this.family_type = family_type;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getUnionfamily_id() {
        return unionfamily_id;
    }

    public void setUnionfamily_id(String unionfamily_id) {
        this.unionfamily_id = unionfamily_id;
    }


}
