package com.ctf.bean.baseinfo;

public class UnionFamily {
    //联户编号
    private String unionfamily_id;
    //联户单位性质
    private String uf_attribute;
    //双联户户长姓名
    private String uf_leader_name;
    //双联户户长身份证号
    private String uf_leader_idcard_num;
    //双联户户长联系电话
    private String uf_leader_phone_num;
    //双联户户长银行卡号
    private String uf_leader_bankcard_num;
    //所属村id
    private int village_id;
    //户长照片（单独定义为一个类）
    //private String photo_uf_leader;
    //数据系统编号
    private String sid;

    public UnionFamily() {
    }

    public UnionFamily(String unionfamily_id, String uf_attribute, String uf_leader_name,
                       String uf_leader_idcard_num, String uf_leader_phone_num,
                       String uf_leader_bankcard_num, int village_id, String sid) {
        this.unionfamily_id = unionfamily_id;
        this.uf_attribute = uf_attribute;
        this.uf_leader_name = uf_leader_name;
        this.uf_leader_idcard_num = uf_leader_idcard_num;
        this.uf_leader_phone_num = uf_leader_phone_num;
        this.uf_leader_bankcard_num = uf_leader_bankcard_num;
        this.village_id = village_id;
        this.sid = sid;
    }

    @Override
    public String toString() {
        return "UnionFamily{" +
                "unionfamily_id='" + unionfamily_id + '\'' +
                ", uf_attribute='" + uf_attribute + '\'' +
                ", uf_leader_name='" + uf_leader_name + '\'' +
                ", uf_leader_idcard_num='" + uf_leader_idcard_num + '\'' +
                ", uf_leader_phone_num='" + uf_leader_phone_num + '\'' +
                ", uf_leader_bankcard_num='" + uf_leader_bankcard_num + '\'' +
                ", village_id=" + village_id +
                ", sid='" + sid + '\'' +
                '}';
    }

    public String getUnionfamily_id() {
        return unionfamily_id;
    }

    public void setUnionfamily_id(String unionfamily_id) {
        this.unionfamily_id = unionfamily_id;
    }

    public int getVillage_id() {
        return village_id;
    }

    public void setVillage_id(int village_id) {
        this.village_id = village_id;
    }

    public String getUf_attribute() {
        return uf_attribute;
    }

    public void setUf_attribute(String uf_attribute) {
        this.uf_attribute = uf_attribute;
    }

    public String getUf_leader_name() {
        return uf_leader_name;
    }

    public void setUf_leader_name(String uf_leader_name) {
        this.uf_leader_name = uf_leader_name;
    }

    public String getUf_leader_idcard_num() {
        return uf_leader_idcard_num;
    }

    public void setUf_leader_idcard_num(String uf_leader_idcard_num) {
        this.uf_leader_idcard_num = uf_leader_idcard_num;
    }

    public String getUf_leader_phone_num() {
        return uf_leader_phone_num;
    }

    public void setUf_leader_phone_num(String uf_leader_phone_num) {
        this.uf_leader_phone_num = uf_leader_phone_num;
    }

    public String getUf_leader_bankcard_num() {
        return uf_leader_bankcard_num;
    }

    public void setUf_leader_bankcard_num(String uf_leader_bankcard_num) {
        this.uf_leader_bankcard_num = uf_leader_bankcard_num;
    }

    public String getSid() {
        return sid;
    }

    public void setSid(String sid) {
        this.sid = sid;
    }
}
