package com.ctf.dao.impl;

import com.ctf.bean.baseinfo.Town;
import com.ctf.dao.BaseDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TownDaoImpl extends BaseDao {
    //乡镇信息表表名
    private static final String TABLE_TOWNINFO = " "+"TOWNINFO"+" ";

    //新增一个乡镇
    public int addATown(Town town) throws SQLException {
        String sql = "insert into "+TABLE_TOWNINFO+"(town_name,county) values(?,?)";
        return update(sql,town.getTown_name(),town.getCounty());
    }
    //根据id删除一个乡镇
    public int deleteATownByTownId(int town_id) throws SQLException {
        String sql =  "delete from "+TABLE_TOWNINFO+" where town_id=?";
        return update(sql,town_id);
    }
    //修改乡镇信息
    public int updateTownInfo(Town newTownInfo) throws SQLException {
        String sql = "update "+TABLE_TOWNINFO+" set " +
                "town_name=?," +
                "county=? " +
                "where " +
                "town_id=?";
        List<Object> params = new ArrayList<>();
        params.add(newTownInfo.getTown_name());
        params.add(newTownInfo.getCounty());
        params.add(newTownInfo.getTown_id());
        return update(sql,params.toArray());
    }
    //查询所有乡镇
    public List<Town> queryAllTownInfo(Integer pageNo,Integer pageSize) throws SQLException {

        String sql_str = "select * from "+TABLE_TOWNINFO+" order by town_id desc ";
        StringBuilder sql = new StringBuilder(sql_str);

        //看是否为分页查询
        if(pageNo!=null && pageSize!=null){
            //分页查询

            //分页参数：起始值
            Integer start = (pageNo-1)*pageSize;
            //分页参数：结束值
            Integer end = pageSize;
            //参数集合
            List<Object> params = new ArrayList<>();

            sql.append("  limit ?,?");
            params.add(start);
            params.add(end);

            return queryForList(Town.class,sql.toString(),params.toArray());
        }

        return queryForList(Town.class,sql.toString());
    }
    //根据id查询乡镇信息
    public Town queryTownInfoByTownId(int town_id) throws SQLException {
        String sql = "select * from "+TABLE_TOWNINFO+" where town_id=?";
        return queryForOne(Town.class,sql,town_id);
    }
    //按条件查询乡镇信息
    public List<Town> querySomeTownInfo(Map<String, String[]> map, Integer pageNo, Integer pageSize) throws SQLException {

        String sql_str = "select * from "+TABLE_TOWNINFO+" where 1=1 ";
        StringBuilder sql = new StringBuilder(sql_str);
        List<Object> params = new ArrayList<>();

        //解析map数据
        for(Map.Entry<String,String[]> m:map.entrySet()){
            switch (m.getKey()){
                case "town_name":
                    if(!m.getValue()[0].trim().isEmpty()){
                        sql.append(" and town_name like ?");
                        params.add("%"+m.getValue()[0].trim()+"%");
                    }
                    break;
                case "county":
                    if(!m.getValue()[0].trim().isEmpty()){
                        sql.append(" and county like ? ");
                        params.add("%"+m.getValue()[0].trim()+"%");
                    }
                    break;
            }
        }

        //返回的结果排序方式
        sql.append("order by town_id desc");

        //判断是否分页
        if(pageNo!=null && pageSize!=null){
            //需要分页
            //起始值
            Integer start = (pageNo-1)*pageSize;
            //结束值
            Integer end = pageSize;
            sql.append(" limit ?,? ");
            params.add(start);
            params.add(end);
        }

        return queryForList(Town.class,sql.toString(),params.toArray());
    }
}
