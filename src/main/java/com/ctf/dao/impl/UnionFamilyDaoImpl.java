package com.ctf.dao.impl;

import com.ctf.bean.baseinfo.UCPVillagePhoto;
import com.ctf.bean.baseinfo.UnionFamily;
import com.ctf.bean.baseinfo.UnionFamilyLeaderPhoto;
import com.ctf.dao.BaseDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UnionFamilyDaoImpl extends BaseDao {
    //双联户信息表表名
    private static final String TABLE_UNIONFAMILY = " "+"UNIONFAMILY"+" ";
    //双联户户长图片信息表表名
    private static final String TABLE_UNIONFAMILY_LEADER_PHOTO = " "+"UNIONFAMILY_PHOTO_LEADER"+" ";

    //新增一个双联户
    public int addAnUnionFamily(UnionFamily unionFamily) throws SQLException {
        String sql = "insert into "+TABLE_UNIONFAMILY+
                "(unionfamily_id,uf_attribute,uf_leader_name,uf_leader_idcard_num," +
                "uf_leader_phone_num,uf_leader_bankcard_num,village_id)" +
                "values(?,?,?,?,?,?,?)";

        List<Object> params = new ArrayList<>();
        params.add(unionFamily.getUnionfamily_id());
        params.add(unionFamily.getUf_attribute());
        params.add(unionFamily.getUf_leader_name());
        params.add(unionFamily.getUf_leader_idcard_num());
        params.add(unionFamily.getUf_leader_phone_num());
        params.add(unionFamily.getUf_leader_bankcard_num());
        params.add(unionFamily.getVillage_id());

        return update(sql,params.toArray());
    }

    //删除一个双联户
    public int deleteAnUnionFamilyByUnionFamilyID(String unionFamilyID) throws SQLException {
        String sql = "delete from "+TABLE_UNIONFAMILY+" where unionfamily_id=?";
        return update(sql,unionFamilyID);
    }

    //修改双联户的基本信息
    public int updateUnionFamilyBaseinfo(UnionFamily newUnionFamilyInfo) throws SQLException {
        String sql = "update "+TABLE_UNIONFAMILY+" set " +
                "uf_attribute=?,uf_leader_name=?,uf_leader_idcard_num=?," +
                "uf_leader_phone_num=?,uf_leader_bankcard_num=?,village_id=? " +
                "where unionfamily_id=?";

        List<Object> params = new ArrayList<>();
        params.add(newUnionFamilyInfo.getUf_attribute());
        params.add(newUnionFamilyInfo.getUf_leader_name());
        params.add(newUnionFamilyInfo.getUf_leader_idcard_num());
        params.add(newUnionFamilyInfo.getUf_leader_phone_num());
        params.add(newUnionFamilyInfo.getUf_leader_bankcard_num());
        params.add(newUnionFamilyInfo.getVillage_id());
        params.add(newUnionFamilyInfo.getUnionfamily_id());

        return update(sql,params.toArray());
    }

    //查询所有双联户
    public List<UnionFamily> queryAllUnionFamily(Integer pageNo,Integer pageSize) throws SQLException {
        String sql_str = "select * from "+TABLE_UNIONFAMILY+" order by sid desc";
        StringBuilder sql = new StringBuilder(sql_str);
        List<Object> params = new ArrayList<>();

        //判断是否分页
        if(pageNo!=null && pageSize!=null){
            //需要分页
            //获取起始参数
            Integer start = (pageNo-1)*pageSize;
            Integer end = pageSize;

            sql.append(" limit ?,?");
            params.add(start);
            params.add(end);
        }

        return queryForList(UnionFamily.class,sql.toString(),params.toArray());
    }

    //查询双联户数量
    public int queryUnionFamilyCount() throws SQLException {
        return queryAllUnionFamily(null,null).size();
    }

    //根据双联户id查询双联户信息
    public UnionFamily queryAnUnionFamilyByID(String unionfamily_id) throws SQLException {
        String sql = "select * from"+TABLE_UNIONFAMILY+" where unionfamily_id=?";
        return queryForOne(UnionFamily.class,sql,unionfamily_id);
    }

    //根据行政村id查询双联户信息
    public List<UnionFamily> queryUnionFamilesByVillageID(int village_id) throws SQLException {
        String sql = "select * from"+TABLE_UNIONFAMILY+" where village_id=?";
        return queryForList(UnionFamily.class,sql,village_id);
    }

    //根据条件查询双联户信息
    public List<UnionFamily> querySomeUnionFamiles(Map<String,String[]> map,Integer pageNo,Integer pageSize) throws SQLException {
        String sql_str = "select * from "+TABLE_UNIONFAMILY+" where 1=1";
        StringBuilder sql = new StringBuilder(sql_str);
        List<Object> params = new ArrayList<>();

        //解析前端传来的查询条件
        for(Map.Entry<String,String[]> m:map.entrySet()){
            String param_name = m.getKey();
            String param_value = m.getValue()[0].trim();

            switch (param_name){
                case "unionfamily_id":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
                case "uf_attribute":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
                case "village_id":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" = ?");
                        params.add(param_value);
                    }
                    break;
                case "uf_leader_name":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
                case "uf_leader_idcard_num":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
                case "uf_leader_phone_num":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
                case "uf_leader_bankcard_num":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
            }

        }

        //设置排序规则
        sql.append(" order by sid desc ");
        //判断是否分页
        if(pageNo!=null && pageSize!=null){
            //需要分页
            Integer start = (pageNo-1)*pageSize;
            Integer end = pageSize;

            sql.append(" limit ?,?");
            params.add(start);
            params.add(end);
        }

        return queryForList(UnionFamily.class,sql.toString(),params.toArray());
    }

    /*处理双联户户长图片*/
    //根据双联户编号新增双联户图片
    public int bindUnionFamilyLeaderPhotoByUnionFamilyID(String src,String unionfamily_id) throws SQLException {
        String sql = "insert into "+TABLE_UNIONFAMILY_LEADER_PHOTO+"(" +
                "UNIONFAMILY_id,UNIONFAMILY_photo_leader_src) " +
                "values(?,?)";
        return update(sql,unionfamily_id,src);
    }

    //根据自然村编号与需要删除的图片编号删除该图片
    public int deleteUnionFamilyLeaderPhotoByUnionFamilyIdAndPhotoId(String unionfamily_id,int photo_id) throws SQLException {
        String sql = "delete from "+TABLE_UNIONFAMILY_LEADER_PHOTO+" where photoID=? and UNIONFAMILY_id=?";
        return update(sql,photo_id,unionfamily_id);
    }

    //根据双联户编号删除该双联户户长所有图片
    public int deleteUnionFamilyLeaderPhotoByUnionFamilyID(String unionfamily_id) throws SQLException {
        String sql = "delete from "+TABLE_UNIONFAMILY_LEADER_PHOTO+" where UNIONFAMILY_id=?";
        return update(sql,unionfamily_id);
    }

    //根据图片编号与双联户编号查询并返回双联户户长图片对象信息
    public UnionFamilyLeaderPhoto queryUnionFamilyLeaderPhotoByUnionFamilyIdAndPhotoId(String unionfamily_id, int photo_id) throws SQLException {
        String sql = "select * from "+TABLE_UNIONFAMILY_LEADER_PHOTO+" where photoID=? and UNIONFAMILY_id=?";
        return queryForOne(UnionFamilyLeaderPhoto.class,sql,photo_id,unionfamily_id);
    }

    //查询双联户的图片
    public List<UnionFamilyLeaderPhoto> queryUnionFamilyLeaderPhotoListByUnionFamilyID(String unionfamily_id) throws SQLException {
        String sql = "select * from "+TABLE_UNIONFAMILY_LEADER_PHOTO+" where UNIONFAMILY_id=?";
        return queryForList(UnionFamilyLeaderPhoto.class,sql,unionfamily_id);
    }

}
