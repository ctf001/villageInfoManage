package com.ctf.dao.impl;

import com.ctf.bean.baseinfo.*;
import com.ctf.dao.BaseDao;
import com.ctf.dao.PersonDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PersonDaoImpl extends BaseDao implements PersonDao {
    //人口基础信息表表名
    private static final String TABLE_PERSONINFO = " "+"PERSONINFO"+" ";
    //人员身份证图像表表名
    private static final String TABLE_PERSON_PHOTO_IDCARD = " "+"PERSON_PHOTO_IDCARD"+" ";
    //人员身份证图像表查询名
    private static final String QUERY_PERSON_PHOTO_IDCARD = "IDCARD";
    //人员个人图像表表名
    private static final String TABLE_PERSON_PHOTO_PERSON = " "+"PERSON_PHOTO_PERSON"+" ";
    //人员个人图像表查询名
    private static final String QUERY_PERSON_PHOTO_PERSON = " PERSON";
    //人员银行卡图像表表名
    private static final String TABLE_PERSON_PHOTO_BANK_CARD = " "+"PERSON_PHOTO_BANK_CARD"+" ";
    //人员银行卡图像表查询名
    private static final String QUERY_PERSON_PHOTO_BANK_CARD = "BANKCARD";
    //人员户口本信息图像表表名
    private static final String TABLE_PERSON_PHOTO_RESIDENCE_BOOKLET_SELF = " "+"PERSON_PHOTO_RESIDENCE_BOOKLET_SELF"+" ";
    //人员户口本信息图像表查询名
    private static final String QUERY_PERSON_PHOTO_RESIDENCE_BOOKLET_SELF = "RESIDENCEBOOKLET";
    //人员社保卡图像表表名
    private static final String TABLE_PERSON_PHOTO_SOCIAL_SECURITY_CARD = " "+"PERSON_PHOTO_SOCIAL_SECURITY_CARD"+" ";
    //人员社保卡图像表查询名
    private static final String QUERY_PERSON_PHOTO_SOCIAL_SECURITY_CARD = "SOCIALSECURITYCARD";
    //无身份证号
    private static final String NOIDCARDNUM = "";
    //无图片编号
    private static final int NOPHOTOID = -1;
    /*【起】-----------------------------------------共用内部方法------------------------------*/

    //根据业务中的图片类型字符串，返回对应表名
    private String getTableNameByPhotoTypeString(String photoType){
        String tableName = "";
        switch (photoType){
            case "IDCARD":
                tableName = TABLE_PERSON_PHOTO_IDCARD;
                break;
            case "PERSON":
                tableName = TABLE_PERSON_PHOTO_PERSON;
                break;
            case "BANKCARD":
                tableName = TABLE_PERSON_PHOTO_BANK_CARD;
                break;
            case "RESIDENCEBOOKLET":
                tableName = TABLE_PERSON_PHOTO_RESIDENCE_BOOKLET_SELF;
                break;
            case "SOCIALSECURITYCARD":
                tableName = TABLE_PERSON_PHOTO_SOCIAL_SECURITY_CARD;
                break;
        }

        return tableName;
    }

    //根据业务中的图片类型类型对象，返回对应表名
    private  String getTableNameByPhotoTypeClass(Class photoType){
        String tableName = "";
        if (PersonIDCardPhoto.class.equals(photoType)) {
            tableName = TABLE_PERSON_PHOTO_IDCARD;
        } else if (PersonPhoto.class.equals(photoType)) {
            tableName = TABLE_PERSON_PHOTO_PERSON;
        } else if (PersonBankCardSelfPhoto.class.equals(photoType)) {
            tableName = TABLE_PERSON_PHOTO_BANK_CARD;
        } else if (PersonResidenceBookletSelfPhoto.class.equals(photoType)) {
            tableName = TABLE_PERSON_PHOTO_RESIDENCE_BOOKLET_SELF;
        } else if (PersonSocialSecurityCardPhoto.class.equals(photoType)) {
            tableName = TABLE_PERSON_PHOTO_SOCIAL_SECURITY_CARD;
        }

        return tableName;
    }

    /*【止】-----------------------------------------共用内部方法------------------------------*/

    /*【起】  -----------------------------------------重写方法--------------------------------*/

    @Override
    public int addAPerson(Person personBaseInfo) throws SQLException {
        String sql = "insert into "+TABLE_PERSONINFO
                + "(family_num,name,sex,id_number_self,birthday,"
                +"native_place,address,phone,wechat_id,education,"
                +"schooltag,major,religion,politics_status,office,"
                +"job_title,marry_status,name_spouse,id_number_spouse,is_master,"
                +"relation_with_master,is_labor,is_separate,social_security_card_num,bank_card_num)"
                +"values(?,?,?,?,?,"
                +"?,?,?,?,?,"
                +"?,?,?,?,?,"
                +"?,?,?,?,?,"
                +"?,?,?,?,?)";

        //参数集合
        List<Object> params = new ArrayList<>();
        params.add(personBaseInfo.getFamily_num());
        params.add(personBaseInfo.getName());
        params.add(personBaseInfo.getSex());
        params.add(personBaseInfo.getId_number_self());
        params.add(personBaseInfo.getBirthday());
        params.add(personBaseInfo.getNative_place());
        params.add(personBaseInfo.getAddress());
        params.add(personBaseInfo.getPhone());
        params.add(personBaseInfo.getWechat_id());
        params.add(personBaseInfo.getEducation());
        params.add(personBaseInfo.getSchooltag());
        params.add(personBaseInfo.getMajor());
        params.add(personBaseInfo.getReligion());
        params.add(personBaseInfo.getPolitics_status());
        params.add(personBaseInfo.getOffice());
        params.add(personBaseInfo.getJob_title());
        params.add(personBaseInfo.getMarry_status());
        params.add(personBaseInfo.getName_spouse());
        params.add(personBaseInfo.getId_number_spouse());
        params.add(personBaseInfo.getIs_master());
        params.add(personBaseInfo.getRelation_with_master());
        params.add(personBaseInfo.getIs_labor());
        params.add(personBaseInfo.getIs_separate());
        params.add(personBaseInfo.getSocial_security_card_num());
        params.add(personBaseInfo.getBank_card_num());

        return update(sql,params.toArray());
    }

    @Override
    public int addSomePhotoOfAPerson(String addPhotoType,String photoSrc,String IDCardNum,int IDCardFaceFlag) throws SQLException {
        //获取需要使用的表格名称
        String tableName = getTableNameByPhotoTypeString(addPhotoType);
        //基础sql语句
        StringBuilder sql = new StringBuilder("insert into + " +tableName);
        //参数集合
        ArrayList<Object> params = new ArrayList<>();
        switch (tableName){
            case TABLE_PERSON_PHOTO_IDCARD:
                sql.append("(id_number_self,photo_src,photo_type) values(?,?,?)");
                params.add(IDCardNum);
                params.add(photoSrc);
                params.add(IDCardFaceFlag);
                break;
            default:
                sql.append("(id_number_self,photo_src) values(?,?)");
                params.add(IDCardNum);
                params.add(photoSrc);
                break;
        }

        return update(sql.toString(),params.toArray());
    }

    @Override
    public int deleteAPersonBaseInfoByIDCardNum(String IDCardNum) {
        return 0;
    }

    @Override
    public int deleteSomePhotoOfAPerson(String deletePhotoType,String IDCardNum,int photoID) throws SQLException {
        //获取需要使用的表格名称
        String tableName = getTableNameByPhotoTypeString(deletePhotoType);
        //基础sql语句
        StringBuilder sql  = new StringBuilder("delete from " + tableName + " where 1=1 ");
        //参数集合
        ArrayList<Object> params = new ArrayList<>();
        //处理身份证号，若为Null或者为""，则代表不需要追加身份证号的条件语句，否则，需要添加身份证号的语句
        if(IDCardNum != null){
            if(!IDCardNum.trim().equals("")){
                sql.append(" and id_number_self=? ");
                params.add(IDCardNum);
            }
        }
        //处理图像编号，若为-1，则代表不需要追加图像编号的条件语句，否则，需要添加图像编号的语句
        if(photoID == -1){
            sql.append(" and photoID=? ");
            params.add(photoID);
        }

        return update(sql.toString(),params.toArray());
    }

    @Override
    public int updatePersonBaseInfo(Person newPersonInfo) {
        return 0;
    }

    @Override
    public List<Person> queryAllPersonBaseInfo(Integer pageNo, Integer pageSize) throws SQLException {
        //查询部分内容
        String sql_query = " person_id,family_num,name,sex,id_number_self,birthday,TIMESTAMPDIFF(YEAR, birthday, CURDATE()) age," +
                "native_place,address,phone,wechat_id,education," +
                "schooltag,major,religion,politics_status,office," +
                "job_title,marry_status,name_spouse,id_number_spouse,is_master, "+
                "relation_with_master,is_labor,is_separate,social_security_card_num,bank_card_num ";
        //sql字符串
        String sql_str = "select "+sql_query+" from "+TABLE_PERSONINFO+" order by person_id desc";
        //参数集合
        List<Object> params = new ArrayList<>();
        //基础查询语句
        StringBuilder sql = new StringBuilder(sql_str);
        //处理分页
        if(pageNo!=null && pageSize!=null){
            //获取起始参数
            Integer start = (pageNo-1)*pageSize;
            Integer end = pageSize;
            sql.append(" limit ?,?");
            params.add(start);
            params.add(end);
        }
        return queryForList(Person.class,sql.toString(),params.toArray());
    }

    @Override
    public List<Person> querySomePersonBaseInfo(Map<String, Object> paramMap, Integer PageNo, Integer PageSize) {
        return null;
    }

    @Override
    public Person queryAPersonByIDCardNum(String IDCardNum) {
        return null;
    }

    @Override
    public <T> List<T> queryPhotoOfPerson(String IDCardNum, int photoID, Class<T> returnClass) throws SQLException {
        //根据需要查询的图片类型生成需要查询的表名
        String queryTableName = getTableNameByPhotoTypeClass(returnClass);

        //基础查询语句
        StringBuilder sql = new StringBuilder("select * from "+queryTableName+" where 1=1 ");

        //参数集合
        ArrayList<Object> params = new ArrayList<>();

        //处理身份证号，若为Null或者为""，则代表不需要追加身份证号的条件语句，否则，需要添加身份证号的语句
        if(IDCardNum != null){
            if(!IDCardNum.trim().equals("")){
                sql.append(" and id_number_self=? ");
                params.add(IDCardNum);
            }
        }

        //处理图像编号，若为-1，则代表不需要追加图像编号的条件语句，否则，需要添加图像编号的语句
        if(photoID == -1){
            sql.append(" and photoID=? ");
            params.add(photoID);
        }
        return queryForList(returnClass, sql.toString(), params.toArray());
    }

    /*【止】  -----------------------------------------重写方法--------------------------------*/
}
