package com.ctf.dao.impl;

import com.ctf.bean.baseinfo.Family;
import com.ctf.bean.baseinfo.FamilyRBPhoto;
import com.ctf.bean.baseinfo.UnionFamily;
import com.ctf.bean.baseinfo.UnionFamilyLeaderPhoto;
import com.ctf.dao.BaseDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FamilyDaoImpl extends BaseDao {
    //家庭信息表表名
    private static final String TABLE_FAMILY = " "+"FAMILYINFO"+" ";
    //家庭户口本首页图片信息表表名
    private static final String TABLE_FAMILY_PHOTO_RESIDENCE_BOOKLET = " "+"FAMILY_PHOTO_RESIDENCE_BOOKLET"+" ";

     /*
      * @Description ：新增一个家庭
      * @Param Family: 新增的家庭信息，封装为Family对象(Family Family)
      * @Return ：int 新增操作的状态码，若遇到异常，将异常抛出给service处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/29 23:28
      */
    public int addAFamily(Family Family) throws SQLException {
        String sql = "insert into "+TABLE_FAMILY+
                "(family_num,family_name,family_type,address,unionfamily_id,ucp_village_id,village_id)" +
                "values(?,?,?,?,?,?,?)";

        List<Object> params = new ArrayList<>();
        params.add(Family.getFamily_num());
        params.add(Family.getFamily_name());
        params.add(Family.getFamily_type());
        params.add(Family.getAddress());
        params.add(Family.getUnionfamily_id());
        params.add(Family.getUcp_village_id());
        params.add(Family.getVillage_id());

        return update(sql,params.toArray());
    }

     /*
      * @Description ：根据家庭编号删除数据库中家庭基本信息记录
      * @Param FamilyNum: 户号（户口本记录）
      * @Return ：int 删除操作的状态码，若遇到异常，将异常抛出给service处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/30 11:40
      */
    public int deleteAFamilyByFamilyNum(String FamilyNum) throws SQLException {
        String sql = "delete from "+TABLE_FAMILY+" where family_num=?";
        return update(sql,FamilyNum);
    }

     /*
      * @Description ：修改家庭的基本信息
      * @Param FamilyInfo: 需要更新的家庭信息，封装为Family对象(Family FamilyInfo)
      * @Return ：int 更新操作的状态码，若遇到异常，将异常抛出给service处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/30 11:41
      */
    public int updateFamilyBaseinfo(Family FamilyInfo) throws SQLException {
        String sql = "update "+TABLE_FAMILY+" set " +
                "family_num=?,family_name=?,family_type=?," +
                "address=?,unionfamily_id=?,ucp_village_id=?,village_id=? " +
                "where family_id=?";

        List<Object> params = new ArrayList<>();
        params.add(FamilyInfo.getFamily_num());
        params.add(FamilyInfo.getFamily_name());
        params.add(FamilyInfo.getFamily_type());
        params.add(FamilyInfo.getAddress());
        params.add(FamilyInfo.getUnionfamily_id());
        params.add(FamilyInfo.getUcp_village_id());
        params.add(FamilyInfo.getVillage_id());
        params.add(FamilyInfo.getFamily_id());

        return update(sql,params.toArray());
    }

     /*
      * @Description ：查询所有家庭信息
      * @Param pageNo: 如果不为null，则需要分页，此项表示分页时的页码
      * @Param pageSize: 如果不为null，则需要分页，此项表示分页时的数据量
      * @Return ：List<Family> 符合条件的Family对象组成的list
      * @Author: CTF
      * @Date ：2022/3/30 11:42
      */
    public List<Family> queryAllFamily(Integer pageNo,Integer pageSize) throws SQLException {
        String sql_str = "select * from "+TABLE_FAMILY+" order by family_id desc";
        StringBuilder sql = new StringBuilder(sql_str);
        List<Object> params = new ArrayList<>();

        //判断是否分页
        if(pageNo!=null && pageSize!=null){
            //需要分页
            //获取起始参数
            Integer start = (pageNo-1)*pageSize;
            Integer end = pageSize;

            sql.append(" limit ?,?");
            params.add(start);
            params.add(end);
        }

        return queryForList(Family.class,sql.toString(),params.toArray());
    }

     /*
      * @Description ：根据家庭户号查询家庭信息
      * @Param familyNum: 户号（户口本记录）
      * @Return ：Family 符合条件的家庭信息，封装为Family对象
      * @Author: CTF
      * @Date ：2022/3/30 11:45
      */
    public Family queryAFamilyByFamilyNum(String familyNum) throws SQLException {
        String sql = "select * from"+TABLE_FAMILY+" where family_num=?";
        return queryForOne(Family.class,sql,familyNum);
    }

    //根据条件查询双联户信息
    public List<Family> querySomeFamiles(Map<String,String[]> map, Integer pageNo, Integer pageSize) throws SQLException {
        String sql_str = "select * from "+TABLE_FAMILY+" where 1=1";
        StringBuilder sql = new StringBuilder(sql_str);
        List<Object> params = new ArrayList<>();

        //解析前端传来的查询条件
        for(Map.Entry<String,String[]> m:map.entrySet()){
            String param_name = m.getKey();
            String param_value = m.getValue()[0].trim();

            switch (param_name){
                case "family_name":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
                case "family_type":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
                case "family_num":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" = ?");
                        params.add(param_value);
                    }
                    break;
                case "address":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
                case "village_id":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" = ?");
                        params.add(param_value);
                    }
                    break;
                case "ucp_village_id":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" = ?");
                        params.add(param_value);
                    }
                    break;
                case "union_family_id":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" = ?");
                        params.add(param_value);
                    }
                    break;
            }

        }

        //设置排序规则
        sql.append(" order by family_id desc ");
        //判断是否分页
        if(pageNo!=null && pageSize!=null){
            //需要分页
            Integer start = (pageNo-1)*pageSize;
            Integer end = pageSize;

            sql.append(" limit ?,?");
            params.add(start);
            params.add(end);
        }

        return queryForList(Family.class,sql.toString(),params.toArray());
    }

    /*处理家庭户口本首页图片*/
     /*
      * @Description ：根据家庭户号新增户口本首页图片
      * @Param familyNum: 户号（户口本记录）
      * @Param src: 上传的图片存放路径
      * @Return ：int 操作码，若遇到异常，将异常抛出给service处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/30 11:53
      */
    public int bindFamilyRBPhotoByFamilyNum(String familyNum,String src) throws SQLException {
        String sql = "insert into "+TABLE_FAMILY_PHOTO_RESIDENCE_BOOKLET+"(" +
                "family_num,family_photo_residence_booklet_src) " +
                "values(?,?)";
        return update(sql,familyNum,src);
    }

     /*
      * @Description ：根据家庭编号与需要删除的图片的编号删除数据库中该家庭的指定户口本首页图片记录
      * @Param familyNum: 户号（户口本记录）
      * @Param photo_id:  需要删除的图片的编号
      * @Return ：int 操作码，若遇到异常，将异常抛出给service处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/30 16:02
      */
    public int deleteFamilyRBPhotoByFamilyNumAndPhotoID(String familyNum,int photo_id) throws SQLException {
        String sql = "delete from "+TABLE_FAMILY_PHOTO_RESIDENCE_BOOKLET+" where photoID=? and family_num =?";
        return update(sql,photo_id,familyNum);
    }

     /*
      * @Description ：根据家庭编号删除数据库中该家庭的所有户口本首页图片记录
      * @Param familyNum: 户号（户口本记录）
      * @Return ：int 操作码，若遇到异常，将异常抛出给service处理，处理后将相关信息返回给前端
      * @Author: CTF
      * @Date ：2022/3/30 16:04
      */
    public int deleteFamilyRBPhotoByFamilyNum(String familyNum) throws SQLException {
        String sql = "delete from "+TABLE_FAMILY_PHOTO_RESIDENCE_BOOKLET+" where family_num=?";
        return update(sql,familyNum);
    }

     /*
      * @Description ：根据图片编号与家庭编号查询并返回家庭户口本首页图片对象信息
      * @Param familyNum: 户号（户口本记录）
      * @Param photo_id: 图片编号
      * @Return ：FamilyRBPhoto 符合条件的家庭户口本首页图片对象（FamilyRBPhoto familyRBPhoto）
      * @Author: CTF
      * @Date ：2022/3/30 16:05
      */
    public FamilyRBPhoto queryFamilyRBPhotoByFamilyNumAndPhotoID(String familyNum, int photo_id) throws SQLException {
        String sql = "select * from "+TABLE_FAMILY_PHOTO_RESIDENCE_BOOKLET+" where photoID=? and family_num=?";
        return queryForOne(FamilyRBPhoto.class,sql,photo_id,familyNum);
    }

     /*
      * @Description ：根据家庭编号查询家庭的所有户口本首页图片
      * @Param familyNum: 户号（户口本记录）
      * @Return ：List<FamilyRBPhoto> 由家庭户口本首页图片图片对象组成的list
      * @Author: CTF
      * @Date ：2022/3/30 16:06
      */
    public List<FamilyRBPhoto> queryFamilyRBPhotoListByFamilyNum(String familyNum) throws SQLException {
        String sql = "select * from "+TABLE_FAMILY_PHOTO_RESIDENCE_BOOKLET+" where family_num=?";
        return queryForList(FamilyRBPhoto.class,sql,familyNum);
    }

}
