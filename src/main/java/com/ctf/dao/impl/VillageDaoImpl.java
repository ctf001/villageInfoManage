package com.ctf.dao.impl;

import com.ctf.bean.baseinfo.Village;
import com.ctf.dao.BaseDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class VillageDaoImpl extends BaseDao {
    //村庄信息表
    private static final String TABLE_VILLAGEINFO = " "+"VILLAGEINFO"+" ";

    //新增一个村庄
    public int addAVillage(Village village) throws SQLException {
        String sql = "insert into "+TABLE_VILLAGEINFO+"(village_name,town_id,apart_from_town," +
                "plough_area,elevation,meadow_area) values(?,?,?,?,?,?)";

        List<Object> params = new ArrayList<>();
        params.add(village.getVillage_name());
        params.add(village.getTown_id());
        params.add(village.getApart_from_town());
        params.add(village.getPlough_area());
        params.add(village.getElevation());
        params.add(village.getMeadow_area());

        return update(sql,params.toArray());
    }
    //根据id删除一个村庄
    public int deleteAVillageByID(int villageID) throws SQLException {
        String sql = "delete from "+TABLE_VILLAGEINFO+" where village_id=?";
        return update(sql,villageID);
    }
    //修改一个村庄
    public int updateVillageInfo(Village newVillageInfo) throws SQLException {
        String sql = "update "+TABLE_VILLAGEINFO+
                "set village_name = ?," +
                "town_id=?,apart_from_town=?,plough_area=?,elevation=?,meadow_area=? " +
                "where village_id=?";

        List<Object> params = new ArrayList<>();
        params.add(newVillageInfo.getVillage_name());
        params.add(newVillageInfo.getTown_id());
        params.add(newVillageInfo.getApart_from_town());
        params.add(newVillageInfo.getPlough_area());
        params.add(newVillageInfo.getElevation());
        params.add(newVillageInfo.getMeadow_area());
        params.add(newVillageInfo.getVillage_id());

        return update(sql,params.toArray());
    }
    //查询所有村庄信息
    public List<Village> queryAllVillage(Integer pageNo,Integer pageSize) throws SQLException {
        String sql_str = "select *  from "+TABLE_VILLAGEINFO+" order by village_id desc";
        StringBuilder sql = new StringBuilder(sql_str);
        List<Object> params = new ArrayList<>();
        //是否分页
        if(pageNo!=null && pageSize!=null){
            //需要分页
            Integer start = (pageNo-1)*pageSize;
            Integer end = pageSize;

            sql.append(" limit ?,?");
            params.add(start);
            params.add(end);
        }

        return queryForList(Village.class,sql.toString(),params.toArray());
    }
    //查询村庄个数
    public int queryVillageCount() throws SQLException {
        return queryAllVillage(null,null).size();
    }
    //根据id查询一个村庄
    public Village queryVillageInfoByID(int village_id) throws SQLException {
        String sql = "select * from "+TABLE_VILLAGEINFO+" where village_id=?";
        return queryForOne(Village.class,sql,village_id);
    }
    //根据乡镇id查询所有该乡镇下所有行政村的信息
    public List<Village> queryUCPVillageByVillageID(int town_id) throws SQLException {
        String sql = "select * from"+TABLE_VILLAGEINFO+"where town_id=? order by village_id desc";
        return queryForList(Village.class,sql,town_id);
    }
    //根据条件查询村庄信息
    public List<Village> querySomeVillages(Map<String,String[]> map,Integer pageNo,Integer pageSize) throws SQLException {
        String sql_str = "select * from "+TABLE_VILLAGEINFO+" where 1=1";
        StringBuilder sql = new StringBuilder(sql_str);
        List<Object> params = new ArrayList<>();

        //解析村庄信息
        //1.初始化需要在范围内查询的变量
        String apart_from_town_min = null;
        String apart_from_town_max = null;
        String plough_area_min = null;
        String plough_area_max = null;
        String elevation_min = null;
        String elevation_max = null;
        String meadow_area_min = null;
        String meadow_area_max = null;
        for(Map.Entry<String,String[]> m : map.entrySet()){

            String param_name = m.getKey();
            String param_value = m.getValue()[0].trim();

            switch (param_name){
                case "village_name":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
                case "town_id":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" =?");
                        params.add(param_value);
                    }
                    break;
                    //以下数据需要做范围查询
                case "apart_from_town_min":
                    if(!param_value.isEmpty()){
                        apart_from_town_min = param_value;
                    }
                    break;
                case "apart_from_town_max":
                    if(!param_value.isEmpty()){
                        apart_from_town_max = param_value;
                    }
                    break;
                case "plough_area_min":
                    if(!param_value.isEmpty()){
                        plough_area_min = param_value;
                    }
                    break;
                case "plough_area_max":
                    if(!param_value.isEmpty()){
                        plough_area_max = param_value;
                    }
                    break;
                case "elevation_min":
                    if(!param_value.isEmpty()){
                        elevation_min = param_value;
                    }
                    break;
                case "elevation_max":
                    if(!param_value.isEmpty()){
                        elevation_max = param_value;
                    }
                    break;
                case "meadow_area_min":
                    if(!param_value.isEmpty()){
                        meadow_area_min = param_value;
                    }
                    break;
                case "meadow_area_max":
                    if(!param_value.isEmpty()){
                        meadow_area_max = param_value;
                    }
                    break;
            }
        }
        //2.绑定范围查询相关的信息
        if(apart_from_town_min!=null && apart_from_town_max==null){
            //只有最小值，没有最大值
            sql.append(" and apart_from_town >=?");
            params.add(apart_from_town_min);
        }else if(apart_from_town_min==null && apart_from_town_max!=null){
            //只有最大值，没有最小值
            sql.append(" and apart_from_town <=?");
            params.add(apart_from_town_max);
        }else if(apart_from_town_min!=null && apart_from_town_max!=null){
            //既有最小值，也有最大值
            sql.append(" and (apart_from_town between ? and ?)");
            params.add(apart_from_town_min);
            params.add(apart_from_town_max);
        }

        if(plough_area_min!=null && plough_area_max==null){
            //只有最小值，没有最大值
            sql.append(" and plough_area >=?");
            params.add(plough_area_min);
        }else if(plough_area_min==null && plough_area_max!=null){
            //只有最大值，没有最小值
            sql.append(" and plough_area <=?");
            params.add(plough_area_max);
        }else if(plough_area_min!=null && plough_area_max!=null){
            //既有最小值，也有最大值
            sql.append(" and (plough_area between ? and ?)");
            params.add(plough_area_min);
            params.add(plough_area_max);
        }

        if(elevation_min!=null && elevation_max==null){
            //只有最小值，没有最大值
            sql.append(" and elevation >=?");
            params.add(elevation_min);
        }else if(elevation_min==null && elevation_max!=null){
            //只有最大值，没有最小值
            sql.append(" and elevation <=?");
            params.add(elevation_max);
        }else if(elevation_min!=null && elevation_max!=null){
            //既有最小值，也有最大值
            sql.append(" and (elevation between ? and ?)");
            params.add(elevation_min);
            params.add(elevation_max);
        }

        if(meadow_area_min!=null && meadow_area_max==null){
            //只有最小值，没有最大值
            sql.append(" and meadow_area >=?");
            params.add(meadow_area_min);
        }else if(meadow_area_min==null && meadow_area_max!=null){
            //只有最大值，没有最小值
            sql.append(" and meadow_area <=?");
            params.add(meadow_area_max);
        }else if(meadow_area_min!=null && meadow_area_max!=null){
            //既有最小值，也有最大值
            sql.append(" and (meadow_area between ? and ?)");
            params.add(meadow_area_min);
            params.add(meadow_area_max);
        }

        //返回的结果排序方式
        sql.append("order by village_id desc");

        //判断是否分页
        if(pageNo!=null && pageSize!=null){
            //需要分页
            Integer start = (pageNo-1)*pageSize;
            Integer end = pageSize;

            sql.append(" limit ?,?");
            params.add(start);
            params.add(end);
        }

        return queryForList(Village.class,sql.toString(),params.toArray());
    }
}
