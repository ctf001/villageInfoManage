package com.ctf.dao.impl;

import com.ctf.bean.*;
import com.ctf.dao.BaseDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: SystemDataDao
 * @Package: com.ctf.dao
 * @Description：用于查询系统数据或公共数据
 * @Author: CTF
 * @Date：2021/11/5 18:13
 */

public class SystemDataDaoImpl extends BaseDao {


    //修改短信提醒天数
    public int updateSmsAlertDays(int newDays) throws SQLException {
        String sql = "update system_info set sms_alert=?";
        return update(sql,newDays);
    }
    //修改短信发送对象
    public int updateSendObj(String objName,int status) throws SQLException {
        String sql = "update system_info set "+objName+"=?";
        return update(sql,status);
    }

    //查询发送对象状态码
    public Integer querySendObjStatusCode(String objName) throws SQLException {
        String sql = "select "+objName+" from system_info";
        return Integer.parseInt(queryForANumber(sql)[0].toString());
    }

    //查询需要提前多少天提醒请假者
    public int querySmsAlertDays() throws SQLException {
        String smsAlertDaysSql = "select sms_alert from system_info";
        return (Integer)queryForSingleValue(smsAlertDaysSql);
    }

    //查询当前发送短信的对象代码
    public Map<String,Integer> querySendTargetCode() throws SQLException {
        String doesSendLeaderSql = "select doesSendLeader from system_info";
        String doesSendSelfSql = "select doesSendSelf from system_info";

        HashMap<String,Integer> hashMap = new HashMap<>();
        hashMap.put("doesSendLeaderCode",(Integer) queryForSingleValue(doesSendLeaderSql));
        hashMap.put("doesSendSelfCode",(Integer)queryForSingleValue(doesSendSelfSql));

        return hashMap;
    }


    //查询民族内容（常用于下拉框）
    public List<Nation> queryNation() throws SQLException {
        String sql = "select * from nation_info";
        List<Nation> nations = queryForList(Nation.class, sql);
        return nations;
    }

}
