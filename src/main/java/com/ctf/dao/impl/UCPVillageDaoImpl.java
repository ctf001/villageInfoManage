package com.ctf.dao.impl;

import com.ctf.bean.baseinfo.UCPVillage;
import com.ctf.bean.baseinfo.UCPVillagePhoto;
import com.ctf.dao.BaseDao;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class UCPVillageDaoImpl extends BaseDao {
    //自然村信息表表名
    private static final String TABLE_UCPVILLAGEINFO = " "+"UCPVILLAGEINFO"+" ";
    private static final String TABLE_UCPVILLAGEINFO_PHOTO = " "+"ucpvillageinfo_photo"+" ";

    //新增一个自然村
    public int addAnUCPVillage(UCPVillage ucpVillage) throws SQLException {
        String sql = "insert into "+TABLE_UCPVILLAGEINFO+"(" +
                "ucp_village_id,ucp_village_name,village_id," +
                "apart_from_village,plough_area,elevation,meadow_area) " +
                "values(?,?,?,?,?,?,?)";

        List<Object> params = new ArrayList<>();
        params.add(ucpVillage.getUcp_village_id());
        params.add(ucpVillage.getUcp_village_name());
        params.add(ucpVillage.getVillage_id());
        params.add(ucpVillage.getApart_from_village());
        params.add(ucpVillage.getPlough_area());
        params.add(ucpVillage.getElevation());
        params.add(ucpVillage.getMeadow_area());

        return update(sql,params.toArray());
    }

    //根据id删除一个自然村
    public int deleteAnUCPVillageByID(int ucpVillageID) throws SQLException {
        String sql = "delete from "+TABLE_UCPVILLAGEINFO+" where ucp_village_id=?";
        return update(sql,ucpVillageID);
    }

    //修改一个自然村
    public int updateUCPVillageInfo(UCPVillage newUCPVillageInfo) throws SQLException {
        String sql = "update "+TABLE_UCPVILLAGEINFO+
                "set ucp_village_name = ?,village_id = ?,apart_from_village = ?," +
                "plough_area = ?,elevation = ?,meadow_area = ? " +
                "where ucp_village_id=?";

        List<Object> params = new ArrayList<>();
        params.add(newUCPVillageInfo.getUcp_village_name());
        params.add(newUCPVillageInfo.getVillage_id());
        params.add(newUCPVillageInfo.getApart_from_village());
        params.add(newUCPVillageInfo.getPlough_area());
        params.add(newUCPVillageInfo.getElevation());
        params.add(newUCPVillageInfo.getMeadow_area());
        params.add(newUCPVillageInfo.getUcp_village_id());

        if(update(sql,params.toArray())!=1){
            throw new SQLException();
        }

        return update(sql,params.toArray());
    }

    //查询所有自然村信息
    public List<UCPVillage> queryAllUCPVillage(Integer pageNo,Integer pageSize) throws SQLException {
        String sql_str = "select *  from "+TABLE_UCPVILLAGEINFO+" order by sid desc ";
        StringBuilder sql = new StringBuilder(sql_str);
        List<Object> params = new ArrayList<>();
        //是否分页
        if(pageNo!=null && pageSize!=null){
            //需要分页
            Integer start = (pageNo-1)*pageSize;
            Integer end = pageSize;

            sql.append("limit ?,?");
            params.add(start);
            params.add(end);
        }

        return queryForList(UCPVillage.class,sql.toString(),params.toArray());
    }

    //查询自然村个数
    public int queryUCPVillageCount() throws SQLException {
        return queryAllUCPVillage(null,null).size();
    }

    //根据行政村id查询所有该行政村下所有自然村的信息
    public List<UCPVillage> queryUCPVillageByVillageID(int village_id) throws SQLException {
        String sql = "select * from"+TABLE_UCPVILLAGEINFO+"where village_id=?";
        return queryForList(UCPVillage.class,sql,village_id);
    }

    //根据条件查询自然村信息
    public List<UCPVillage> querySomeUCPVillages(Map<String,String[]> map, Integer pageNo, Integer pageSize) throws SQLException {
        String sql_str = "select * from "+TABLE_UCPVILLAGEINFO+" where 1=1";
        StringBuilder sql = new StringBuilder(sql_str);
        List<Object> params = new ArrayList<>();

        //解析村庄信息
        //1.初始化需要在范围内查询的变量
        String apart_from_village_min = null;
        String apart_from_village_max = null;
        String plough_area_min = null;
        String plough_area_max = null;
        String elevation_min = null;
        String elevation_max = null;
        String meadow_area_min = null;
        String meadow_area_max = null;

        for(Map.Entry<String,String[]> m : map.entrySet()){

            String param_name = m.getKey();
            String param_value = m.getValue()[0].trim();

            switch (param_name){
                case "ucp_village_name":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" like ?");
                        params.add("%"+param_value+"%");
                    }
                    break;
                case "village_id":
                    if(!param_value.isEmpty()){
                        sql.append(" and "+param_name+" = ?");
                        params.add(param_value);
                    }
                    break;
                //以下数据需要做范围查询
                case "apart_from_village_min":
                    if(!param_value.isEmpty()){
                        apart_from_village_min = param_value;
                    }
                    break;
                case "apart_from_village_max":
                    if(!param_value.isEmpty()){
                        apart_from_village_max = param_value;
                    }
                    break;
                case "plough_area_min":
                    if(!param_value.isEmpty()){
                        plough_area_min = param_value;
                    }
                    break;
                case "plough_area_max":
                    if(!param_value.isEmpty()){
                        plough_area_max = param_value;
                    }
                    break;
                case "elevation_min":
                    if(!param_value.isEmpty()){
                        elevation_min = param_value;
                    }
                    break;
                case "elevation_max":
                    if(!param_value.isEmpty()){
                        elevation_max = param_value;
                    }
                    break;
                case "meadow_area_min":
                    if(!param_value.isEmpty()){
                        meadow_area_min = param_value;
                    }
                    break;
                case "meadow_area_max":
                    if(!param_value.isEmpty()){
                        meadow_area_max = param_value;
                    }
                    break;
            }
        }

        //2.绑定范围查询相关的信息
        if(apart_from_village_min!=null && apart_from_village_max==null){
            //只有最小值，没有最大值
            sql.append(" and apart_from_village >=?");
            params.add(apart_from_village_min);
        }
        else if(apart_from_village_min==null && apart_from_village_max!=null){
            //只有最大值，没有最小值
            sql.append(" and apart_from_village <=?");
            params.add(apart_from_village_max);
        }else if(apart_from_village_min!=null && apart_from_village_max!=null){
            //既有最小值，也有最大值
            sql.append(" and (apart_from_village between ? and ?)");
            params.add(apart_from_village_min);
            params.add(apart_from_village_max);
        }

        if(plough_area_min!=null && plough_area_max==null){
            //只有最小值，没有最大值
            sql.append(" and plough_area >=?");
            params.add(plough_area_min);
        }else if(plough_area_min==null && plough_area_max!=null){
            //只有最大值，没有最小值
            sql.append(" and plough_area <=?");
            params.add(plough_area_max);
        }else if(plough_area_min!=null && plough_area_max!=null){
            //既有最小值，也有最大值
            sql.append(" and (plough_area between ? and ?)");
            params.add(plough_area_min);
            params.add(plough_area_max);
        }

        if(elevation_min!=null && elevation_max==null){
            //只有最小值，没有最大值
            sql.append(" and elevation >=?");
            params.add(elevation_min);
        }else if(elevation_min==null && elevation_max!=null){
            //只有最大值，没有最小值
            sql.append(" and elevation <=?");
            params.add(elevation_max);
        }else if(elevation_min!=null && elevation_max!=null){
            //既有最小值，也有最大值
            sql.append(" and (elevation between ? and ?)");
            params.add(elevation_min);
            params.add(elevation_max);
        }

        if(meadow_area_min!=null && meadow_area_max==null){
            //只有最小值，没有最大值
            sql.append(" and meadow_area >=?");
            params.add(meadow_area_min);
        }else if(meadow_area_min==null && meadow_area_max!=null){
            //只有最大值，没有最小值
            sql.append(" and meadow_area <=?");
            params.add(meadow_area_max);
        }else if(meadow_area_min!=null && meadow_area_max!=null){
            //既有最小值，也有最大值
            sql.append(" and (meadow_area between ? and ?)");
            params.add(meadow_area_min);
            params.add(meadow_area_max);
        }

        //返回的结果排序方式
        sql.append("order by sid desc");

        //判断是否分页
        if(pageNo!=null && pageSize!=null){
            //需要分页
            Integer start = (pageNo-1)*pageSize;
            Integer end = pageSize;

            sql.append(" limit ?,?");
            params.add(start);
            params.add(end);
        }

        return queryForList(UCPVillage.class,sql.toString(),params.toArray());
    }

    //根据自然村编号id查询自然村信息
    public UCPVillage queryUCPVillageByUCPVillageID(int ucp_village_id) throws SQLException {
        String sql = "select * from "+TABLE_UCPVILLAGEINFO+" where ucp_village_id=?";
        return queryForOne(UCPVillage.class,sql,ucp_village_id);
    }

    //根据自然村编号新增自然村图片
    public int bindUCPVillagePhotoByID(String src,int ucp_village_id) throws SQLException {
        String sql = "insert into "+TABLE_UCPVILLAGEINFO_PHOTO+"(" +
                "ucp_village_id,UCPVillage_picture_src) " +
                "values(?,?)";
        return update(sql,ucp_village_id,src);
    }

    //根据自然村编号与需要删除的图片编号删除该图片
    public int deleteUCPVillagePhotoByUCPVillageIdAndPhotoId(int UCPVillageId,int photo_id) throws SQLException {
        String sql = "delete from "+TABLE_UCPVILLAGEINFO_PHOTO+" where photoID=? and ucp_village_id=?";
        return update(sql,photo_id,UCPVillageId);
    }

    //根据自然村编号删除自然村所有图片
    public int deleteUCPVillagePhotoByUCPVillageID(int ucp_village_id) throws SQLException {
        String sql = "delete from "+TABLE_UCPVILLAGEINFO_PHOTO+" where ucp_village_id=?";
        return update(sql,ucp_village_id);
    }

    //根据图片编号与自然村编号查询并返回自然村图片对象信息
    public UCPVillagePhoto queryUCPVillagePhotoByUCPVillageIdAndPhotoId(int UCPVillageId,int photo_id) throws SQLException {
        String sql = "select * from "+TABLE_UCPVILLAGEINFO_PHOTO+" where photoID=? and ucp_village_id=?";
        return queryForOne(UCPVillagePhoto.class,sql,photo_id,UCPVillageId);
    }

    //查询自然村的图片
    public List<UCPVillagePhoto> queryUCPVillagePhotoListByUCPVillageID(int ucp_village_id) throws SQLException {
        String sql = "select * from "+TABLE_UCPVILLAGEINFO_PHOTO+" where ucp_village_id=?";
        return queryForList(UCPVillagePhoto.class,sql,ucp_village_id);
    }

}
