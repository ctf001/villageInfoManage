package com.ctf.dao;
import com.ctf.utils.JDBCUtils;
import org.apache.commons.dbutils.QueryRunner;
import org.apache.commons.dbutils.handlers.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public abstract class BaseDao {

    //使用DbUtils操作数据库
    private QueryRunner queryRunner = new QueryRunner();

    /**
     * update() 方法用来执行：Insert\Update\Delete语句
     *
     * @return 如果返回-1,说明执行失败<br/>返回其他表示影响的行数
     */
    public int update(String sql, Object... args) throws SQLException {
        Connection connection = JDBCUtils.getConnection();
        try {
            return queryRunner.update(connection, sql, args);
        } catch (SQLException e) {
            throw e;
        } finally {
            JDBCUtils.close(connection);
        }
    }

   /**
     * 查询返回一个javaBean的sql语句
     *
     * @param type 返回的对象类型
     * @param sql  执行的sql语句
     * @param args sql对应的参数值
     * @param <T>  返回的类型的泛型
     * @return
     */
    public <T> T queryForOne(Class<T> type, String sql, Object... args) throws SQLException {
        Connection con =JDBCUtils.getConnection();
        try {
            return queryRunner.query(con, sql, new BeanHandler<T>(type), args);
        } catch (SQLException e) {
            throw e;
        } finally {
            JDBCUtils.close(con);
        }
    }

    //用于查询某一行的某一个数据
    public Object[] queryForANumber(String sql, Object... args) throws SQLException {
        Connection con =JDBCUtils.getConnection();
        try {
            return queryRunner.query(con, sql, new ArrayHandler(), args);
        } catch (SQLException e) {
            throw e;
        } finally {
            JDBCUtils.close(con);
        }
    }


    /**
     * 查询返回多个javaBean的sql语句
     *
     * @param type 返回的对象类型
     * @param sql  执行的sql语句
     * @param args sql对应的参数值
     * @param <T>  返回的类型的泛型
     * @return
     */
    public <T> List<T> queryForList(Class<T> type, String sql, Object... args) throws SQLException {
        Connection con = JDBCUtils.getConnection();
        try {
            return queryRunner.query(con, sql, new BeanListHandler<T>(type), args);
        } catch (SQLException e) {
            throw e;
        } finally {
            JDBCUtils.close(con);
        }
    }

    /**
     * 执行返回一行一列的sql语句
     * @param sql   执行的sql语句
     * @param args  sql对应的参数值
     * @return
     */
    public Object queryForSingleValue(String sql, Object... args) throws SQLException {

        Connection conn = JDBCUtils.getConnection();

        try {
            return queryRunner.query(conn, sql, new ScalarHandler(), args);
        } catch (Exception e) {
            throw e;
        } finally {
            JDBCUtils.close(conn);
        }
    }
    
    /*
     * @Description :执行返回一列多行的sql语句
     * @param: sql 执行的sql语句
     * @param: args sql对应的参数值
     * @return java.util.List<java.lang.Object>
     * @Author tianfeichen
     * @Date 2021/12/1 11:34
     **/
    public List<Object> queryForOneCol(String sql,Object... args) throws SQLException {

        Connection conn = JDBCUtils.getConnection();
        try {
            return queryRunner.query(conn,sql, new ColumnListHandler("person_id"),args);
        } catch (Exception e) {
            throw e;
        } finally {
            JDBCUtils.close(conn);
        }
    }
    
}
