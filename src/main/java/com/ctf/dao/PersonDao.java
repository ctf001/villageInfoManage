package com.ctf.dao;

import com.ctf.bean.baseinfo.Person;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface PersonDao {
    /*---------------------------------------------新增----------------------------------*/
    //新增一个人员的基础信息 int addAPerson(Person personBaseInfo)
    int addAPerson(Person personBaseInfo) throws SQLException;
    //根据需要指定图像类型名称新增一个人员的相关图像信息 int addSomePhotoOfAPerson(String addPhotoType,String photoSrc,String IDCardNum,int IDCardFaceFlag)
    int addSomePhotoOfAPerson(String addPhotoType,String photoSrc,String IDCardNum,int IDCardFaceFlag) throws SQLException;

    /*---------------------------------------------删除----------------------------------*/
    //从基础信息表中根据身份证号删除一个人员的基础信息记录 int deleteAPersonBaseInfoByIDCardNum(String IDCardNum)
    int deleteAPersonBaseInfoByIDCardNum(String IDCardNum);
    //从指定记录中根据身份证号或图像编号删除一个人员的相关图片信息记录 int deleteSomePhotoOfAPerson(String deletePhotoType,String IDCardNum,int photoID)
    int deleteSomePhotoOfAPerson(String deletePhotoType,String IDCardNum,int photoID) throws SQLException;

    /*---------------------------------------------更新修改----------------------------------*/
    //更新人员基础信息 int updatePersonBaseInfo(Person newPersonInfo)
    int updatePersonBaseInfo(Person newPersonInfo);

    /*---------------------------------------------查询----------------------------------*/
    //查询所有人员信息 List<Person> queryAllPersonBaseInfo(Integer PageNo, Integer PageSize)
    List<Person> queryAllPersonBaseInfo(Integer PageNo, Integer PageSize) throws SQLException;
    //根据条件查询部分人员信息 List<Person> querySomePersonBaseInfo(Map<String,Object> paramMap,Integer PageNo, Integer PageSize)
    List<Person> querySomePersonBaseInfo(Map<String,Object> paramMap, Integer PageNo, Integer PageSize);
    //根据身份证号查询人员信息 Person queryAPersonByIDCardNum(String IDCardNum)
    Person queryAPersonByIDCardNum(String IDCardNum);
    //根据身份证号或图像编号查询人员的相关图像信息 List<T> queryPhotoOfPerson(String IDCardNum,int photoID,String photoType,Class<T> photoType)
    <T> List<T> queryPhotoOfPerson(String IDCardNum, int photoID, Class<T> photoType) throws SQLException;

}
