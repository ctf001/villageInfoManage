package com.ctf.web.Filter;
import com.ctf.bean.Role;
import com.ctf.bean.User;
import com.ctf.dao.impl.UserDaoImpl;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

//@WebFilter("/pages/*")
public class BAuthorityFilter implements Filter {

    private UserDaoImpl userDao = new UserDaoImpl();

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        //获取用户信息
        User user = (User)req.getSession().getAttribute("user");
        //获取用户对应的角色信息
        Role roleInfo = null;
        try {
            roleInfo = userDao.queryRoleInfoByUserId(user.getId());
            if(roleInfo!=null){
                //判断是否是超级管理员
                if(!roleInfo.getRole_name().equals("超级管理员") && !roleInfo.getRole_name().equals("组织部")){
                    //不是超级管理员
                    req.getRequestDispatcher("/ordinaryPages/service/personinfomation/personinformation.jsp").forward(request,response);
                }

                chain.doFilter(request, response);
            }
        } catch (SQLException e) {
            //登录失败,回到登录页面，告知用户
            request.setAttribute("message","系统错误:"+e.getMessage());
            request.getRequestDispatcher("/login.jsp").forward(request,response);
        }


    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void destroy() {

    }
}
