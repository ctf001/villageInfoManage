package com.ctf.web.Servlet;

import com.ctf.Exception.FileOperateException;
import com.ctf.bean.baseinfo.*;
import com.ctf.service.impl.BaseInfoServiceImpl;
import com.ctf.utils.*;
import com.google.gson.Gson;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.*;

@WebServlet("/baseInfoServlet")
public class BaseInfoServlet extends BaseServlet{
    BaseInfoServiceImpl baseInfoService = new BaseInfoServiceImpl();

     /*
      * @Description ：获取系统启用级别
      * @Param : 无
      * @Return ：int -1代表获取失败，1代表乡村两级联用，2代表只启用乡镇，3代表只启用村级
      * @Author: CTF
      * @Date ：2022/3/10 19:27
      */
    private static int getSystemLevel(){
        try {
            String isTownAndVillage = PropertiedUtils.getValue(PropertiedUtils.TOWNINFOPROPERTIES,
                    "isTownAndVillage",String.class);
            String isTown = PropertiedUtils.getValue(PropertiedUtils.TOWNINFOPROPERTIES,
                    "isTown",String.class);
            String isVillage = PropertiedUtils.getValue(PropertiedUtils.TOWNINFOPROPERTIES,
                    "isVillage",String.class);
            if(Integer.parseInt(isTownAndVillage)==1){
                return 1;
            }
            else if(Integer.parseInt(isTown)==1 && Integer.parseInt(isVillage)==1){
                return 1;
            }
            else if(Integer.parseInt(isTown)==1){
                return 2;
            }
            else if(Integer.parseInt(isVillage)==1){
                return 3;
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return -1;
    }

    //处理乡镇基本信息相关业务------------------------------------------------------------------//
    //新增一个乡镇addATown(TownInfo)
    //删除一个乡镇deleteATownByTownId(TownId)
    //修改乡镇信息updateTownInfo（TownInfo）
    //查询所有乡镇信息queryAllTownInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询乡镇信息querySomeTownInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据乡镇编号查询乡镇信息queryTownInfoByTownId(TownId)，注意导出功能配置

     /*
      * @Description ：新增一个乡镇
      * @Param request: http请求对象，需要从中获取新的需要修改的乡镇信息，将其封装为Town对象(Town TownInfo)，注意处理乱码问题
      * @Param response:  http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 19:04
      */
    public void addATown(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //查询参数文件，若isTownAndVillage=1，即表示乡村两级共用，其他表示未启用
        //查询参数文件，若isTown=1，即表示只启用乡镇，其他表示未启用
        //查询参数文件，若isVillage=1，即表示只启用村级，其他表示未启用
        int systemLevel = getSystemLevel();

        //获取参数
        Map<String, String[]> townInfo = request.getParameterMap();
        //封装成对应的对象
        Town town = WebUtils.fillBean(townInfo, Town.class);

        //处理错误信息
        String msg = "Add success.新增成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.addATown(town);
        } catch (SQLException e) {
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

     /*
      * @Description ：根据乡镇编号删除一个乡镇
      * @Param request: http请求对象，需要从中获取需要删除的乡镇编号(int TownId)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 19:03
      */
    public void deleteATownByTownId(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");
        //获取参数
        String town_id_str = request.getParameter("town_id");

        int town_id = -1;
        //判空处理
        if(town_id_str!=null){
            if(!town_id_str.equals("")){
                town_id = Integer.parseInt(town_id_str);
            }
        }

        //处理错误信息
        String msg = "Delete success.删除成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.deleteATownByTownId(town_id);
        } catch (SQLException e) {
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

     /*
      * @Description ：修改乡镇信息
      * @Param request: http请求对象，需要从中获取新的需要修改的乡镇信息，将其封装为Town对象(Town TownInfo)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 19:03
      */
    public void updateTownInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        Map<String, String[]> townInfo = request.getParameterMap();
        //封装成对应的对象
        Town newTownInfo = WebUtils.fillBean(townInfo, Town.class);

        //处理错误信息
        String msg = "Update success.更新成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.updateTownInfo(newTownInfo);
        } catch (SQLException e) {
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

     /*
      * @Description ：查询所有乡镇信息，注意导出功能配置
      * @Param request: http请求对象，需要从中获取分页功能数据，即当前页码(integer PageNo)
      *                 与当前页显示数据量(integer PageSize)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 19:03
      */
    public void queryAllTownInfo(HttpServletRequest request, HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取页码
        Integer pageNo = WebUtils.getPageNo(request);
        //获取当前页显示数量
        Integer pageSize = WebUtils.getPageSize(request);

        //定义成功消息
        String msg = "Success";
        //定义成功代码
        int code = 0;
        List<Town> towns_page = null;
        List<Town> towns_all = null;
        try {
            towns_page = baseInfoService.queryAllTownInfo(pageNo, pageSize);
            towns_all = baseInfoService.queryAllTownInfo(null, null);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",towns_all);
        map.put("data",towns_page);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

     /*
      * @Description ：根据条件查询乡镇信息，注意导出功能配置
      * @Param request: http请求对象，需要从中获取查询条件（Map<String,String> QueryParams）
      *                 与分页功能数据，即当前页码(integer PageNo)与当前页显示数据量(integer PageSize)
      *                 ，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 19:02
      */
    public void querySomeTownInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");
        //获取页码
        Integer pageNo = WebUtils.getPageNo(request);
        //获取当前页显示数量
        Integer pageSize = WebUtils.getPageSize(request);

        //获取前端传来的查询参数
        Map<String, String[]> request_map = request.getParameterMap();

        //定义成功消息
        String msg = "Success";
        //定义成功代码
        int code = 0;
        List<Town> towns_page = null;
        List<Town> towns_all = null;
        try {
            towns_page = baseInfoService.querySomeTownInfo(request_map, pageNo, pageSize);
            towns_all = baseInfoService.querySomeTownInfo(request_map,null, null);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",towns_all);
        map.put("data",towns_page);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

     /*
      * @Description ：根据乡镇编号查询乡镇信息，注意导出功能配置
      * @Param request: http请求对象，需要从中获取需要查询的乡镇编号(int TownId)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 19:05
      */
    public void queryTownInfoByTownId(HttpServletRequest request,HttpServletResponse response){

    }

    //处理村基本信息相关业务------------------------------------------------------------------//
    //新增一个村庄addAVillage(VillageInfo)
    //删除一个村庄deleteAVillageByVillageId(VillageId)
    //修改村庄信息updateVillageInfo(VillageInfo)
    //查询所有村庄信息queryAllVillageInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询村庄信息querySomeVillageInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据乡镇编号查询村庄信息queryVillageInfoByVillageId(VillageId)，注意导出功能配置

    /*
      * @Description ：新增一个村庄
      * @Param request: http请求对象，需要从中获取新的需要添加的村庄信息，将其封装为Village对象(Village VillageInfo)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 18:46
      */
    public void addAVillage(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        Map<String, String[]> villageInfo = request.getParameterMap();
        //封装成对应的对象
        Village village = WebUtils.fillBean(villageInfo, Village.class);

        //处理错误信息
        String msg = "Add success.新增成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.addAVillage(village);
        } catch (SQLException e) {
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }
    //
     /*
      * @Description ：删除一个村庄
      * @Param request: http请求对象，需要从中获取需要删除的村庄编号(int VillageId)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 18:46
      */
    public void deleteAVillageByVillageId(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");
        //获取参数
        String village_id_str = request.getParameter("village_id");

        int village_id = -1;
        //判空处理
        if(village_id_str!=null){
            if(!village_id_str.equals("")){
                village_id = Integer.parseInt(village_id_str);
            }
        }

        //处理错误信息
        String msg = "Delete success.删除成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.deleteAVillageByVillageId(village_id);
        } catch (SQLException e) {
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }
    //
     /*
      * @Description ： 修改村庄信息
      * @Param request: http请求对象,需要从中获取新的需要修改的村庄信息，将其封装为Village对象(Village VillageInfo)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 18:46
      */
    public void updateVillageInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        Map<String, String[]> villageInfo = request.getParameterMap();
        //封装成对应的对象
        Village newVillageInfo = WebUtils.fillBean(villageInfo, Village.class);

        //处理错误信息
        String msg = "Update success.更新成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.updateVillageInfo(newVillageInfo);
        } catch (SQLException e) {
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }
    //
     /*
      * @Description ： 查询所有村庄信息，注意导出功能配置
      * @Param request: http请求对象，需要从中获取分页功能数据，即当前页码(integer PageNo)
      *                 与当前页显示数据量(integer PageSize)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 18:46
      */
    public void queryAllVillageInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取页码
        Integer pageNo = WebUtils.getPageNo(request);
        //获取当前页显示数量
        Integer pageSize = WebUtils.getPageSize(request);

        //定义成功消息
        String msg = "Success";
        //定义成功代码
        int code = 0;
        List<HashMap<String, Object>> village_all = null;
        List<HashMap<String, Object>> village_page = null;
        try {
            village_page = baseInfoService.queryAllVillageInfo(pageNo, pageSize);
            village_all = baseInfoService.queryAllVillageInfo(null, null);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",village_all);
        map.put("data",village_page);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }
    //
     /*
      * @Description ：根据条件查询村庄信息，注意导出功能配置
      * @Param request: http请求对象，需要从中获取查询条件（Map<String,String> QueryParams）
      *                 与分页功能数据，即当前页码(integer PageNo)与当前页显示数据量(integer PageSize)
      *                 ，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 18:46
      */
    public void querySomeVillageInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取页码
        Integer pageNo = WebUtils.getPageNo(request);
        //获取当前页显示数量
        Integer pageSize = WebUtils.getPageSize(request);

        //获取前端传来的查询参数
        Map<String, String[]> request_map = request.getParameterMap();

        //定义成功消息
        String msg = "Success";
        //定义成功代码
        int code = 0;
        List<HashMap<String, Object>> village_all = null;
        List<HashMap<String, Object>> village_page = null;
        try {
            village_page = baseInfoService.querySomeVillageInfo(request_map, pageNo, pageSize);
            village_all = baseInfoService.querySomeVillageInfo(request_map,null, null);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",village_all);
        map.put("data",village_page);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }
    //
     /*
      * @Description ：根据乡镇编号查询村庄信息，注意导出功能配置
      * @Param request: http请求对象，需要从中获取需要查询的村庄编号(int VillageId)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/17 18:46
      */
    public void queryVillageInfoByVillageId(HttpServletRequest request,HttpServletResponse response){

    }

    //处理自然村基本信息相关业务------------------------------------------------------------------//
    //新增一个自然村addAnUCPVillage(UCPVillageInfo)
    //删除一个自然村deleteAnUCPVillageByUCPVillageId(UCPVillageId)
    //修改自然村信息updateUCPVillageInfo(UCPVillageInfo)
    //查询所有自然村信息queryAllUCPVillageInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询自然村信息querySomeUCPVillageInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据自然村编号查询自然村信息queryUCPVillageInfoByUCPVillageId(UCPVillageId)，注意导出功能配置
    //根据自然村编号为自然村绑定图片bindUCPVillagePhotoByID(UCPVillageId)
    //根据自然村编号查询自然村图片queryUCPVillagePhoto(UCPVillageId)
    //根据自然村编号与需要删除的图片编号删除该图片deleteUCPVillagePhotoByPhotoId(UCPVillageId，PhotoId)
    //根据自然村编号打包该自然村图片，并下载downloadUcpVillagePhoto(UCPVillageId)
    //根据行政村编号查询该行政村下自然村信息queryUCPVillageByVillageID(village_id)

    /*
     * @Description ：新增一个自然村
     * @Param request: http请求对象，需要从中获取新的需要添加的自然村信息，将其封装为UCPVillage对象(UCPVillage UCPVillageInfo)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void addAnUCPVillage(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        Map<String, String[]> ucpVillageInfo = request.getParameterMap();
        //封装成对应的对象
        UCPVillage ucpVillage = WebUtils.fillBean(ucpVillageInfo, UCPVillage.class);

        //处理错误信息
        String msg = "Add success.新增成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.addAnUCPVillage(ucpVillage);
        } catch (SQLException | ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("statusCode",code);
        map.put("msg",msg);
        map.put("ucp_village_id",ucpVillage.getUcp_village_id());

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

     /*
      * @Description ：根据自然村编号为自然村绑定图片
      * @Param request: http请求对象，需要从中上传的文件对象
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/18 21:59
      */
    public void bindUCPVillagePhotoByID(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");
        //这种方式不能获取到参数，上传的参数需要对表单数据做处理才能获取，详见下方迭代表单数据里的操作
        //String ucp_village_id = request.getParameter("ucp_village_id");

        Gson gson = new Gson();
        //定义上传文件的相对路径
        String relativePath = "";
        //定义上传文件的完整路径
        String savePath = "";
        //获取请求中需要上传的文件名字（包含后缀名）
        String fileName = "";
        String ucp_village_name = "";
        int ucp_village_id = -1;
        //定义成功时返回前端的信息msg
        String msg = "上传成功";
        //定义成功时的状态码code
        int code = 1;
        try {
            /*上传代码*/
            // 配置上传参数
            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            // 解析请求的内容提取文件数据
            List<FileItem> formItems = upload.parseRequest(request);
            // 迭代表单数据
            //找其他附属信息
            for (FileItem item : formItems) {
                //首先获取自然村编号
                if(item.isFormField()){
                    //不是文件，则是其他附属信息
                    if(item.getFieldName().equals("ucp_village_id")){
                        //该参数则为自然村编号
                        ucp_village_id = Integer.parseInt(item.getString());
                    }

                    if(item.getFieldName().equals("ucp_village_name")){
                        //该参数则为自然村编号
                        ucp_village_name = item.getString();
                    }

                }
            }
            //找上传的文件
            int photoCountIndex = 1;
            //读取本系统文件存放的路径
            String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                    "fileSaveDocPathWindows",
                    String.class);
            //读取所有图片存放的路径
            String picture_path = PropertiedUtils.getValue("system.properties",
                    "photoSaveDocPathWindows",
                    String.class);
            //读取自然村图片存放的路径
            String ucpvillage_picture_path = PropertiedUtils.getValue("system.properties",
                    "photoSaveDocPathWindowsUcpVillage",
                    String.class);

            for (FileItem item : formItems) {
                if (!item.isFormField()) {
                    //是文件，这里就是需要上传的文件了
                    //获取请求中需要上传的文件名字（包含后缀名）
                    fileName = item.getName();
                    //获取后缀名标记的索引
                    int index = fileName.lastIndexOf(File.separator);
                    if (index != -1) {
                        //从文件名中获取除了后缀名之外的真正文件名
                        fileName = fileName.substring(index + 1);
                    }

                    //定义上传文件的相对路径
                    relativePath = picture_path + ucpvillage_picture_path + File.separator + ucp_village_id;

                    //定义上传文件的真实存放路径
                    String realPath = fileSaveDocPathWindows +  relativePath;

                    //检验上传文件夹是否存在
                    File photoDirectory = new File(realPath);
                    if(!photoDirectory.exists()){
                        //文件夹不存在，创建文件夹
                        photoDirectory.mkdirs();
                    }

                    //修改文件名，添加自然村编号
                    fileName = ucp_village_id + "-" + fileName;

                    //定义上传文件的完整路径
                    savePath = String.format("%s"+File.separator+"%s", realPath, fileName);

                    //判断文件是否已经存在，不存在才能上传，存在的话就不用上传了
                    if(!new File(savePath).exists()){
                        //不存在
                        //新建文件处理对象
                        File storeFile = new File(savePath);
                        // 保存文件到硬盘
                        item.write(storeFile);
                        /*数据库记录代码*/
                        baseInfoService.bindUCPVillagePhotoByID(
                                String.format("%s"+File.separator+"%s", relativePath, fileName),
                                ucp_village_id);
                    }else {
                        throw new Exception("文件名已经存在，请重新选择!");
                    }

                }
                photoCountIndex++;
            }
        }
        catch (UnsupportedEncodingException e) {
            code = -1;
            msg = e.getMessage()+"#编码格式错误";
        }
        catch (IOException e) {
            code = -1;
            msg = e.getMessage();
        }
        catch (ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }
        catch (FileUploadException e) {
            code = -1;
            msg = e.getMessage();
        }
        catch (SQLException e){
            code = -1;
            msg = e.getMessage();
        }
        catch (Exception e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> result = new HashMap<>();
        result.put("filepath",relativePath);
        result.put("msg",msg);
        result.put("code",code);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(result);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：删除一个自然村
     * @Param request: http请求对象，需要从中获取需要删除的自然村编号(int UCPVillageId)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void deleteAnUCPVillageByUCPVillageId(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");
        //获取参数
        String ucp_village_id_str = request.getParameter("ucp_village_id");

        int ucp_village_id = -1;
        //判空处理
        if(ucp_village_id_str!=null){
            if(!ucp_village_id_str.equals("")){
                ucp_village_id = Integer.parseInt(ucp_village_id_str);
            }
        }

        //处理错误信息
        String msg = "Delete success.删除成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.deleteAnUCPVillageByUCPVillageId(ucp_village_id);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        } catch (FileOperateException e) {
            code = -1;
            msg = e.getMessage();
        } catch (IOException e) {
            code = -1;
            msg = e.getMessage();
        } catch (ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ： 修改自然村信息
     * @Param request: http请求对象,需要从中获取新的需要修改的自然村信息，将其封装为UCPVillage对象(UCPVillage UCPVillageInfo)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void updateUCPVillageInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        Map<String, String[]> ucpVillageInfo = request.getParameterMap();
        //封装成对应的对象
        UCPVillage ucpVillage = WebUtils.fillBean(ucpVillageInfo, UCPVillage.class);

        //处理错误信息
        String msg = "Update success.更新成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.updateUCPVillageInfo(ucpVillage);
        } catch (SQLException e) {
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("statusCode",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ： 查询所有自然村信息，注意导出功能配置
     * @Param request: http请求对象，需要从中获取分页功能数据，即当前页码(integer PageNo)
     *                 与当前页显示数据量(integer PageSize)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void queryAllUCPVillageInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取页码
        Integer pageNo = WebUtils.getPageNo(request);
        //获取当前页显示数量
        Integer pageSize = WebUtils.getPageSize(request);

        //定义成功消息
        String msg = "Success";
        //定义成功代码
        int code = 0;
        List<HashMap<String, Object>> ucp_village_all = null;
        List<HashMap<String, Object>> ucp_village_page = null;
        try {
            ucp_village_page = baseInfoService.queryAllUCPVillageInfo(pageNo, pageSize);;
            ucp_village_all = baseInfoService.queryAllUCPVillageInfo(null, null);
        } catch (SQLException | ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",ucp_village_all);
        map.put("data",ucp_village_page);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据条件查询自然村信息，注意导出功能配置
     * @Param request: http请求对象，需要从中获取查询条件（Map<String,String> QueryParams）
     *                 与分页功能数据，即当前页码(integer PageNo)与当前页显示数据量(integer PageSize)
     *                 ，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void querySomeUCPVillageInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取页码
        Integer pageNo = WebUtils.getPageNo(request);
        //获取当前页显示数量
        Integer pageSize = WebUtils.getPageSize(request);

        //获取前端传来的查询参数
        Map<String, String[]> request_map = request.getParameterMap();

        //定义成功消息
        String msg = "Success";
        //定义成功代码
        int code = 0;
        List<HashMap<String, Object>> ucp_village_all = null;
        List<HashMap<String, Object>> ucp_village_page = null;
        try {
            ucp_village_page = baseInfoService.querySomeUCPVillageInfo(request_map, pageNo, pageSize);
            ucp_village_all = baseInfoService.querySomeUCPVillageInfo(request_map,null, null);
        } catch (SQLException | ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",ucp_village_all);
        map.put("data",ucp_village_page);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据自然村编号查询自然村信息，注意导出功能配置
     * @Param request: http请求对象，需要从中获取需要查询的村庄编号(int UCPVillageId)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void queryUCPVillageInfoByUCPVillageId(HttpServletRequest request,HttpServletResponse response){

    }

     /*
      * @Description ：根据自然村编号查询自然村图片
      * @Param request: http请求对象，需要从中获取需要查询的村庄编号(int UCPVillageId)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/22 11:13
      */
    public void queryUCPVillagePhoto(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        String ucp_village_id_str = request.getParameter("ucp_village_id");

        int ucp_village_id = -1;
        //判空处理
        if(ucp_village_id_str!=null){
            if(!ucp_village_id_str.equals("")){
                ucp_village_id = Integer.parseInt(ucp_village_id_str);
            }
        }

        //提交相关service处理
        //定义成功消息
        String msg = "Success";
        //定义成功状态码
        int code = 0;
        List<HashMap<String, Object>> hashMapList  = null;
        try {
            hashMapList = baseInfoService.queryUCPVillagePhotoByUCPVillageId(ucp_village_id);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",hashMapList);
        map.put("data",hashMapList);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

     /*
      * @Description ：根据自然村编号打包该自然村图片，并下载
      * @Param request: http请求对象，需要从中获取需要查询的村庄编号(int UCPVillageId)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/26 10:49
      */
    public void downloadUcpVillagePhoto(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        String ucp_village_id_str = request.getParameter("ucp_village_id");
        int ucp_village_id = -1;
        if(ucp_village_id_str!=null){
            ucp_village_id = (ucp_village_id_str.equals("")) ? -1 : Integer.parseInt(ucp_village_id_str);
        }

        File file = null;
        String zipFileSavePath = null;
        try {
            /*根据自然村编号获取自然村图片，并压缩，得到压缩成功后的文件地址*/
            zipFileSavePath
                    = baseInfoService.ZipUCPVillagePhotoByUCPVillageId(ucp_village_id);

            if(zipFileSavePath != null){
                //完成了压缩，将文件流输出给前端完成下载
                /*设置文件名*/
                file = new File(zipFileSavePath);
                //处理文件名乱码问题
                String fileDownloadName = URLEncoder.encode(file.getName(), "UTF-8");
                /*设置响应格式*/
                response.setContentType("application/octet-stream; charset=utf-8");
                response.setHeader("Content-Disposition","attachment;filename="+fileDownloadName);
                response.setContentLength((int) file.length());
                /*开始写入流*/
                FileInputStream fis = new FileInputStream(file);
                byte[] buffer = new byte[128];
                int count = 0;
                ServletOutputStream outputStream = response.getOutputStream();
                while ((count = fis.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, count);
                }
                fis.close();
                outputStream.close();
            }else {
                throw new Exception("文件压缩失败");
            }
        }
        catch (SQLException e) {
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }
        catch (IOException e) {
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }
        catch (ClassNotFoundException e) {
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }
        catch (Exception e){
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }finally {
            //下载完成后删除相关文件
            if(file.exists()){
                FileUtils.deleteIfExists(zipFileSavePath);
            }
        }
    }

     /*
      * @Description ：根据自然村编号与需要删除的图片编号删除该图片
      * @Param request: http请求对象，需要从中获取自然村编号(int UCPVillageId)
      *                 与需要删除的图片编号(int PhotoId)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/26 11:06
      */
    public void deleteUCPVillagePhotoByUCPVillageIdAndPhotoId(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        /*图片编号*/
        String photo_id_str = request.getParameter("photoID");
        int photoID = -1;
        if(photo_id_str!=null){
            photoID = (photo_id_str.equals("")) ? -1 : Integer.parseInt(photo_id_str);
        }
        /*自然村编号*/
        String ucp_village_id_str = request.getParameter("ucp_village_id");
        int ucp_village_id = -1;
        if(ucp_village_id_str!=null){
            ucp_village_id = (ucp_village_id_str.equals("")) ? -1 : Integer.parseInt(ucp_village_id_str);
        }

        //提交相关service处理
        //定义成功消息
        String msg = "Success";
        //定义成功状态码
        int code = 0;
        List<HashMap<String, Object>> hashMapList  = null;
        try {
            hashMapList = baseInfoService.queryUCPVillagePhotoByUCPVillageId(ucp_village_id);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",hashMapList);
        map.put("data",hashMapList);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

     /*
      * @Description ：根据行政村id查询所有该行政村下所有自然村的信息
      * @Param request: http请求对象，需要从中获取行政村编号(int villageId)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/30 17:42
      */
    public void queryUCPVillageByVillageID(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        /*行政村编号*/
        String village_id_str = request.getParameter("village_id");
        int village_id = -1;
        if(village_id_str!=null){
            village_id = (village_id_str.equals("")) ? -1 : Integer.parseInt(village_id_str);
        }

        //提交相关service处理
        //定义成功消息
        String msg = "Success";
        //定义成功状态码
        int code = 0;
        List<HashMap<String, Object>> hashMapList  = null;
        try {
            hashMapList = baseInfoService.queryUCPVillageByVillageID(village_id);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",hashMapList);
        map.put("data",hashMapList);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);

    }

    //处理双联户基本信息相关业务------------------------------------------------------------------//
    //新增一个双联户addAnUnionFamily(UnionFamilyInfo)
    //根据双联户编号删除一个双联户deleteAnUnionFamilyByUnionFamilyID(UnionFamilyID)
    //修改双联户基础信息updateUnionFamilyBaseInfo(UnionFamilyInfo)
    //查询所有双联户信息queryAllUnionFamilyInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询双联户信息querySomeUnionFamilyInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据双联户编号查询双联户信息queryUnionFamilyInfoByUnionFamilyID(UnionFamilyId)，注意导出功能配置
    //根据双联户编号为双联户绑定图片bindUnionFamilyPhotoByUnionFamilyID(UnionFamilyID)
    //根据双联户编号查询双联户户长图片queryUnionFamilyLeaderPhotoByUnionFamilyID(UnionFamilyID)
    //根据双联户编号与需要删除的图片编号删除该图片deleteUFLeaderPhotoByUFIDAndPhotoID(UnionFamilyID,PhotoId)
    //根据双联户编号打包该双联户图片，并下载downloadUFLeaderPhotoByUFID(UnionFamilyID)
    // 根据行政村id查询所有该行政村下所有双联户的信息queryUnionFamilesByVillageID(village_id)

    /*
     * @Description ：新增一个双联户
     * @Param request: http请求对象，需要从中获取新的需要添加的双联户信息，
     *                 将其封装为UnionFamily对象(UnionFamily UnionFamilyInfo)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void addAnUnionFamily(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        Map<String, String[]> UnionFamilyInfo = request.getParameterMap();
        //封装成对应的对象
        UnionFamily unionFamily = WebUtils.fillBean(UnionFamilyInfo, UnionFamily.class);

        //处理错误信息
        String msg = "Add success.新增成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.addAnUnionFamily(unionFamily);
        } catch (SQLException | ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("statusCode",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据双联户编号为双联户绑定图片
     * @Param request: http请求对象，需要从中上传的文件对象
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/18 21:59
     */
    public void bindUnionFamilyLeaderPhotoByUnionFamilyID(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");
        //这种方式不能获取到参数，上传的参数需要对表单数据做处理才能获取，详见下方迭代表单数据里的操作
        //String ucp_village_id = request.getParameter("ucp_village_id");

        Gson gson = new Gson();
        //定义上传文件的相对路径
        String relativePath = "";
        //定义上传文件的完整路径
        String savePath = "";
        //获取请求中需要上传的文件名字（包含后缀名）
        String fileName = "";
        //定义收到的非文件参数
        String unionfamily_id = "";
        String uf_leader_name = "";
        //定义成功时返回前端的信息msg
        String msg = "上传成功";
        //定义成功时的状态码code
        int code = 1;
        try {
            /*上传代码*/
            // 配置上传参数
            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setHeaderEncoding("UTF-8");
            // 解析请求的内容提取文件数据
            List<FileItem> formItems = upload.parseRequest(request);
            // 迭代表单数据
            //找其他附属信息
            for (FileItem item : formItems) {
                //首先获取自然村编号
                if(item.isFormField()){
                    //不是文件，则是其他附属信息
                    if(item.getFieldName().equals("unionfamily_id")){
                        //该参数则为自然村编号
                        unionfamily_id = item.getString();
                    }

                    if(item.getFieldName().equals("uf_leader_name")){
                        //该参数则为自然村编号
                        //表单内容 + 处理乱码
                        uf_leader_name = item.getString("UTF-8");
                    }
                }
            }
            //找上传的文件
            int photoCountIndex = 1;
            //读取本系统文件存放的路径
            String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                    "fileSaveDocPathWindows",
                    String.class);
            //读取所有图片存放的路径
            String picture_path = PropertiedUtils.getValue("system.properties",
                    "photoSaveDocPathWindows",
                    String.class);
            //读取双联户图片存放的路径
            String unionfamily_photo_path = PropertiedUtils.getValue("system.properties",
                    "photoSaveDocPathWindowsUnionFamilyLeader", String.class);

            for (FileItem item : formItems) {
                if (!item.isFormField()) {
                    //是文件，这里就是需要上传的文件了
                    //获取请求中需要上传的文件名字（包含后缀名）
                    fileName = item.getName();
                    //获取后缀名标记的索引
                    int index = fileName.lastIndexOf(File.separator);
                    if (index != -1) {
                        //从文件名中获取除了后缀名之外的真正文件名
                        fileName = fileName.substring(index + 1);
                    }

                    //定义上传文件的相对路径
                    relativePath = picture_path + unionfamily_photo_path
                            + File.separator + unionfamily_id;

                    //定义上传文件的真实存放路径
                    String realPath = fileSaveDocPathWindows +  relativePath;

                    //检验上传文件夹是否存在
                    File photoDirectory = new File(realPath);
                    if(!photoDirectory.exists()){
                        //文件夹不存在，创建文件夹
                        photoDirectory.mkdirs();
                    }

                    //修改文件名，添加双联户编号
                    fileName = "【"+unionfamily_id + "】" + uf_leader_name + "-" + fileName;

                    //定义上传文件的完整路径
                    savePath = String.format("%s"+File.separator+"%s", realPath, fileName);

                    //判断文件是否已经存在，不存在才能上传，存在的话就不用上传了
                    if(!new File(savePath).exists()){
                        //不存在
                        //新建文件处理对象
                        File storeFile = new File(savePath);
                        // 保存文件到硬盘
                        item.write(storeFile);
                        /*数据库记录代码*/
                        baseInfoService.bindUnionFamilyLeaderPhotoByUnionFamilyID(
                                String.format("%s"+File.separator+"%s", relativePath, fileName),
                                unionfamily_id);
                    }else {
                        throw new Exception("文件名已经存在，请重新选择!");
                    }

                }
                photoCountIndex++;
            }
        }
        catch (UnsupportedEncodingException e) {
            code = -1;
            msg = e.getMessage()+"#编码格式错误";
        }
        catch (IOException e) {
            code = -1;
            msg = e.getMessage();
        }
        catch (ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }
        catch (FileUploadException e) {
            code = -1;
            msg = e.getMessage();
        }
        catch (SQLException e){
            code = -1;
            msg = e.getMessage();
        }
        catch (Exception e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> result = new HashMap<>();
        result.put("filepath",relativePath);
        result.put("msg",msg);
        result.put("code",code);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(result);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据双联户编号删除一个双联户
     * @Param request: http请求对象，需要从中获取需要删除的双联户编号(int UnionFamilyID)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void deleteAnUnionFamilyByUnionFamilyID(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");
        //获取参数
        String unionfamily_id = request.getParameter("unionfamily_id");

        //处理错误信息
        String msg = "Delete success.删除成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.deleteAnUnionFamilyByUnionFamilyID(unionfamily_id);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        } catch (FileOperateException e) {
            code = -1;
            msg = e.getMessage();
        } catch (IOException e) {
            code = -1;
            msg = e.getMessage();
        } catch (ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        } catch (Exception e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ： 修改双联户基础信息
     * @Param request: http请求对象,需要从中获取新的需要修改的双联户信息
     *                 ，将其封装为UnionFamily对象(UnionFamily UnionFamilyInfo)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void updateUnionFamilyBaseInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        Map<String, String[]> unionFamilyBaseInfo = request.getParameterMap();
        //封装成对应的对象
        UnionFamily unionFamily = WebUtils.fillBean(unionFamilyBaseInfo, UnionFamily.class);

        //处理错误信息
        String msg = "Update success.更新成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.updateUnionFamilyBaseInfo(unionFamily);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("statusCode",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ： 查询所有双联户信息，注意导出功能配置
     * @Param request: http请求对象，需要从中获取分页功能数据，即当前页码(integer PageNo)
     *                 与当前页显示数据量(integer PageSize)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void queryAllUnionFamilyInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取页码
        Integer pageNo = WebUtils.getPageNo(request);
        //获取当前页显示数量
        Integer pageSize = WebUtils.getPageSize(request);

        //定义成功消息
        String msg = "Success";
        //定义成功代码
        int code = 0;
        List<HashMap<String, Object>> unionfamily_all = null;
        List<HashMap<String, Object>> unionfamily_page = null;
        try {
            unionfamily_page = baseInfoService.queryAllUnionFamilyInfo(pageNo, pageSize);;
            unionfamily_all = baseInfoService.queryAllUnionFamilyInfo(null, null);
        } catch (SQLException | ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",unionfamily_all);
        map.put("data",unionfamily_page);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据条件查询双联户信息，注意导出功能配置
     * @Param request: http请求对象，需要从中获取查询条件（Map<String,String> QueryParams）
     *                 与分页功能数据，即当前页码(integer PageNo)与当前页显示数据量(integer PageSize)
     *                 ，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void querySomeUnionFamilyInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取页码
        Integer pageNo = WebUtils.getPageNo(request);
        //获取当前页显示数量
        Integer pageSize = WebUtils.getPageSize(request);

        //获取前端传来的查询参数
        Map<String, String[]> request_map = request.getParameterMap();

        //定义成功消息
        String msg = "Success";
        //定义成功代码
        int code = 0;
        List<HashMap<String, Object>> unionfamily_all = null;
        List<HashMap<String, Object>> unionfamily_page = null;
        try {
            unionfamily_all = baseInfoService.querySomeUnionFamilyInfo(request_map,null, null);
            unionfamily_page = baseInfoService.querySomeUnionFamilyInfo(request_map, pageNo, pageSize);
        } catch (SQLException | ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",unionfamily_all);
        map.put("data",unionfamily_page);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据双联户编号查询双联户信息，注意导出功能配置
     * @Param request: http请求对象，需要从中获取需要查询的双联户编号(int UnionFamilyID)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void queryUnionFamilyInfoByUnionFamilyID(HttpServletRequest request,HttpServletResponse response){

    }

    /*
     * @Description ：根据双联户编号查询双联户图片
     * @Param request: http请求对象，需要从中获取需要查询的双联户编号(int UnionFamilyID)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/22 11:13
     */
    public void queryUnionFamilyLeaderPhotoByUnionFamilyID(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        String unionfamily_id = request.getParameter("unionfamily_id");

        //提交相关service处理
        //定义成功消息
        String msg = "Success";
        //定义成功状态码
        int code = 0;
        List<HashMap<String, Object>> hashMapList  = null;
        try {
            hashMapList = baseInfoService.queryUnionFamilyLeaderPhotoByUnionFamilyID(unionfamily_id);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",hashMapList);
        map.put("data",hashMapList);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据双联户编号打包该双联户图片，并下载
     * @Param request: http请求对象，需要从中获取需要查询的双联户编号(int UnionFamilyID)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/26 10:49
     */
    public void downloadUFLeaderPhotoByUFID(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        String unionfamily_id = request.getParameter("unionfamily_id");
        File file = null;
        String zipFileSavePath = null;
        try {
            /*根据双联户联户编号获取双联户户长村图片，并压缩，得到压缩成功后的文件地址*/
            zipFileSavePath
                    = baseInfoService.ZipUnionFamilyLeaderPhotoByUnionFamilyID(unionfamily_id);

            if(zipFileSavePath != null){
                //完成了压缩，将文件流输出给前端完成下载
                /*设置文件名*/
                file = new File(zipFileSavePath);
                //处理文件名乱码问题
                String fileDownloadName = URLEncoder.encode(file.getName(), "UTF-8");
                /*设置响应格式*/
                response.setContentType("application/octet-stream; charset=utf-8");
                response.setHeader("Content-Disposition","attachment;filename="+fileDownloadName);
                response.setContentLength((int) file.length());
                /*开始写入流*/
                FileInputStream fis = new FileInputStream(file);
                byte[] buffer = new byte[128];
                int count = 0;
                ServletOutputStream outputStream = response.getOutputStream();
                while ((count = fis.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, count);
                }
                fis.close();
                outputStream.close();
            }else {
                throw new Exception("文件压缩失败");
            }
        }
        catch (SQLException e) {
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }
        catch (IOException e) {
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }
        catch (ClassNotFoundException e) {
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }
        catch (Exception e){
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }finally {
            //下载完成后删除相关文件
            if(file.exists()){
                FileUtils.deleteIfExists(zipFileSavePath);
            }
        }
    }

    /*
     * @Description ：根据双联户编号与需要删除的图片编号删除该图片
     * @Param request: http请求对象，需要从中获取双联户编号(int UnionFamilyID)
     *                 与需要删除的图片编号(int PhotoId)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/26 11:06
     */
    public void deleteUFLeaderPhotoByUFIDAndPhotoID(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        /*图片编号*/
        String photo_id_str = request.getParameter("photoID");
        int photoID = -1;
        if(photo_id_str!=null){
            photoID = (photo_id_str.equals("")) ? -1 : Integer.parseInt(photo_id_str);
        }
        /*双联户编号*/
        String unionfamily_id = request.getParameter("unionfamily_id");

        //处理错误信息
        String msg = "Delete success.删除成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.deleteUnionFamilyLeaderPhotoByUnionFamilyIDAndPhotoId(unionfamily_id,photoID);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        } catch (ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        } catch (FileOperateException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据行政村id查询所有该行政村下所有双联户的信息
     * @Param request: http请求对象，需要从中获取行政村编号(int villageId)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/30 17:42
     */
    public void queryUnionFamilesByVillageID(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        /*行政村编号*/
        String village_id_str = request.getParameter("village_id");
        int village_id = -1;
        if(village_id_str!=null){
            village_id = (village_id_str.equals("")) ? -1 : Integer.parseInt(village_id_str);
        }

        //提交相关service处理
        //定义成功消息
        String msg = "Success";
        //定义成功状态码
        int code = 0;
        List<HashMap<String, Object>> hashMapList  = null;
        try {
            hashMapList = baseInfoService.queryUnionFamilesByVillageID(village_id);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",hashMapList);
        map.put("data",hashMapList);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);

    }


    //处理家庭基本信息相关业务-------------------------------------------------------------------//
    //新增一个家庭户addAFamily(FamilyInfo)
    //根据家庭编号删除一个家庭deleteAFamilyByFamilyNum(FamilyNum)
    //修改家庭基础信息updateFamilyBaseInfo(FamilyInfo)
    //查询所有家庭信息queryAllFamilyInfo(PageNo,PageSize)，注意导出功能配置
    //根据条件查询家庭信息querySomeFamilyInfo(QueryParams,PageNo,PageSize)，注意导出功能配置
    //根据家庭编号查询家庭信息queryFamilyInfoByFamilyNum(FamilyNum)，注意导出功能配置
    //根据家庭编号为其绑定户口本首页图片bindFamilyRBPhotoByFamilyNum(FamilyNum)
    //根据家庭编号查询户口本首页图片queryFamilyRBPhotoByFamilyNum(FamilyNum)
    //根据家庭编号与需要删除的户口本首页图片编号删除该图片deleteFamilyRBPhotoByFamilyNumAndPhotoID(FamilyNum,PhotoId)
    //根据家庭编号打包该家庭图片，并下载downloadFamilyRBPhotoByFamilyNum(FamilyNum)

     /*
      * @Description ：新增一个家庭户
      * @Param request: http请求对象，需要从中获取新的需要添加的双联户信息，
      *                 将其封装为Family对象(Family FamilyInfo)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/29 22:27
      */
    public void addAFamily(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        Map<String, String[]> familyInfo = request.getParameterMap();
        //封装成对应的对象
        Family family = WebUtils.fillBean(familyInfo, Family.class);

        //处理错误信息
        String msg = "Add success.新增成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.addAFamily(family);
        } catch (SQLException | ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("statusCode",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

     /*
      * @Description ：根据家庭编号为其绑定户口本首页图片
      * @Param request: http请求对象，需要从中上传的文件对象
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/29 22:28
      */
    public void bindFamilyRBPhotoByFamilyNum(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");
        //这种方式不能获取到参数，上传的参数需要对表单数据做处理才能获取，详见下方迭代表单数据里的操作
        //String ucp_village_id = request.getParameter("ucp_village_id");

        Gson gson = new Gson();
        //定义上传文件的相对路径
        String relativePath = "";
        //定义上传文件的完整路径
        String savePath = "";
        //获取请求中需要上传的文件名字（包含后缀名）
        String fileName = "";
        //定义收到的非文件参数
        String family_num = "";
        String family_name = "";
        //定义成功时返回前端的信息msg
        String msg = "上传成功";
        //定义成功时的状态码code
        int code = 1;
        try {
            /*上传代码*/
            // 配置上传参数
            DiskFileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);
            upload.setHeaderEncoding("UTF-8");
            // 解析请求的内容提取文件数据
            List<FileItem> formItems = upload.parseRequest(request);
            // 迭代表单数据
            //找其他附属信息
            for (FileItem item : formItems) {
                //首先获取自然村编号
                if(item.isFormField()){
                    //不是文件，则是其他附属信息
                    if(item.getFieldName().equals("family_num")){
                        //该参数则为自然村编号
                        family_num = item.getString();
                    }

                    if(item.getFieldName().equals("family_name")){
                        //该参数则为自然村编号
                        //表单内容 + 处理乱码
                        family_name = item.getString("UTF-8");
                    }
                }
            }
            //找上传的文件
            int photoCountIndex = 1;
            //读取本系统文件存放的路径
            String fileSaveDocPathWindows = PropertiedUtils.getValue("system.properties",
                    "fileSaveDocPathWindows",
                    String.class);
            //读取所有图片存放的路径
            String picture_path = PropertiedUtils.getValue("system.properties",
                    "photoSaveDocPathWindows",
                    String.class);
            //读取双联户图片存放的路径
            String family_residence_booklet_photo_path = PropertiedUtils.getValue("system.properties",
                    "photoSaveDocPathWindowsFamilyResidenceBooklet", String.class);

            for (FileItem item : formItems) {
                if (!item.isFormField()) {
                    //是文件，这里就是需要上传的文件了
                    //获取请求中需要上传的文件名字（包含后缀名）
                    fileName = item.getName();
                    //获取后缀名标记的索引
                    int index = fileName.lastIndexOf(File.separator);
                    if (index != -1) {
                        //从文件名中获取除了后缀名之外的真正文件名
                        fileName = fileName.substring(index + 1);
                    }

                    //定义上传文件的相对路径
                    relativePath = picture_path + family_residence_booklet_photo_path
                            + File.separator + family_num;

                    //定义上传文件的真实存放路径
                    String realPath = fileSaveDocPathWindows +  relativePath;

                    //检验上传文件夹是否存在
                    File photoDirectory = new File(realPath);
                    if(!photoDirectory.exists()){
                        //文件夹不存在，创建文件夹
                        photoDirectory.mkdirs();
                    }

                    //修改文件名，添加双联户编号
                    fileName = "【"+family_num + "】" + family_name + "-" + fileName;

                    //定义上传文件的完整路径
                    savePath = String.format("%s"+File.separator+"%s", realPath, fileName);

                    //判断文件是否已经存在，不存在才能上传，存在的话就不用上传了
                    if(!new File(savePath).exists()){
                        //不存在
                        //新建文件处理对象
                        File storeFile = new File(savePath);
                        // 保存文件到硬盘
                        item.write(storeFile);
                        /*数据库记录代码*/
                        baseInfoService.bindFamilyRBPhotoByFamilyNum(
                                String.format("%s"+File.separator+"%s", relativePath, fileName),
                                family_num);
                    }else {
                        throw new Exception("文件名已经存在，请重新选择!");
                    }

                }
                photoCountIndex++;
            }
        }
        catch (UnsupportedEncodingException e) {
            code = -1;
            msg = e.getMessage()+"#编码格式错误";
        }
        catch (IOException e) {
            code = -1;
            msg = e.getMessage();
        }
        catch (ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }
        catch (FileUploadException e) {
            code = -1;
            msg = e.getMessage();
        }
        catch (SQLException e){
            code = -1;
            msg = e.getMessage();
        }
        catch (Exception e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> result = new HashMap<>();
        result.put("filepath",relativePath);
        result.put("msg",msg);
        result.put("code",code);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(result);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

     /*
      * @Description ：根据家庭编号删除一个家庭
      * @Param request: http请求对象，需要从中获取需要删除的双联户编号(String FamilyNum)，注意处理乱码问题
      * @Param response: http响应对象，注意返回前端可能出现的乱码问题
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/29 22:28
      */
    public void deleteAFamilyByFamilyNum(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");
        //获取参数
        String family_num = request.getParameter("family_num");

        //处理错误信息
        String msg = "Delete success.删除成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.deleteAFamilyByFamilyNum(family_num);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        } catch (FileOperateException e) {
            code = -1;
            msg = e.getMessage();
        } catch (IOException e) {
            code = -1;
            msg = e.getMessage();
        } catch (ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        } catch (Exception e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ： 修改家庭基础信息
     * @Param request: http请求对象,需要从中获取新的需要修改的家庭信息，将其封装为Family对象(Family FamilyInfo)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void updateFamilyBaseInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        Map<String, String[]> familyBaseInfo = request.getParameterMap();
        //封装成对应的对象
        Family family = WebUtils.fillBean(familyBaseInfo, Family.class);

        System.out.println(family);

        //处理错误信息
        String msg = "Update success.更新成功";
        //提交相关service处理
        int code = -1;
        try {
            code = baseInfoService.updateFamilyBaseInfo(family);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("statusCode",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ： 查询所有家庭信息，注意导出功能配置
     * @Param request: http请求对象，需要从中获取分页功能数据，即当前页码(integer PageNo)
     *                 与当前页显示数据量(integer PageSize)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void queryAllFamilyInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取页码
        Integer pageNo = WebUtils.getPageNo(request);
        //获取当前页显示数量
        Integer pageSize = WebUtils.getPageSize(request);

        //定义成功消息
        String msg = "Success";
        //定义成功代码
        int code = 0;
        List<HashMap<String, Object>> family_page = null;
        List<HashMap<String, Object>> family_all = null;
        try {
            family_page = baseInfoService.queryAllFamilyInfo(pageNo, pageSize);;
            family_all = baseInfoService.queryAllFamilyInfo(null, null);
        } catch (SQLException | ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",family_all);
        map.put("data",family_page);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据条件查询家庭信息，注意导出功能配置
     * @Param request: http请求对象，需要从中获取查询条件（Map<String,String> QueryParams）
     *                 与分页功能数据，即当前页码(integer PageNo)与当前页显示数据量(integer PageSize)
     *                 ，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void querySomeFamilyInfo(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取页码
        Integer pageNo = WebUtils.getPageNo(request);
        //获取当前页显示数量
        Integer pageSize = WebUtils.getPageSize(request);

        //获取前端传来的查询参数
        Map<String, String[]> request_map = request.getParameterMap();

        //定义成功消息
        String msg = "Success";
        //定义成功代码
        int code = 0;
        List<HashMap<String, Object>> family_all = null;
        List<HashMap<String, Object>> family_page = null;
        try {
            family_all = baseInfoService.querySomeFamilyInfo(request_map,null, null);
            family_page = baseInfoService.querySomeFamilyInfo(request_map, pageNo, pageSize);
        } catch (SQLException | ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",family_all);
        map.put("data",family_page);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据家庭编号查询家庭信息，注意导出功能配置
     * @Param request: http请求对象，需要从中获取需要查询的家庭编号(String FamilyNum)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/17 18:46
     */
    public void queryFamilyInfoByFamilyNum(HttpServletRequest request,HttpServletResponse response){

    }

    /*
     * @Description ：根据家庭编号查询户口本首页图片
     * @Param request: http请求对象，需要从中获取需要查询的家庭编号(String FamilyNum)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/22 11:13
     */
    public void queryFamilyRBPhotoByFamilyNum(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        String family_num = request.getParameter("family_num");

        //提交相关service处理
        //定义成功消息
        String msg = "Success";
        //定义成功状态码
        int code = 0;
        List<HashMap<String, Object>> hashMapList  = null;
        try {
            hashMapList = baseInfoService.queryFamilyRBPhotoByFamilyNum(family_num);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);
        map.put("count",hashMapList);
        map.put("data",hashMapList);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    /*
     * @Description ：根据家庭编号打包该家庭图片，并下载
     * @Param request: http请求对象，需要从中获取需要查询的家庭编号(int FamilyID)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/26 10:49
     */
    public void downloadFamilyRBPhotoByFamilyNum(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        String family_num = request.getParameter("family_num");
        File file = null;
        String zipFileSavePath = null;
        try {
            /*根据双联户联户编号获取双联户户长村图片，并压缩，得到压缩成功后的文件地址*/
            zipFileSavePath
                    = baseInfoService.ZipFamilyRBPhotoByFamilyNum(family_num);

            if(zipFileSavePath != null){
                //完成了压缩，将文件流输出给前端完成下载
                /*设置文件名*/
                file = new File(zipFileSavePath);
                //处理文件名乱码问题
                String fileDownloadName = URLEncoder.encode(file.getName(), "UTF-8");
                /*设置响应格式*/
                response.setContentType("application/octet-stream; charset=utf-8");
                response.setHeader("Content-Disposition","attachment;filename="+fileDownloadName);
                response.setContentLength((int) file.length());
                /*开始写入流*/
                FileInputStream fis = new FileInputStream(file);
                byte[] buffer = new byte[128];
                int count = 0;
                ServletOutputStream outputStream = response.getOutputStream();
                while ((count = fis.read(buffer)) > 0) {
                    outputStream.write(buffer, 0, count);
                }
                fis.close();
                outputStream.close();
            }else {
                throw new Exception("文件压缩失败");
            }
        }
        catch (SQLException e) {
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }
        catch (IOException e) {
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }
        catch (ClassNotFoundException e) {
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }
        catch (Exception e){
            //封装成json字符串，通过getWriter().write()返回给页面
            Map<String,Object> map = new HashMap<>();
            map.put("code",-1);
            map.put("msg",e.getMessage());
            //将结果集转换为json格式
            String result_json = new Gson().toJson(map);
            //解决post请求方式响应中文乱码问题
            response.setContentType("text/html;charset=utf-8");
            //通过getWriter().write()返回给页面
            response.getWriter().write(result_json);
        }finally {
            //下载完成后删除相关文件
            if(file.exists()){
                FileUtils.deleteIfExists(zipFileSavePath);
            }
        }
    }

    /*
     * @Description ：根据家庭编号与需要删除的户口本首页图片编号删除该图片
     * @Param request: http请求对象，需要从中获取家庭编号(String FamilyNum)
     *                 与需要删除的图片编号(int PhotoID)，注意处理乱码问题
     * @Param response: http响应对象，注意返回前端可能出现的乱码问题
     * @Return ：void
     * @Author: CTF
     * @Date ：2022/3/26 11:06
     */
    public void deleteFamilyRBPhotoByFamilyNumAndPhotoID(HttpServletRequest request,HttpServletResponse response) throws IOException {
        //解决post请求方式获取请求参数的中文乱码问题
        request.setCharacterEncoding("utf-8");

        //获取参数
        /*图片编号*/
        String photo_id_str = request.getParameter("photoID");
        int photoID = -1;
        if(photo_id_str!=null){
            photoID = (photo_id_str.equals("")) ? -1 : Integer.parseInt(photo_id_str);
        }
        /*双联户编号*/
        String family_num = request.getParameter("family_num");

        //处理错误信息
        String msg = "Delete success.删除成功";
        //提交相关service处理
        int code = 1;
        try {
            code = baseInfoService.deleteFamilyRBPhotoByFamilyNumAndPhotoID(family_num,photoID);
        } catch (SQLException e) {
            code = -1;
            msg = e.getMessage();
        } catch (ClassNotFoundException e) {
            code = -1;
            msg = e.getMessage();
        } catch (FileOperateException e) {
            code = -1;
            msg = e.getMessage();
        }

        //封装成json字符串，通过getWriter().write()返回给页面
        Map<String,Object> map = new HashMap<>();
        map.put("code",code);
        map.put("msg",msg);

        //将结果集转换为json格式
        String result_json = new Gson().toJson(map);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

    //处理人员基本信息相关业务------------------------------------------------------------------//

}
