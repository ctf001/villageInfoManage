package com.ctf.web.Servlet;

import com.ctf.bean.Role;
import com.ctf.bean.User;
import com.ctf.dao.impl.UserDaoImpl;
import com.ctf.service.impl.LoginServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/loginServlet")
public class LoginServlet extends BaseServlet{

    private LoginServiceImpl loginService = new LoginServiceImpl();
    private UserDaoImpl userDao = new UserDaoImpl();
    //处理登录功能
    public void login(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String username = request.getParameter("username");
        String password = request.getParameter("password");

        User user = null;
        try {
            user = loginService.login(username, password);
            if(user != null){
                //用户存在，可以登录
                //1.绑定session数据
                request.getSession().setAttribute("user",user);
                //2.获取用户角色信息
                Role role = userDao.queryRoleInfoByUserId(user.getId());
                //3.重定向到主页面
                //根据角色分配到指定页面（暂无角色区分）
                response.sendRedirect(request.getContextPath()+"/pages/common/main.jsp");

            }else {
                //登录失败,回到登录页面，告知用户
                request.setAttribute("message","用户名或密码错误，请重新输入");
                request.getRequestDispatcher("/login.jsp").forward(request,response);
            }
        } catch (SQLException e) {
            //登录失败,回到登录页面，告知用户
            request.setAttribute("message","系统错误:"+e.getMessage());
            request.getRequestDispatcher("/login.jsp").forward(request,response);

        }



    }

    //处理退出功能
    public void loginOut(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //1、销毁Session中用户登录的信息（或者销毁Session）
        request.getSession().invalidate();
        //2、重定向到首页（或登录页面）。
        response.sendRedirect(request.getContextPath()+"/login.jsp");
    }

}
