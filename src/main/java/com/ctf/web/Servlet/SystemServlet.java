package com.ctf.web.Servlet;

import com.ctf.utils.IDCreater;
import com.google.gson.Gson;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/systemServlet")
public class SystemServlet extends BaseServlet{

    public void createID(HttpServletRequest request, HttpServletResponse response) throws IOException {
        int id = IDCreater.getID();
        //将结果集转换为json格式
        String result_json = new Gson().toJson(id);
        //解决post请求方式响应中文乱码问题
        response.setContentType("text/html;charset=utf-8");
        //通过getWriter().write()返回给页面
        response.getWriter().write(result_json);
    }

}
