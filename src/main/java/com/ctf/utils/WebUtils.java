package com.ctf.utils;

import com.ctf.dao.impl.SystemDataDaoImpl;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.ConvertUtils;
import org.apache.commons.beanutils.Converter;

import javax.servlet.http.HttpServletRequest;
import java.text.SimpleDateFormat;
import java.util.*;

//导入可选配置类

// 导入对应SMS模块的client


// 导入要请求接口对应的request response类

// 导入要请求接口对应的request response类


/**
*@Description :
*@ClassName WebUtils
*@Author tianfeichen
*@Date 2021/8/21 16:12
*@Version v1.0
*/
public class WebUtils {
    private static final String FORMAT = "yyyy-MM-dd" ;
    private static SystemDataDaoImpl systemDataDao = new SystemDataDaoImpl();

    public static Integer parseInt(String str,Integer defaultValue){
        Integer value = Integer.parseInt(str);
        if(value != null){
            return value;
        }else {
            return defaultValue;
        }
    }

     /*
      * @Description ：通过请求解析页码
      * @Param request: 获取请求内容，由请求来解析页码
      * @Return ：Integer 页码
      * @Author: CTF
      * @Date ：2022/3/7 19:44
      */
    public static  Integer getPageNo(HttpServletRequest request){
        //处理分页
        String curr = request.getParameter("curr");
        //当前页码
        Integer pageNo = null;
        if(curr!=null){
            if(!curr.trim().equals("")){
                //有当前页码，需要处理
                pageNo =Integer.valueOf(curr);
            }
        }
        return pageNo;
    }

    //
     /*
      * @Description ：通过请求每页数据量
      * @Param request:获取请求内容，由请求来解析每页数据量
      * @Return ：Integer 每页数据量
      * @Author: CTF
      * @Date ：2022/3/7 19:45
      */
    public static Integer getPageSize(HttpServletRequest request){
        //处理分页
        String nums = request.getParameter("nums");
        //每页显示数量
        Integer pageSize = null;
        if(nums!=null){
            if(!nums.trim().equals("")){
                //有页码数据量，需要处理
                pageSize = Integer.valueOf(nums);
            }
        }
        return pageSize;
    }


    //打印hashMap的list
    public static void printHashMapList(List<HashMap<String, Object>> hashMaps){
        for(HashMap<String, Object> hashMap:hashMaps){
            for(Map.Entry<String, Object> map : hashMap.entrySet()){
                System.out.println("key="+map.getKey()+";  value="+map.getValue());
            }
        }
    }

    /**
     * @Name: fillBean
     * @Description: 将Map集合中的数据封装到JavaBean
     * 说明：
     *   Map的key：与属性名称保持一致；
     *   Map的value：设置为属性值；
     * @Author: XXX
     * @Version: V1.0
     * @CreateDate: XXX
     * @Parameters: @param map
     * @Parameters: @param clazz
     * @Return: T
     */
    public static <T> T fillBean(Map<String, String[]> map, Class<T> clazz) {
        T bean = null ;
        try {
            bean = clazz.newInstance() ;

            ConvertUtils.register(new Converter() {
                //修改第三方jar引入方法的参数时候，可以关联源码，ctrl选择该类，点进去，选择Attach source--->external file
                @Override
                public Object convert(Class type, Object value) {
                    //判断
                    if (value ==null ||"".equals(value.toString().trim())){
                        return null;
                    }
                    if (type !=Date.class){
                        return null;
                    }
                    try {
                        //字符串转换为日期
                        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
                        return sdf.parse(value.toString());
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    }
                }
            },Date.class);

            //将Map集合中的数据封装到JavaBean
            BeanUtils.populate(bean, map) ;
        } catch (Exception e) {
            throw new RuntimeException(e) ;
        }
        return bean ;
    }

    //将String类型的List转换为String[]
    public static String[] stringListTostringArray(List<String> stringList){
        String[] strings = new String[stringList.size()];
        int index = 0;
        for(String templateParam : stringList){
            strings[index++] = templateParam;
        }
        return strings;
    }

    //数据导出到excel
    public static void exportToExcel(){}

}
