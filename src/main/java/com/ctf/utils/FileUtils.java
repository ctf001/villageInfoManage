package com.ctf.utils;

import java.io.*;
import java.nio.channels.FileChannel;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;

 /**
  * @ClassName: FileUtilsNIO
  * @Package: com.ctf.utils
  * @Description： 使用java.nio完成文件或文件夹的相关操作
  * @Author: CTF
  * @Date：2022/3/21 22:41
  */
public class FileUtils {

      /*
       * @Description ：复制文件
       * @Param srcPath_str: 源路径：要复制哪些文件
       * @Param targetPath_str: 目标路径：复制这些文件到哪里去
       * @Return ：void
       * @Author: CTF
       * @Date ：2022/3/23 20:26
       */
    public static void copyFile(String srcPath_str,String targetPath_str) throws IOException {
        FileInputStream fileInputStream = null;
        FileOutputStream fileOutputStream = null;
        FileChannel fileChannelInput = null;
        FileChannel fileChannelOutput = null;

        File fromFile = new File(srcPath_str);
        File toFile = new File(targetPath_str);

        try {
            fileInputStream = new FileInputStream(fromFile);
            fileOutputStream = new FileOutputStream(toFile);
            // 得到fileInputStream的文件通道
            fileChannelInput = fileInputStream.getChannel();
            // 得到fileOutputStream的文件通道
            fileChannelOutput = fileOutputStream.getChannel();
            // 将fileChannelInput通道的数据，写入到fileChannelOutput通道
            fileChannelInput.transferTo(0, fileChannelInput.size(), fileChannelOutput);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (fileInputStream != null)
                    fileInputStream.close();
                if (fileChannelInput != null)
                    fileChannelInput.close();
                if (fileOutputStream != null)
                    fileOutputStream.close();
                if (fileChannelOutput != null)
                    fileChannelOutput.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

      /*
       * @Description ：复制文件夹
       * @Param sourcePathStr: 文件夹源路径，要复制哪个文件夹
       * @Param targetPathStr: 文件夹复制目标路径，要复制这个文件夹到哪儿去
       * @Param options:文件复制选项:
                            StandardCopyOption.REPLACE_EXISTING：如果目的路径有同名文件则替换
                            StandardCopyOption.ATOMIC_MOVE：若失败则回滚
                            StandardCopyOption.COPY_ATTRIBUTES：把源文件的文件属性一同复制给新文件(readAttributes等方法可以读取文件属性)
       * @Return ：void
       * @Author: CTF
       * @Date ：2022/3/23 19:24
       */
     public static void copyDir(String sourcePathStr, String targetPathStr, CopyOption... options) throws IOException{
         operateDir(false, sourcePathStr, targetPathStr, options);
     }

      /*
       * @Description ：移动文件夹
       * @Param sourcePathStr: 文件夹源路径，要复制哪个文件夹
       * @Param targetPathStr: 文件夹复制目标路径，要复制这个文件夹到哪儿去
       * @Param options:文件复制选项:
                            StandardCopyOption.REPLACE_EXISTING：如果目的路径有同名文件则替换
                            StandardCopyOption.ATOMIC_MOVE：若失败则回滚
                            StandardCopyOption.COPY_ATTRIBUTES：把源文件的文件属性一同复制给新文件(readAttributes等方法可以读取文件属性)
       * @Return ：void
       * @Author: CTF
       * @Date ：2022/3/23 19:24
       */
     public static void moveDir(String sourcePathStr, String targetPathStr, CopyOption... options) throws IOException{
         operateDir(true, sourcePathStr, targetPathStr, options);
     }

      /*
       * @Description ：复制/移动文件夹的核心操作，可以根据move这个操作标记来表示是移动文件夹还是复制文件夹
       * @Param move: 操作标记，为true时移动文件夹,否则为复制
       * @Param source: 要复制/移动的源文件夹
       * @Param target: 源文件夹要复制/移动到的目标文件夹
       * @Param options: 文件复制选项:
                            StandardCopyOption.REPLACE_EXISTING：如果目的路径有同名文件则替换
                            StandardCopyOption.ATOMIC_MOVE：若失败则回滚
                            StandardCopyOption.COPY_ATTRIBUTES：把源文件的文件属性一同复制给新文件(readAttributes等方法可以读取文件属性)
       * @Return ：void
       * @Author: CTF
       * @Date ：2022/3/23 19:20
       */
     public static void operateDir(boolean move,
                                   String sourcePathStr,
                                   String targetPathStr,
                                   CopyOption... options) throws IOException{

         Path source = Paths.get(sourcePathStr);
         Path target = Paths.get(targetPathStr);

         if(null==source||!Files.isDirectory(source)){
             throw new IllegalArgumentException("source must be directory");
         }

         Path dest = target.resolve(source.getFileName());

         // 如果相同则返回
         if(Files.exists(dest)&&Files.isSameFile(source, dest)){
             return;
         }

         // 目标文件夹不能是源文件夹的子文件夹
         if(isSub(source,dest)){
             throw new IllegalArgumentException("dest must not  be sub directory of source");
         }

         boolean clear=true;

         for(CopyOption option:options){
             if(StandardCopyOption.REPLACE_EXISTING==option){
                 clear=false;
                 break;
                 // 如果指定了REPLACE_EXISTING选项则不清除目标文件夹
             }
         }

         if(clear){
             deleteIfExists(targetPathStr);
         }

         Files.walkFileTree(source,
                 new SimpleFileVisitor<Path>() {

                     @Override
                     public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) throws IOException {
                         // 在目标文件夹中创建dir对应的子文件夹
                         Path subDir = 0==dir.compareTo(source)?dest:dest.resolve(dir.subpath(source.getNameCount(), dir.getNameCount()));
                         Files.createDirectories(subDir);
                         return FileVisitResult.CONTINUE;
                     }

                     @Override
                     public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                         if(move)
                             Files.move(file, dest.resolve(file.subpath(source.getNameCount(), file.getNameCount())),options);
                         else
                             Files.copy(file, dest.resolve(file.subpath(source.getNameCount(), file.getNameCount())),options);
                         return FileVisitResult.CONTINUE;
                     }

                     @Override
                     public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                         // 移动操作时删除源文件夹
                         if(move)
                             Files.delete(dir);
                         return super.postVisitDirectory(dir, exc);
                     }
                 });
     }

      /*
       * @Description ：判断sub是否与parent相等或在其之下，
       *                parent必须存在，且必须是directory,否则抛出{@link IllegalArgumentException}
       * @Param parent: 上层路径
       * @Param sub: 子路径
       * @Return ：boolean
       * @Author: CTF
       * @Date ：2022/3/23 19:18
       */
     public static boolean sameOrSub(Path parent,Path sub) throws IOException{
         if(null==parent)
             throw new NullPointerException("parent is null");
         if(!Files.exists(parent)||!Files.isDirectory(parent))
             throw new IllegalArgumentException(String.format("the parent not exist or not directory %s",parent));
         while(null!=sub) {
             if(Files.exists(sub)&&Files.isSameFile(parent, sub))
                 return true;
             sub=sub.getParent();
         }
         return false;
     }

      /*
       * @Description ：判断sub是否在parent之下的文件或子文件夹,
       *                parent必须存在，且必须是directory,否则抛出{@link IllegalArgumentException}
       * @Param parent: 上层路径
       * @Param sub: 子路径
       * @Return ：boolean
       * @throws IOException
       * @see {@link #sameOrSub(Path, Path)}
       * @Author: CTF
       * @Date ：2022/3/23 19:17
       */
     public static boolean isSub(Path parent,Path sub) throws IOException{
         return (null==sub)?false:sameOrSub(parent,sub.getParent());
     }

     /*
      * @Description ：强制删除文件/文件夹(含不为空的文件夹)
      * @Param path_str: 文件或文件夹路径
      * @Return ：boolean 删除成功true，失败返回false
      * @Author: CTF
      * @Date ：2022/3/21 22:39
      */
    public static boolean deleteIfExists(String path_str) throws IOException {
        boolean flag =  false;
        Path dir = Paths.get(path_str);
        try{
           flag = Files.deleteIfExists(dir);
        }
        catch (DirectoryNotEmptyException e) {
            Files.walkFileTree(dir, new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
                    Files.delete(file);
                    return FileVisitResult.CONTINUE;
                }

                @Override
                public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                    Files.delete(dir);
                    return super.postVisitDirectory(dir, exc);
                }
            });

            if(!Files.exists(dir)){
                //删除成功，该文件已不存在
                flag = true;
            }
        }
        return flag;
    }
}
