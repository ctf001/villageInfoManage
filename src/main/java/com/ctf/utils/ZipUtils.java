package com.ctf.utils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.zip.*;

public class ZipUtils {

    private static final int BUFFER = 8192;
    private File zipFile;

     /*
      * @Description ：压缩文件工具类的构造函数
      * @Param pathName: 压缩后的文件存放目录路径，在构造对象的时候便赋值
      * @Return ：null
      * @Author: CTF
      * @Date ：2022/3/23 20:54
      */
    public ZipUtils(String pathName) {
        zipFile = new File(pathName);
    }

     /*
      * @Description ：执行压缩操作
      * @Param srcPathName: 被压缩的文件/文件夹的路径
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/23 20:54
      */
    public void compressExe(String srcPathName) {
        File file = new File(srcPathName);
        if (!file.exists()){
            throw new RuntimeException(srcPathName + "不存在！");
        }
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(zipFile);
            CheckedOutputStream cos = new CheckedOutputStream(fileOutputStream,new CRC32());
            ZipOutputStream out = new ZipOutputStream(cos);


            String basedir = "";
            compressByType(file, out, basedir);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
            //logger.error("执行压缩操作时发生异常:"+e);
            throw new RuntimeException(e);
        }
    }

    public void test(String zipFileName, String extPlace)throws Exception {
        // 验证ZIP压缩文件是否有效
        File fileZip = new File(zipFileName);
        boolean exists = fileZip.exists();
        // 判断文件/文件夹是否存在
        if (exists) {
            // 创建extPlace目录
            File dirs = new File(extPlace);
            dirs.mkdirs();
            dirs = null;
            // 请空目录
            ZipFile zipFile = null;
            try {
                //[color=red]些句相当重要[/color]
                System.setProperty("sun.zip.encoding",
                        System.getProperty("sun.jnu.encoding"));
                zipFile = new ZipFile(zipFileName);

                FileInputStream zin = new FileInputStream(zipFileName);
                ZipInputStream zis = new ZipInputStream(zin);

                //这里必需用jdk自带的ZipEntry
                java.util.zip.ZipEntry zipEntry = zis.getNextEntry();

                while (zipEntry != null) {
                    String entryName = zipEntry.getName().replace(
                            File.separatorChar, '/');
                    String names[] = entryName.split("/");
                    int length = names.length;
                    String path = extPlace;
                    
                    for (int v = 0; v < length; v++) {
                        if (v < length - 1) {
                            path += names[v] + "/";
                            new File(path).mkdir();
                        } else {
                            // 最后一个
                            if (entryName.endsWith("/")) {
                                // 为目录,则创建文件夹
                                new File(extPlace + entryName).mkdir();
                            } else {
                                FileOutputStream fos = new FileOutputStream(extPlace + entryName);
                                int len;
                                byte[] buffer = new byte[1024];
                                while ((len = zis.read(buffer)) > 0) {
                                    fos.write(buffer, 0, len);
                                }
                                fos.close();
                            }
                        }
                    }
                    zis.closeEntry();
                    zipEntry = zis.getNextEntry();
                }
            } catch (Exception e) {
                e.printStackTrace();
                if (zipFile != null)
                    zipFile.close();
            }

            if (zipFile != null)
                zipFile.close();
        } else {

        }
        fileZip = null;
    }


     /*
      * @Description ：判断是目录还是文件，根据类型（文件/文件夹）执行不同的压缩方法
      * @Param file: 被压缩的文件/文件夹的路径
      * @Param out: 压缩文件的输出流
      * @Param basedir:
      * @Return ：void
      * @Author: CTF
      * @Date ：2022/3/23 20:55
      */
    private void compressByType(File file, ZipOutputStream out, String basedir) {
        /* 判断是目录还是文件 */
        if (file.isDirectory()) {
            //logger.info("压缩：" + basedir + file.getName());
            this.compressDirectory(file, out, basedir);
        } else {
            //logger.info("压缩：" + basedir + file.getName());
            this.compressFile(file, out, basedir);
        }
    }

    /**
     * 压缩一个目录
     * @param dir
     * @param out
     * @param basedir
     */
    private void compressDirectory(File dir, ZipOutputStream out, String basedir) {
        if (!dir.exists()){
            return;
        }
        File[] files = dir.listFiles();
        for (int i = 0; i < files.length; i++) {
            /* 递归 */
            compressByType(files[i], out, basedir + dir.getName() + "/");
        }
    }

    /**
     * 压缩一个文件
     * @param file
     * @param out
     * @param basedir
     */
    private void compressFile(File file, ZipOutputStream out, String basedir) {
        if (!file.exists()) {
            return;
        }
        try {
            BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
            ZipEntry entry = new ZipEntry(basedir + file.getName());
            out.putNextEntry(entry);
            int count;
            byte data[] = new byte[BUFFER];
            while ((count = bis.read(data, 0, BUFFER)) != -1) {
                out.write(data, 0, count);
            }
            bis.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
