<%--
  Created by IntelliJ IDEA.
  User: tianfeichen
  Date: 2021/8/21
  Time: 16:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%--更新乡镇信息--%>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>
<div style="padding: 10px">

    <form class="layui-form layui-form-pane">

        <div class="layui-form-item">
            <label class="layui-form-label" style="width:150px">乡镇名称</label>
            <div class="layui-input-inline" style="width:400px">
                <input type="text" name="town_name"
                       autocomplete=“off”
                       lay-verify="required" id="town_name"
                       class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-inline" id="address_select">
                <label class="layui-form-label" style="width: 150px">所属县区</label>
                <div class="layui-input-inline" style="width: 120px">
                    <select name="province"
                            id="province"
                            lay-verify="required"
                            lay-filter="province" lay-search>
                        <option value="">选择省</option>
                    </select>
                </div>
                <div class="layui-input-inline" style="width: 120px">
                    <select name="city"
                            id="city"
                            lay-verify="required"
                            lay-filter="city" lay-search>
                        <option value="">选择市</option>
                    </select>
                </div>
                <div class="layui-input-inline" style="width: 120px">
                    <select name="district"
                            id="district"
                            lay-filter="district"
                            lay-verify="required"
                            lay-search>
                        <option value="">选择区</option>
                    </select>
                </div>
            </div>
        </div>

        <button type="submit" class="layui-btn" style="display:none"
                id="updateTownSubmit" lay-submit lay-filter="updateTownSubmit"></button>
        <button type="reset" class="layui-btn" style="display:none"
                id="updateTownReset" lay-submit lay-filter="updateTownReset"></button>

    </form>
</div>

<script type="text/javascript">


    layui.use(['laydate','form','common','table'], function() {
        var laydate = layui.laydate;
        var form = layui.form;
        var $ = layui.jquery;
        var index = parent.layer.getFrameIndex(window.name);
        var common = layui.common;

        $(function () {
            //读取父页面的值
            var county = parent.$("#county:hidden").val();

            const province = getProvinceStr(county);
            const city = getCityStr(county);
            const district = getDistrictStr(county);

            $("select#province + div div input").val(province);
            $("select#province + div dl>dd:contains('"+province+"')").addClass("layui-this");

            $("select#city + div div input").val(city);
            $("select#city + div dl>dd:contains('"+city+"')").addClass("layui-this");

            $("select#district + div div input").val(district);
            $("select#district + div dl>dd:contains('"+district+"')").addClass("layui-this");

        })

        form.on('submit(updateTownSubmit)', function(data){
            const sourceData = data.field;

            //解析解析框中的地址内容
            const city = sourceData.city;
            const district = sourceData.district;
            const province = sourceData.province;
            // 通过地址code码获取地址名称
            const address = common.getCity({
                province,
                city,
                district
            });
            const provinceName = address.provinceName;
            const cityName = address.cityName;
            const districtName = address.districtName;
            //解析解析框中的地址内容
            const county = provinceName + ' ' + cityName + ' ' + districtName;
            //乡镇名称
            const town_name = sourceData.town_name;

            //获取当前页码
            var currentPage = parent.$(".layui-laypage-skip .layui-input").val();
            //获取乡镇编号
            var town_id = parent.$("#town_id:hidden").val();

            $.ajax({
                type : 'POST',
                url : 'baseInfoServlet?action=updateTownInfo',
                data : {
                    town_id : town_id,
                    town_name : town_name,
                    county : county
                },
                dataType : 'json',
                success : function(data) {
                    if(data.code != 1){
                        parent.layer.msg(data.msg, {
                            icon : 5
                        });
                    }
                    else {
                        // 更新成功
                        parent.layer.msg('更新成功', {
                            icon : 6,
                        });
                        //重载表格
                        parent.layui.table.reload('towninfo', {
                            url: 'baseInfoServlet?action=queryAllTownInfo'
                            ,page: {
                                curr: currentPage //从更新的信息所在页开始
                            }
                            ,request: {
                                pageName: 'curr'
                                ,limitName: 'nums'
                            }
                        });
                        //关闭此页面
                        parent.layer.close(index);
                    }
                },
                error : function(data) {
                    // 异常提示
                    parent.layer.msg('出现网络故障', {
                        icon : 5
                    });
                    //关闭此页面
                    parent.layer.close(index);
                }
            });
            return false;
        });
    });
</script>
</body>
</html>
