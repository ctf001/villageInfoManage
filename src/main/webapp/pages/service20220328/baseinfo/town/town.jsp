<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>
    <div class="layui-layout layui-layout-admin">
        <%--头部信息--%>
        <%@include file="/pages/common/header.jsp"%>

        <%--左侧导航--%>
        <%@include file="/pages/common/menu.jsp"%>

        <%--主体信息--%>
        <div class="layui-body layui-bg-gray" style="padding: 10px">
            <%--信息查询--%>
            <div class="layui-row">
                <div class="layui-bg-gray" style="padding: 10px;">
                    <div class="layui-row">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header layui-bg-blue">乡镇信息查询</div>
                                <div class="layui-card-body">
                                    <%--查询表单开始--%>
                                    <form class="layui-form layui-form-pane">

                                        <div class="layui-form-item">
                                            <div class="layui-inline">
                                                <label class="layui-form-label" style="width: 100px">乡镇名称</label>
                                                <div class="layui-input-inline"  style="width: 150px">
                                                    <input type="text" name="town_name"
                                                           placeholder="请输入乡镇名称"
                                                           autocomplete=“off”
                                                           class="layui-input">
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <label class="layui-form-label" style="width: 100px">所属县区</label>
                                                <div class="layui-input-inline" style="width: 135px">
                                                    <select name="province" lay-filter="province">
                                                        <option value="">选择省</option>
                                                    </select>
                                                </div>
                                                <div class="layui-input-inline" style="width: 135px">
                                                    <select name="city" lay-filter="city">
                                                        <option value="">选择市</option>
                                                    </select>
                                                </div>
                                                <div class="layui-input-inline" style="width: 135px">
                                                    <select name="district" lay-filter="district">
                                                        <option value="">选择区</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="layui-form-item" style="padding-left: 70%">
                                            <div class="layui-input-block">
                                                <button type="submit" class="layui-btn" lay-submit
                                                        lay-filter="town_info_query">查询</button>
                                                <button type="reset" class="layui-btn layui-btn-normal"
                                                        id="town_info_query_reset" >重置</button>
                                            </div>
                                        </div>


                                    </form>
                                    <%--查询表单结束--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--表格展示--%>
            <div class="layui-row">
                <div class="layui-col-md12">
                    <div class="layui-bg-gray" style="padding: 10px;">
                        <div class="layui-row">
                            <div class="layui-col-md12">
                                <div class="layui-card">
                                    <div class="layui-card-header layui-bg-blue">乡镇信息管理</div>
                                    <div class="layui-card-body">

                                        <%--数据展示--%>
                                        <table class="layui-hide" id="towninfo"
                                               lay-filter="towninfo"></table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%--底部信息--%>
        <div class="layui-footer">
            <%@include file="/pages/common/footer.jsp" %>
        </div>

    </div>

    <%--隐藏域：接受地址值，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            name="county"id="county"/>
    <%--隐藏域：接受乡镇编号，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            name="town_id"id="town_id"/>

    <%--表格上方工具栏--%>
    <script type="text/html" id="toolbar">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-primary layui-border-green layui-btn-sm"
                    lay-event="export" id="export" >导出当前查询数据</button>
            <button class="layui-btn layui-btn-sm" lay-event="addATown" id="addATown">新增乡镇</button>
        </div>
    </script>

    <%--表格内部工具栏--%>
    <script type="text/html" id="baseInfo">
        <a class="layui-btn layui-btn-xs" lay-event="update" >修改</a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>
    </script>

    <%--本页js--%>
    <script>

        layui.use(['table','upload','laydate','common','element','form',
            'layer', 'util','laypage'], function(){
            var common = layui.common;
            var element = layui.element;
            var layer = layui.layer;
            var util = layui.util;
            var $ = layui.jquery;
            var table = layui.table;
            var form = layui.form;
            var laydate = layui.laydate;
            var laypage = layui.laypage;
            var upload = layui.upload;

            //定义导出报表的数据
            let exportData = {};

            //表格数据读取参数
            var town_info_table = table.render({
                elem: '#towninfo'
                ,url:'baseInfoServlet?action=queryAllTownInfo'
                ,toolbar: '#toolbar'
                ,defaultToolbar: []
                ,title: '乡镇信息表'
                ,request: {
                    pageName: 'curr' //页码的参数名称，默认：page
                    ,limitName: 'nums' //每页数据量的参数名，默认：limit
                }
                ,limit:5
                ,limits:[5,10,15]
                ,cols: [[
                    {field:'town_id', title:'乡镇序号',align:"center"}
                    ,{field:'town_name', title:'乡镇名称',align:"center"}
                    ,{field:'county', title:'所属县区',align:"center"}
                    ,{fixed: 'right', title:'操作',align:"center", toolbar: '#baseInfo'}
                ]]
                ,page: true
                ,parseData: function(res) { //res 即为原始返回的数据
                    //将本次查询的数据赋值给导出数据指定的变量
                    exportData = res.count;
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.msg, //解析提示文本
                        "count": res.count.length, //解析数据长度
                        "data": res.data //解析数据列表
                    };
                }
            });

            /*设定表格工具事件*/
            table.on('toolbar(towninfo)', function(obj){
                var body;
                switch(obj.event){
                    case 'addATown':
                        layer.open({
                            type: 2,
                            title: '新增乡镇',
                            shadeClose: true,
                            shade: false,
                            maxmin: true, //开启最大化最小化按钮
                            area: ['600px', '680px'],
                            content: "pages/service/baseinfo/town/_addATown.jsp",
                            anim:2,
                            resize:false,
                            id:'LAY_layuipro',
                            btn:['提交','重置'],
                            yes:function (index, layero) {
                                body = layer.getChildFrame('body', index); //提交按钮的回调
                                body.find('#addTownSubmit').click();// 找到隐藏的提交按钮模拟点击提交
                            },
                            btn2: function (index, layero) {
                                body = layer.getChildFrame('body', index); //重置按钮的回调
                                body.find('#addTownReset').click(); // 找到隐藏的提交按钮模拟点击提交
                                return false;// 开启该代码可禁止点击该按钮关闭
                            },
                            cancel: function () {
                                //右上角关闭回调
                                //return false 开启该代码可禁止点击该按钮关闭
                            }
                        });

                        break;
                    case 'export':
                        table.exportFile(town_info_table.config.id, exportData, 'xls');
                        break;
                };
            });

            /*设定行工具事件*/
            table.on('tool(towninfo)', function(obj){
                //获得当前行数据
                var data = obj.data;
                //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                var layEvent = obj.event;
                //获取当前页码
                var currentPage = $(".layui-laypage-skip .layui-input").val();

                //更新乡镇信息
                if(layEvent === 'update'){
                    //为隐藏域赋值
                    $("#county:hidden").val(data.county);
                    $("#town_id:hidden").val(data.town_id);

                   var updateLayer = layer.open({
                        type: 2,
                        title: '更新乡镇信息',
                        maxmin: true,
                        area: ['600px', '600px'],
                        content: "pages/service/baseinfo/town/_updateTownInfo.jsp",
                        anim:2,
                        id:'LAY_layuipro',
                        resize:false,
                        btn:['更新','取消'],
                        success: function (layero, index) {
                            var body = layer.getChildFrame('body', index);
                            //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                            var iframeWin = window[layero.find('iframe')[0]['name']];
                            //初始化表单数据的值
                            body.find("#town_name").val(data.town_name);
                        },
                        yes:function (index, layero) {
                            //更新按钮的回调
                            var body = layer.getChildFrame('body', index);
                            // 找到隐藏的提交按钮模拟点击提交
                            body.find('#updateTownSubmit').click();
                            //return false 开启该代码可禁止点击该按钮关闭
                        },
                        btn2: function (index, layero) {
                            //取消按钮的回调
                            layer.close(updateLayer);
                            //return false 开启该代码可禁止点击该按钮关闭
                        },
                        cancel: function () {
                            layer.close(updateLayer);
                            //右上角关闭回调
                            //return false 开启该代码可禁止点击该按钮关闭
                        }
                    });
                }
                //删除此乡镇
                else if(layEvent === 'delete'){
                    var deleteLayer = layer.confirm('确定删除该乡镇吗？', function(index){
                        obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                        layer.close(index);
                        //向服务端发送删除指令
                        $.ajax({
                            type : 'POST',
                            url : 'baseInfoServlet?action=deleteATownByTownId',
                            data : {
                                town_id : data.town_id,
                            },
                            dataType : 'json',
                            success : function(data) {
                                if(data.code != 1){
                                    parent.layer.msg(data.msg, {
                                        icon : 5
                                    });
                                }
                                else {
                                    layer.msg('已删除', {
                                        icon : 6,
                                    });
                                    //重载表格
                                    table.reload('towninfo', {
                                        url: 'baseInfoServlet?action=queryAllTownInfo'
                                        ,page: {
                                            curr: currentPage //从数据发生所在页开始
                                        }
                                        ,request: {
                                            pageName: 'curr'
                                            ,limitName: 'nums'
                                        }
                                    });
                                    //关闭此页面
                                    layer.close(deleteLayer);
                                }
                            },
                            error : function(data) {
                                // 异常提示
                                layer.msg('出现网络故障', {
                                    icon : 5
                                });
                            }
                        });
                    });
                }
            });

            //监听查询模块提交事件
            form.on('submit(town_info_query)', function(data){
                var sourceData = data.field;

                //解析解析框中的地址内容
                var city = sourceData.city;
                var district = sourceData.district;
                var province = sourceData.province;
                // 通过地址code码获取地址名称
                var address = common.getCity({
                    province,
                    city,
                    district
                });
                var provinceName = address.provinceName;
                var cityName = address.cityName;
                var districtName = address.districtName;
                //解析解析框中的地址内容
                var county = provinceName + ' ' + cityName + ' ' + districtName;

                //乡镇名称
                var town_name = sourceData.town_name;

                //重载表格
                town_info_table.reload({
                    url: 'baseInfoServlet?action=querySomeTownInfo'
                    ,where: {
                        town_name : town_name,
                        county : county
                    }
                    ,page:true
                    ,request: {
                        pageName: 'curr'
                        ,limitName: 'nums'
                    }
                    ,page: {
                        curr: 1 //重新从第 1 页开始
                    }
                    ,parseData: function(res) {
                        //将本次查询的数据赋值给导出数据指定的变量
                        exportData = res.count;
                        return {
                            "code": res.code, //解析接口状态
                            "msg": res.msg, //解析提示文本
                            "count": res.count.length, //解析数据长度
                            "data": res.data //解析数据列表
                        };
                    }
                });

                return false;
            });

        });
    </script>

</body>

</html>
