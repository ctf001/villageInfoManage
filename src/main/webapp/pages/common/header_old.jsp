<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layui-header"
     style="height: 70px;
                 color:white;
                 letter-spacing:0.5em;
                 vertical-align: middle;
                 line-height: 70px;
                 text-align: center; ">

       <div class="layui-fluid">
           <div class="layui-row">
               <div class="layui-col-md3" style="height: 70px">
               </div>
               <div class="layui-col-md6">
                   <h1>夏麦村村级信息管理系统</h1>
               </div>
               <div class="layui-col-md3" style="height: 70px">
                   <div class="layui-row" style="padding-top: 20px;padding-left: 60px">
                       <p>
                           <%--当前用户：${sessionScope.user.operator}，--%>
                           <a href="loginServlet?action=loginOut" style="color:whitesmoke;">退出</a>
                       </p>
                   </div>

               </div>
           </div>

       </div>
</div>