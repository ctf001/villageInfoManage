<%--
  Created by IntelliJ IDEA.
  User: CTF
  Date: 2022/3/28
  Time: 19:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <%--基础引入--%>
        <%@include file="/pages/common/baseinfo.jsp"%>

        <style type="text/css">
            .rightmenu{font-size:12px; padding:5px 10px; border-radius: 2px;}
            .rightmenu li{line-height:20px; cursor: pointer;}
            ul.layui-tab-title li:first-child i{display:none;}

            .layui-nav-side {
                position: fixed;
                top: 60px;
                bottom: 0px;
                left: 0px;
                overflow-x: hidden;
                z-index: 999;
            }

            .layui-layout-admin .layui-body {
                position: absolute;
                top: 45px;
                padding-bottom: 0px
            }
        </style>

    </head>

    <body class="layui-layout-body layui-layout-admin">
        <%--头部信息--%>
        <div class="layui-header">
            <div class="layui-logo <%--layui-hide-xs layui-bg-black--%>">
                <span>夏麦村村级信息管理系统</span>
            </div>

            <%--左侧区域--%>
            <ul class="layui-nav layui-layout-left">
                <!-- 移动端显示 -->
                <li class="layui-nav-item layui-show-xs-inline-block layui-hide-xs"
                    lay-header-event="menuLeft">
                    <i class="layui-icon layui-icon-spread-left" id="LAY_app_flexible"></i>
                </li>

                <li class="layui-nav-item layui-hide-xs"><a href="">测试功能点1</a></li>
                <li class="layui-nav-item layui-hide-xs"><a href="">测试功能点2</a></li>
                <li class="layui-nav-item">
                    <a href="javascript:;">测试功能集1</a>
                    <dl class="layui-nav-child">
                        <dd><a href="">测试功能点1</a></dd>
                        <dd><a href="">测试功能点2</a></dd>
                        <dd><a href="">测试功能点3</a></dd>
                    </dl>
                </li>
            </ul>

            <%--右侧区域--%>
            <ul class="layui-nav layui-layout-right">
                <li class="layui-nav-item layui-hide layui-show-md-inline-block">
                    <a href="javascript:;">
                        <%--<img src="//tva1.sinaimg.cn/crop.0.0.118.118.180/5db11ff4gw1e77d3nqrv8j203b03cweg.jpg" class="layui-nav-img">--%>
                        ${sessionScope.user.operator}
                    </a>
                </li>
                <li class="layui-nav-item" lay-header-event="menuRight" lay-unselect>
                    <a class="demo1">
                        <i class="layui-icon layui-icon-more-vertical"></i>
                    </a>
                </li>
            </ul>

        </div>

        <%--左侧菜单--%>
        <div class="layui-side layui-bg-black">
            <div class="layui-side-scroll">
                <ul class="layui-nav layui-nav-tree layui-nav-side" lay-filter="treenav">
                    <li class="layui-nav-item layui-nav-itemed">
                        <a href="javascript:;">基础信息</a>
                        <dl class="layui-nav-child">
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="1"
                                   data-title="乡基本信息" data-url="pages/service/baseinfo/town/town.jsp" target="_top">乡基本信息</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="2"
                                   data-title="村基本信息"  data-url="pages/service/baseinfo/village/village.jsp">村基本信息</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="3"
                                   data-title="自然村信息" data-url="pages/service/baseinfo/ucpVillage/ucpVillage.jsp">自然村信息</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="4"
                                   data-title="双联户信息" data-url="pages/service/baseinfo/unionfamily/unionfamily.jsp">双联户信息</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="5"
                                   data-title="家庭信息" data-url="pages/service/baseinfo/family/family.jsp">家庭信息</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="6"
                                   data-title="人员信息" data-url="pages/service/baseinfo/personinfo/personinfo.jsp">人员信息</a>
                            </dd>
                        </dl>
                    </li>

                    <li class="layui-nav-item layui-nav-itemed">
                        <a href="javascript:;">拓展信息</a>
                        <dl class="layui-nav-child">
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="7"
                                   data-title="党群团信息" data-url="pages/common/generating.jsp">党群团信息</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="8"
                                   data-title="国民经济数据维护"  data-url="pages/common/generating.jsp">国民经济数据维护</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="9"
                                   data-title="双联户信息" data-url="pages/common/generating.jsp">双联户信息</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="10"
                                   data-title="家庭信息" data-url="pages/common/generating.jsp">家庭信息</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="11"
                                   data-title="人员信息" data-url="pages/common/generating.jsp">人员信息</a>
                            </dd>
                        </dl>
                    </li>

                    <li class="layui-nav-item layui-nav-itemed">
                        <a href="javascript:;">合作社管理</a>
                        <dl class="layui-nav-child">
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="7"
                                   data-title="合作社基本信息" data-url="pages/common/generating.jsp">合作社基本信息</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="7"
                                   data-title="社员信息" data-url="pages/common/generating.jsp">社员信息</a>
                            </dd>
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="7"
                                   data-title="经营信息" data-url="pages/common/generating.jsp">经营信息</a>
                            </dd>
                        </dl>
                    </li>

                    <li class="layui-nav-item layui-nav-itemed">
                        <a href="javascript:;">业务办理</a>
                        <dl class="layui-nav-child">
                            <dd>
                                <a href="javascript:;" class="site-url" data-id="7"
                                   data-title="党群团信息" data-url="pages/common/generating.jsp">证明开具</a>
                            </dd>
                        </dl>
                    </li>
                </ul>
            </div>
        </div>

        <%--主体区域--%>
        <div class="layui-body layui-bg-gray">
            <div class="layui-tab layui-tab-brief" lay-filter="contentnav" lay-allowClose="true">
                <ul class="layui-tab-title">
                    <li class="layui-this">首页</li>
                </ul>
                <ul class="layui-bg-green rightmenu" style="display: none;position: absolute;">
                    <li data-type="closethis">关闭当前</li>
                    <li data-type="closeall">关闭所有</li>
                </ul>
                <div class="layui-tab-content" style="padding:0;">
                    <div class="layui-tab-item layui-show">首页内容...</div>
                </div>
            </div>
        </div>


        <script type="text/javascript">
        layui.use(['element','dropdown', 'util', 'layer', 'table'], function() {
            var element = layui.element
                ,dropdown = layui.dropdown
                , util = layui.util
                , layer = layui.layer
                , table = layui.table
                , $ = layui.jquery;

            //头部事件
            util.event('lay-header-event', {
                //左侧菜单事件
                menuLeft: function(othis){
                    // 下面是菜单栏收缩
                    var isShow = true;  //定义一个标志位

                    $('#LAY_app_flexible').click(function(){
                        //选择出所有的span，并判断是不是hidden
                        $('.layui-nav-item span').each(function(){
                            if($(this).is(':hidden')){
                                $(this).show();
                            }else{
                                $(this).hide();
                            }
                        });
                        //判断isshow的状态
                        if(isShow){
                            $('.layui-side.layui-bg-black').width(0); //设置宽度
                            $('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
                            //将footer和body的宽度修改
                            $('.layui-body').css('left', 0+'px');
                            $('.layui-footer').css('left', 0+'px');
                            //将二级导航栏隐藏
                            $('dd span').each(function(){
                                $(this).hide();
                            });
                            //修改标志位
                            isShow =false;
                        }else{
                            $('.layui-side.layui-bg-black').width(200);
                            $('.kit-side-fold i').css('margin-right', '10%');
                            $('.layui-body').css('left', 200+'px');
                            $('.layui-footer').css('left', 200+'px');
                            $('dd span').each(function(){
                                $(this).show();
                            });
                            isShow =true;
                        }
                    });

                }
            })
            //初演示
            dropdown.render({
                elem: '.demo1'
                , data: [{
                    title: '账户信息'
                    , id: 100
                }, {
                    title: '系统设置'
                    , id: 101
                }, {
                    title: '退出登录'
                    , id: 102
                }]
                , click: function (obj) {
                    switch (obj.title) {
                        case "退出登录":
                            window.location.href = "loginServlet?action=loginOut";
                        case "系统设置":
                            window.location.href = "#";
                        case "账户信息":
                            window.location.href = "#";
                    }
                }
            });

            var active={
                tabAdd:function(url,id,name){
                    element.tabAdd('contentnav',{
                        title:name,
                        content:'<iframe data-frameid="'+id+'" scrolling="no" frameborder="no" ' +
                            'src="'+url+'" style="width:100%;" class="layadmin-iframe"></iframe>',
                        id:id
                    });
                    rightMenu();
                    iframeWH();
                },
                tabChange:function(id){
                    element.tabChange('contentnav',id);
                },
                tabDelete:function(id){
                    element.tabDelete('contentnav',id);
                },
                tabDeleteAll:function(ids){
                    $.each(ids,function(index,item){
                        element.tabDelete('contentnav',item);
                    });
                }
            };

            $(".site-url").on('click',function(){
                var nav=$(this);
                var length = $("ul.layui-tab-title li").length;
                if(length<=0){
                    active.tabAdd(
                        nav.attr("data-url")
                        ,nav.attr("data-id"),
                        nav.attr("data-title")
                    );
                }else{
                    var isData=false;
                    $.each($("ul.layui-tab-title li"),function(){
                        if($(this).attr("lay-id")==nav.attr("data-id")){
                            isData=true;
                        }
                    });
                    if(isData==false){
                        active.tabAdd(
                            nav.attr("data-url"),
                            nav.attr("data-id"),
                            nav.attr("data-title")
                        );
                    }
                    active.tabChange(nav.attr("data-id"));
                }
            });

            function rightMenu(){
                //右击弹出
                $(".layui-tab-title li").on('contextmenu',function(e){
                    var menu=$(".rightmenu");
                    menu.find("li").attr('data-id',$(this).attr("lay-id"));
                    l = e.clientX;
                    t = e.clientY;
                    menu.css({ left:l, top:t}).show();
                    return false;
                });
                //左键点击隐藏
                $("body,.layui-tab-title li").click(function(){
                    $(".rightmenu").hide();
                });
            }

            $(".rightmenu li").click(function(){
                if($(this).attr("data-type")=="closethis"){
                    active.tabDelete($(this).attr("data-id"));
                }else if($(this).attr("data-type")=="closeall"){
                    var tabtitle = $(".layui-tab-title li");
                    var ids = new Array();
                    tabtitle.each(function(i){
                        ids.push($(this).attr("lay-id"));
                    });
                    //如果关闭所有 ，即将所有的lay-id放进数组，执行tabDeleteAll
                    active.tabDeleteAll(ids);
                }
                $('.rightmenu').hide(); //最后再隐藏右键菜单
            });

            function iframeWH(){
                var H = $(window).height()-80;
                $("iframe").css("height",H+"px");
            }

            $(window).resize(function(){
                iframeWH();
            });
        });
    </script>

    </body>
</html>