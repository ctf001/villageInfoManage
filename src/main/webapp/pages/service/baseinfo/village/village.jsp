<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
    <head>
        <%--基础引入--%>
        <%@include file="/pages/common/baseinfo.jsp"%>
    </head>
    <body>
        <%--主体信息--%>
        <div class="layui-bg-gray" style="padding: 10px">
        <%--信息查询--%>
        <div class="layui-row">
            <div class="layui-bg-gray" style="padding: 10px;">
                <div class="layui-row">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header layui-bg-blue">村庄信息查询</div>
                            <div class="layui-card-body">
                                <%--查询表单开始--%>
                                <form class="layui-form layui-form-pane">

                                    <div class="layui-form-item">
                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">村名</label>
                                            <div class="layui-input-inline"  style="width: 204px">
                                                <input type="text" name="village_name"
                                                       placeholder="请输入村庄名称"
                                                       autocomplete=“off”
                                                       class="layui-input">
                                            </div>
                                        </div>

                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">所属乡</label>
                                            <div class="layui-input-inline"  style="width: 204px" >
                                                <select id="town" name="town" lay-search>
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">距离乡政府公里数</label>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="apart_from_town_min"
                                                       placeholder="公里" autocomplete="off" class="layui-input">
                                            </div>
                                            <div class="layui-form-mid">至</div>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="apart_from_town_max"
                                                       placeholder="公里" autocomplete="off" class="layui-input">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="layui-form-item">
                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">耕地面积</label>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="plough_area_min"
                                                       placeholder="亩" autocomplete="off" class="layui-input">
                                            </div>
                                            <div class="layui-form-mid">至</div>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="plough_area_max"
                                                       placeholder="亩" autocomplete="off" class="layui-input">
                                            </div>
                                        </div>

                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">平均海拔</label>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="elevation_min"
                                                       placeholder="米" autocomplete="off" class="layui-input">
                                            </div>
                                            <div class="layui-form-mid">至</div>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="elevation_max"
                                                       placeholder="米" autocomplete="off" class="layui-input">
                                            </div>
                                        </div>


                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">草场面积</label>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="meadow_area_min"
                                                       placeholder="亩" autocomplete="off" class="layui-input">
                                            </div>
                                            <div class="layui-form-mid">至</div>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="meadow_area_max"
                                                       placeholder="亩" autocomplete="off" class="layui-input">
                                            </div>
                                        </div>

                                    </div>

                                    <%--按钮区--%>
                                    <div class="layui-form-item" style="padding-left: 70%">
                                        <div class="layui-input-block">
                                            <button type="submit" class="layui-btn" lay-submit
                                                    lay-filter="village_info_query">查询</button>
                                            <button type="reset" class="layui-btn layui-btn-normal"
                                                    id="village_info_query_reset" >重置</button>
                                        </div>
                                    </div>

                                </form>
                                <%--查询表单结束--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--表格展示--%>
        <div class="layui-row">
            <div class="layui-col-md12">
                <div class="layui-bg-gray" style="padding: 10px;">
                    <div class="layui-row">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header layui-bg-blue">村庄信息管理</div>
                                <div class="layui-card-body">

                                    <%--数据展示--%>
                                    <table class="layui-hide" id="villageinfo"
                                           lay-filter="villageinfo"></table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

        <%--隐藏域：接受村庄编号，以便子页面获取--%>
        <input  type="hidden" class="layui-input" style="display:none"
                name="village_id" id="village_id"/>
        <%--隐藏域：接受乡镇编号，以便子页面获取--%>
        <input  type="hidden" class="layui-input" style="display:none"
                name="town_id" id="town_id"/>

        <%--表格上方工具栏--%>
        <script type="text/html" id="toolbar">
            <div class="layui-btn-container">
                <button class="layui-btn layui-btn-primary layui-border-green layui-btn-sm"
                        lay-event="export" id="export" >导出当前查询数据</button>
                <button class="layui-btn layui-btn-sm" lay-event="addAVillage" id="addAVillage">新增村庄</button>
            </div>
        </script>

        <%--表格内部工具栏--%>
        <script type="text/html" id="baseInfo">
            <a class="layui-btn layui-btn-xs" lay-event="update" >修改</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>
        </script>

        <%--本页js--%>
        <script>

            layui.use(['table','upload','laydate','common','element','form',
                'layer', 'util','laypage'], function(){
                var common = layui.common;
                var element = layui.element;
                var layer = layui.layer;
                var util = layui.util;
                var $ = layui.jquery;
                var table = layui.table;
                var form = layui.form;
                var laydate = layui.laydate;
                var laypage = layui.laypage;
                var upload = layui.upload;

                //获取数据库乡镇数据并完成下拉框绑定
                bindTownSelectData();

                //定义导出报表的数据
                let exportData = {};

                //表格数据读取参数
                var village_info_table = table.render({
                    elem: '#villageinfo'
                    ,url:'baseInfoServlet?action=queryAllVillageInfo'
                    ,toolbar: '#toolbar'
                    ,defaultToolbar: []
                    ,title: '村庄信息表'
                    ,request: {
                        pageName: 'curr'
                        ,limitName: 'nums'
                    }
                    ,limit:5
                    ,limits:[5,10,15]
                    ,cols: [[
                        {field:'village_id', title:'村庄编号',align:"center"}
                        ,{field:'village_name', title:'村庄名称',align:"center"}
                        ,{field:'town', title:'所属乡镇',align:"center"}
                        ,{field:'apart_from_town', title:'距离乡政府公里数（公里）',align:"center"}
                        ,{field:'plough_area', title:'耕地面积（亩）',align:"center"}
                        ,{field:'elevation', title:'平均海拔（米）',align:"center"}
                        ,{field:'meadow_area', title:'草场面积（亩）',align:"center"}
                        ,{fixed: 'right', title:'操作',align:"center", toolbar: '#baseInfo'}
                    ]]
                    ,page: true
                    ,parseData: function(res) {
                        //将本次查询的数据赋值给导出数据指定的变量
                        exportData = res.count;
                        return {
                            "code": res.code, //解析接口状态
                            "msg": res.msg, //解析提示文本
                            "count": res.count.length, //解析数据长度
                            "data": res.data //解析数据列表
                        };
                    }
                });

                /*设定表格工具事件*/
                table.on('toolbar(villageinfo)', function(obj){
                    var body;
                    switch(obj.event){
                        case 'addAVillage':
                            layer.open({
                                type: 2,
                                title: '新增村庄',
                                maxmin: true,
                                area: ['600px', '680px'],
                                content: "pages/service/baseinfo/village/_addAVillage.jsp",
                                anim:2,
                                resize:false,
                                id:'LAY_layuipro',
                                btn:['提交','重置'],
                                yes:function (index, layero) {
                                    body = layer.getChildFrame('body', index); //提交按钮的回调
                                    body.find('#addAVillageSubmit').click();// 找到隐藏的提交按钮模拟点击提交
                                },
                                btn2: function (index, layero) {
                                    body = layer.getChildFrame('body', index); //重置按钮的回调
                                    body.find('#addAVillageReset').click(); // 找到隐藏的提交按钮模拟点击提交
                                    return false;// 开启该代码可禁止点击该按钮关闭
                                },
                                cancel: function () {
                                }
                            });

                            break;
                        case 'export':
                            table.exportFile(village_info_table.config.id, exportData, 'xls');
                            break;
                    };
                });

                /*设定行工具事件*/
                table.on('tool(villageinfo)', function(obj){
                    //获得当前行数据
                    var data = obj.data;

                    //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
                    var layEvent = obj.event;
                    //获取当前页码
                    var currentPage = $(".layui-laypage-skip .layui-input").val();

                    //更新村庄信息
                    if(layEvent === 'update'){
                        //为隐藏域赋值
                        $("#town_id:hidden").val(data.town_id);
                        $("#village_id:hidden").val(data.village_id);

                        var updateLayer = layer.open({
                            type: 2,
                            title: '更新村庄信息',
                            maxmin: true,
                            area: ['600px', '600px'],
                            content: "pages/service/baseinfo/village/_updateVillageInfo.jsp",
                            anim:2,
                            id:'LAY_layuipro',
                            resize:false,
                            btn:['更新','取消'],
                            success: function (layero, index) {
                                var body = layer.getChildFrame('body', index);
                                //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                                var iframeWin = window[layero.find('iframe')[0]['name']];
                                //初始化表单数据的值
                                body.find("#village_name").val(data.village_name);
                                body.find("#apart_from_town").val(data.apart_from_town);
                                body.find("#plough_area").val(data.plough_area);
                                body.find("#elevation").val(data.elevation);
                                body.find("#meadow_area").val(data.meadow_area);
                                //为所属乡镇通过后台数据库拉取数据并绑定下拉框
                                $.ajax({
                                    url: 'baseInfoServlet?action=queryAllTownInfo',
                                    type : 'POST',
                                    dataType : 'json',
                                    contentType : "text/html;charset=utf-8",
                                    success: function (sourceData) {
                                        var SourceData = sourceData.data;
                                        $.each(SourceData, function (index, item) {
                                            if (data.town_id == item.town_id) {
                                                body.find('[id=town]').append(
                                                    "<option value='"+item.town_id+"' selected>"+item.town_name+"</option>>"
                                                );
                                            } else {
                                                body.find('[id=town]').append(
                                                    $("<option>").attr("value", item.town_id).text(item.town_name));
                                            }
                                        });

                                        //重新渲染，特别重要，不然写的不起作用
                                        iframeWin.layui.form.render("select");

                                    }
                                })
                            },
                            yes:function (index, layero) {
                                //更新按钮的回调
                                var body = layer.getChildFrame('body', index);
                                // 找到隐藏的提交按钮模拟点击提交
                                body.find('#updateVillageSubmit').click();
                                //return false 开启该代码可禁止点击该按钮关闭
                            },
                            btn2: function (index, layero) {
                                //取消按钮的回调
                                layer.close(updateLayer);
                                //return false 开启该代码可禁止点击该按钮关闭
                            },
                            cancel: function () {
                                layer.close(updateLayer);
                                //右上角关闭回调
                                //return false 开启该代码可禁止点击该按钮关闭
                            }
                        });
                    }
                    //删除此村庄
                    else if(layEvent === 'delete'){
                        var deleteLayer = layer.confirm('确定删除该村庄吗？', function(index){
                            obj.del(); //删除对应行（tr）的DOM结构，并更新缓存
                            layer.close(index);
                            $.ajax({
                                type : 'POST',
                                url : 'baseInfoServlet?action=deleteAVillageByVillageId',
                                data : {
                                    village_id : data.village_id,
                                },
                                dataType : 'json',
                                success : function(data) {
                                    if(data.code != 1){
                                        parent.layer.msg(data.msg, {
                                            icon : 5
                                        });
                                    }
                                    else {
                                        layer.msg('已删除', {
                                            icon : 6,
                                        });
                                        //重载表格
                                        table.reload('villageinfo', {
                                            url: 'baseInfoServlet?action=queryAllVillageInfo'
                                            ,page: {
                                                curr: currentPage //从数据发生所在页开始
                                            }
                                            ,request: {
                                                pageName: 'curr'
                                                ,limitName: 'nums'
                                            }
                                        });
                                        //关闭此页面
                                        layer.close(deleteLayer);
                                    }
                                },
                                error : function(data) {
                                    // 异常提示
                                    layer.msg('出现网络故障', {
                                        icon : 5
                                    });
                                }
                            });
                        });
                    }
                });

                //监听查询模块提交事件
                form.on('submit(village_info_query)', function(data){
                    var sourceData = data.field;

                    //村庄名称
                    var village_name = sourceData.village_name;
                    var town_id = sourceData.town;
                    var apart_from_town_min = sourceData.apart_from_town_min;
                    var apart_from_town_max = sourceData.apart_from_town_max;
                    var plough_area_min = sourceData.plough_area_min;
                    var plough_area_max = sourceData.plough_area_max;
                    var elevation_min = sourceData.elevation_min;
                    var elevation_max = sourceData.elevation_max;
                    var meadow_area_min = sourceData.meadow_area_min;
                    var meadow_area_max = sourceData.meadow_area_max;

                    //重载表格
                    village_info_table.reload({
                        url: 'baseInfoServlet?action=querySomeVillageInfo'
                        ,where: {
                            village_name : village_name,
                            town_id : town_id,
                            apart_from_town_min : apart_from_town_min,
                            apart_from_town_max : apart_from_town_max,
                            plough_area_min : plough_area_min,
                            plough_area_max : plough_area_max,
                            elevation_min : elevation_min,
                            elevation_max : elevation_max,
                            meadow_area_min : meadow_area_min,
                            meadow_area_max : meadow_area_max
                        }
                        ,page:true
                        ,request: {
                            pageName: 'curr'
                            ,limitName: 'nums'
                        }
                        ,page: {
                            curr: 1 //重新从第 1 页开始
                        }
                        ,parseData: function(res) {
                            //将本次查询的数据赋值给导出数据指定的变量
                            exportData = res.count;
                            return {
                                "code": res.code, //解析接口状态
                                "msg": res.msg, //解析提示文本
                                "count": res.count.length, //解析数据长度
                                "data": res.data //解析数据列表
                            };
                        }
                    });

                    return false;
                });

            });
        </script>

    </body>
</html>
