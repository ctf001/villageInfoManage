<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>

    <%--信息查询--%>
    <div class="layui-row">
        <div class="layui-bg-gray" style="padding: 10px;">
            <div class="layui-row">
                <div class="layui-col-md12">
                    <div class="layui-card">
                        <div class="layui-card-header layui-bg-blue">家庭信息查询</div>
                        <div class="layui-card-body">
                            <%--查询表单开始--%>
                            <form class="layui-form layui-form-pane">
                                <div class="layui-form-item">
                                    <div class="layui-inline">
                                        <label class="layui-form-label" style="width:170px">户号（户口本记录）</label>
                                        <div class="layui-input-inline" style="width:200px">
                                            <input type="text"
                                                   name="family_num"
                                                   class="layui-input">
                                        </div>
                                    </div>

                                    <div class="layui-inline">
                                        <label class="layui-form-label" style="width:170px">户名</label>
                                        <div class="layui-input-inline" style="width:200px">
                                            <input type="text"
                                                   name="family_name"
                                                   class="layui-input">
                                        </div>
                                    </div>

                                    <div class="layui-inline">
                                        <label class="layui-form-label" style="width:170px">户别（家庭类型）</label>
                                        <div class="layui-input-inline" style="width:200px">
                                            <input type="text"
                                                   name="family_type"
                                                   class="layui-input">
                                        </div>
                                    </div>

                                    <div class="layui-inline">
                                        <label class="layui-form-label" style="width:170px">住址</label>
                                        <div class="layui-input-inline" style="width:200px">
                                            <input type="text"
                                                   name="address"
                                                   class="layui-input">
                                        </div>
                                    </div>

                                </div>

                                <div class="layui-form-item">

                                    <div class="layui-inline">
                                        <label class="layui-form-label" style="width: 170px">所属村</label>
                                        <div class="layui-input-inline"  style="width: 200px" >
                                            <select id="village" name="village" lay-search lay-filter="village">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="layui-inline">
                                        <label class="layui-form-label" style="width: 170px">所在自然村</label>
                                        <div class="layui-input-inline"  style="width: 200px" >
                                            <select id="ucp_village" name="ucp_village" lay-search lay-filter="ucp_village">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="layui-inline">
                                        <label class="layui-form-label" style="width: 170px">所属双联户</label>
                                        <div class="layui-input-inline"  style="width: 200px" >
                                            <select id="union_family" name="union_family" lay-search lay-filter="union_family">
                                                <option value=""></option>
                                            </select>
                                        </div>
                                    </div>

                                </div>

                                <%--按钮区--%>
                                <div class="layui-form-item" style="padding-left: 70%">
                                    <div class="layui-input-block">
                                        <button type="submit" class="layui-btn" lay-submit
                                                lay-filter="family_info_query">查询</button>
                                        <button type="reset" class="layui-btn layui-btn-normal"
                                                id="family_info_query_reset" >重置</button>
                                    </div>
                                </div>

                            </form>
                            <%--查询表单结束--%>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <%--表格展示--%>
    <div class="layui-row">
        <div class="layui-col-md12">
            <div class="layui-bg-gray" style="padding: 10px;">
                <div class="layui-row">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header layui-bg-blue">家庭信息管理</div>
                            <div class="layui-card-body">
                                <%--数据展示--%>
                                <table class="layui-hide" id="family_info" lay-filter="family_info"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--隐藏域：接受村庄编号，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            id="village_id"/>
    <%--隐藏域：接受自然村编号，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            id="ucp_village_id"/>
    <%--隐藏域：接受双联户编号，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            id="unionfamily_id"/>
    <%--隐藏域：接受家庭编号，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            id="family_id"/>
<<<<<<< HEAD
    <%--隐藏域：接受户号（户口本记录），以便子页面获取--%>
=======
    <%--隐藏域：接受双联户户长姓名，以便子页面获取--%>
>>>>>>> origin/master
    <input  type="hidden" class="layui-input" style="display:none"
            id="family_num"/>
    <%--隐藏域：接受户名，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            id="family_name"/>

    <%--表格上方工具栏--%>
    <script type="text/html" id="toolbar">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-primary layui-border-green layui-btn-sm"
                    lay-event="export" id="export" >导出当前查询数据</button>
            <button class="layui-btn layui-btn-sm" lay-event="addAFamily"
                    id="addAFamily">新增家庭</button>
        </div>
    </script>

    <%--表格内部工具栏--%>
    <script type="text/html" id="baseInfo">
        <a class="layui-btn layui-btn-xs" lay-event="more" style="margin-top:4px">更多
            <i class="layui-icon layui-icon-down"></i>
        </a>
    </script>

    <%--表格内部工具栏2：查看附属信息--%>
    <script type="text/html" id="otherInfo">
        <a class="layui-btn layui-btn-xs" lay-event="showOtherInfo" style="margin-top:4px">查看附属信息
            <i class="layui-icon layui-icon-down"></i>
        </a>
    </script>

</script>
<%--本页js--%>
<script>

    layui.use(['table','upload','laydate','common','element','form',
        'layer', 'util','laypage','dropdown'], function(){
        var common = layui.common;
        var element = layui.element;
        var layer = layui.layer;
        var util = layui.util;
        var $ = layui.jquery;
        var table = layui.table;
        var form = layui.form;
        var laydate = layui.laydate;
        var laypage = layui.laypage;
        var upload = layui.upload;
        var dropdown = layui.dropdown;

        //获取数据库村庄数据并完成下拉框绑定
        bindVillageSelectData();
        //当村庄下拉框有数据被选中后，立即赋值village_id
        form.on('select(village)', function(data){
            //选中后为所属村id赋值
            const villageID = data.value;
            bindUnionFamilySelectDataByVillageID(villageID)
            bindUCPVillageSelectDataByVillageID(villageID);
        });

        //定义导出报表的数据
        let exportData = {};

        //表格数据读取参数
        var family_info_table = table.render({
            elem: '#family_info'
            ,url:'baseInfoServlet?action=queryAllFamilyInfo'
            ,toolbar: '#toolbar'
            ,defaultToolbar: []
            ,title: '家庭信息表'
            ,request: {
                pageName: 'curr'
                ,limitName: 'nums'
            }
            ,limit:5
            ,limits:[5,10,15]
            ,cols: [[
                {field:'family_num', title:'户号',align:"center"}
                ,{field:'family_name', title:'户名',align:"center"}
                ,{field:'family_type',title:'户别（家庭类型）',align:"center"}
                ,{field:'address', title:'住址',align:"center"}
                ,{field:'village', title:'所属村',width: 240,align:"center"}
                ,{field:'unionfamily', title:'所属双联户编号',width: 240,align:"center"}
                ,{field:'ucp_village', title:'所属自然村',width: 240,align:"center"}
                ,{field:'other_info', title:'附属信息',width: 150,toolbar: '#otherInfo',align:"center"}
                ,{fixed: 'right', title:'操作',align:"center", toolbar: '#baseInfo',width:150}
            ]]
            ,page: true
            ,parseData: function(res) {
                //将本次查询的数据赋值给导出数据指定的变量
                exportData = res.count;
                return {
                    "code": res.code, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": res.count.length, //解析数据长度
                    "data": res.data //解析数据列表
                };
            }
        });

        /*设定表格工具事件*/
        table.on('toolbar(family_info)', function(obj){
            var body;
            switch(obj.event){
                case 'addAFamily':
                    layer.open({
                        type: 2,
                        title: '新增家庭',
                        maxmin: true,
                        area: ['900px', '680px'],
                        content: "pages/service/baseinfo/family/_addAFamily.jsp",
                        anim:2,
                        resize:false,
                        id:'LAY_layuipro',
                        btn:['提交','重置'],
                        yes:function (index, layero) {
                            body = layer.getChildFrame('body', index); //提交按钮的回调
                            body.find('#addAFamilySubmit').click();// 找到隐藏的提交按钮模拟点击提交
                        },
                        btn2: function (index, layero) {
                            body = layer.getChildFrame('body', index); //重置按钮的回调
                            body.find('#addAFamilyReset').click(); // 找到隐藏的提交按钮模拟点击提交
                            return false;// 开启该代码可禁止点击该按钮关闭
                        },
                        cancel: function () {
                        }
                    });

                    break;
                case 'export':
                    table.exportFile(family_info_table.config.id, exportData, 'xls');
                    break;
            };
        });

        /*设定行工具事件*/
        table.on('tool(family_info)', function(obj){
            //获得当前行数据
            var sourceData = obj.data;
            var that = this;
            //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var layEvent = obj.event;
            //获取当前页码
            var currentPage = $(".layui-laypage-skip .layui-input").val();

            //为隐藏域赋值
            $("#family_id:hidden").val(sourceData.family_id);
            $("#family_num:hidden").val(sourceData.family_num);
            $("#family_name:hidden").val(sourceData.family_name);

            //更新基础信息
            function updateBaseInfo(sourceData){
<<<<<<< HEAD
=======
                //为隐藏域赋值

>>>>>>> origin/master
                var updateLayer = layer.open({
                    type: 2,
                    title: '更新家庭基础信息',
                    maxmin: true,
                    area: ['600px', '500px'],
                    content: "pages/service/baseinfo/family/_updateFamilyBaseInfo.jsp",
                    anim:2,
                    id:'LAY_layuipro',
                    resize:false,
                    btn:['更新','取消'],
                    success: function (layero, index) {
                        var body = layer.getChildFrame('body', index);
                        //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        var iframeWin = window[layero.find('iframe')[0]['name']];
                        //初始化表单数据的值
                        body.find("#family_num").val(sourceData.family_num);
                        body.find("#family_name").val(sourceData.family_name);
                        body.find("#family_type").val(sourceData.family_type);
                        body.find("#address").val(sourceData.address);
                        //为所属村庄过后台数据库拉取数据并绑定下拉框
                        $.ajax({
                            url: 'baseInfoServlet?action=queryAllVillageInfo',
                            dataType: 'json',
                            type: 'post',
                            success: function (result) {
                                var validData = result.data;
                                $.each(validData, function (index, item) {
                                    if (sourceData.village_id == item.village_id) {
                                        body.find('[id=village]').append(
                                            "<option value='"+item.village_id+"' selected>"+item.village_name+"</option>"
                                        );
                                    } else {
                                        body.find('[id=village]').append(
                                            $("<option>").attr("value", item.village_id).text(item.village_name));
                                    }
                                });

                                //重新渲染，特别重要，不然写的不起作用
                                iframeWin.layui.form.render("select");
                            }
                        });

                        //为所属自然村过后台数据库拉取数据并绑定下拉框
                        $.ajax({
                            url: 'baseInfoServlet?action=queryUCPVillageByVillageID',
                            dataType: 'json',
                            data: {
                                village_id: sourceData.village_id
                            },
                            type: 'post',
                            success: function (result) {
                                var validData = result.data;
                                $.each(validData, function (index, item) {
                                    if (sourceData.ucp_village_id == item.ucp_village_id) {
                                        body.find('[id=ucp_village]').append(
                                            "<option value='"+item.ucp_village_id+"' selected>"+item.ucp_village_name+"</option>"
                                        );
                                    } else {
                                        body.find('[id=ucp_village]').append(
                                            $("<option>").attr("value", item.ucp_village_id).text(item.ucp_village_name));
                                    }
                                });

                                //重新渲染，特别重要，不然写的不起作用
                                iframeWin.layui.form.render("select");
                            }
                        });

                        //为所属双联户过后台数据库拉取数据并绑定下拉框
                        $.ajax({
                            url: 'baseInfoServlet?action=queryUnionFamilesByVillageID',
                            dataType: 'json',
                            data: {
                                village_id: sourceData.village_id
                            },
                            type: 'post',
                            success: function (result) {
                                var validData = result.data;
                                $.each(validData, function (index, item) {
                                    var showName = "联户编号："+item.unionfamily_id + "||户长姓名：" + item.uf_leader_name;
                                    if (sourceData.unionfamily_id == item.unionfamily_id) {
                                        body.find('[id=union_family]').append(
                                            "<option value='"+item.unionfamily_id+"' selected>"+showName+"</option>"
                                        );
                                    } else {
                                        body.find('[id=union_family]').append(
                                            $("<option>").attr("value", item.unionfamily_id).text(showName));
                                    }
                                });

                                //重新渲染，特别重要，不然写的不起作用
                                iframeWin.layui.form.render("select");
                            }
                        });
                    },
                    yes:function (index, layero) {
                        //更新按钮的回调
                        var body = layer.getChildFrame('body', index);
                        // 找到隐藏的提交按钮模拟点击提交
                        body.find('#updateFamilySubmit').click();
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    btn2: function (index, layero) {
                        //取消按钮的回调
                        layer.close(updateLayer);
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    cancel: function () {
                        layer.close(updateLayer);
                        //右上角关闭回调
                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                });
            }

            //删除这条信息
            function deleteThisData(sourceData) {
                var deleteLayer = layer.confirm('确定删除该家庭吗？', function(index){
                    obj.del();
                    layer.close(index);
                    $.ajax({
                        type : 'POST',
                        url : 'baseInfoServlet?action=deleteAFamilyByFamilyNum',
                        data : {
                            family_id : sourceData.family_id,
                            family_num : sourceData.family_num
                        },
                        dataType : 'json',
                        success : function(data) {
                            if(data.code < 1){
                                layer.msg("删除失败："+data.msg, {
                                    icon : 5
                                });
                            }
                            else {
                                layer.msg('已删除', {
                                    icon : 6,
                                });
                                //重载表格
                                table.reload('family_info', {
                                    url: 'baseInfoServlet?action=queryAllFamilyInfo'
                                    ,page: {
                                        curr: currentPage //从数据发生所在页开始
                                    }
                                    ,request: {
                                        pageName: 'curr'
                                        ,limitName: 'nums'
                                    }
                                });
                                //关闭此页面
                                layer.close(deleteLayer);
                            }
                        },
                        error : function(data) {
                            // 异常提示
                            layer.msg('出现网络故障', {
                                icon : 5
                            });
                        }
                    });
                });
            }

            //添加户口本首页照片
            function add_family_rb_photo(sourceData) {
                //为隐藏域赋值
                $("#unionfamily_id:hidden").val(sourceData.unionfamily_id);
                var add_UCPVillage_picture_Layer = layer.open({
                    type: 2,
                    title: '添加双联户图片',
                    maxmin: true,
                    area: ['900px', '500px'],
                    content: "pages/service/baseinfo/unionfamily/_addUnionFamilyPhoto.jsp",
                    anim:2,
                    id:'LAY_layuipro',
                    resize:false,
                    btn:['添加','取消'],
                    success: function (layero, index) {
                        var body = layer.getChildFrame('body', index);
                        //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        var iframeWin = window[layero.find('iframe')[0]['name']];
                        //初始化表单数据的值
                        body.find("#uf_leader_bankcard_num").val(data.uf_leader_bankcard_num);
                        body.find("#uf_leader_phone_num").val(data.uf_leader_phone_num);
                        body.find("#uf_leader_idcard_num").val(data.uf_leader_idcard_num);
                        body.find("#uf_leader_name").val(data.uf_leader_name);
                        body.find("#uf_attribute").val(data.uf_attribute);
                        body.find("#unionfamily_id").val(data.unionfamily_id);
                        body.find("#village").val(data.village);
                    },
                    yes:function (index, layero) {
                        //更新按钮的回调
                        var body = layer.getChildFrame('body', index);
                        // 找到隐藏的提交按钮模拟点击提交
                        body.find('#addUnionFamilyPhotoSubmit').click();
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    btn2: function (index, layero) {
                        //取消按钮的回调
                        layer.close(add_UCPVillage_picture_Layer);
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    cancel: function () {
                        layer.close(add_UCPVillage_picture_Layer);
                        //右上角关闭回调
                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                });
            }

            //更新户口本首页照片信息
            function update_family_rb_photo(sourceData) {
                var updateLayer = layer.open({
                    type: 2,
                    title: '更新家庭户口本首页图片信息',
                    maxmin: true,
                    area: ['900px', '700px'],
                    content: "pages/service/baseinfo/family/_updateFamilyRBPhoto.jsp",
                    anim:2,
                    id:'LAY_layuipro',
                    resize:false,
                    btn:['完成并退出'],
                    yes:function (index, layero) {
                        layer.close(updateLayer);
                        //重载表格
                        layui.table.reload('family_info', {
                            url: 'baseInfoServlet?action=queryAllFamilyInfo'
                            ,page: {
                                curr: currentPage
                            }
                            ,request: {
                                pageName: 'curr'
                                ,limitName: 'nums'
                            }
                        });
                    },
                    cancel: function () {}
                });
            }

            //打包并下载户口本首页照片
            function download_family_rb_photo(sourceData) {
                var url = "baseInfoServlet?action=downloadFamilyRBPhotoByFamilyNum&family_num="+sourceData.family_num;
                window.open(url);
            }

            //显示家庭户口本首页
            function show_family_rb_photo(sourceData) {
                layer.photos({
                    photos: sourceData.photo_family_rb //格式见API文档手册页
                    ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机
                });
            }

            //更新双联户基本信息
            if(layEvent === 'more'){
                dropdown.render({
                    elem: that
                    ,show: true //外部事件触发即显示
                    ,data: [{
                        title: '删除家庭信息'
                        ,id: 'delete'
                    }, {
                        title: '更新基础信息'
                        ,id: 'update'
                    }, {
                        title: '更新图片信息'
                        ,id: 'update_family_rb_photo'
                    }, {
                        title: '打包下载图片'
                        ,id: 'download_family_rb_photo'
                    }]
                    ,click: function(dropDownData, othis){
                        if(dropDownData.id === 'update'){
                            updateBaseInfo(sourceData);
                        }
                        else if(dropDownData.id === 'delete'){
                            deleteThisData(sourceData);
                        }
                        else if(dropDownData.id === 'update_family_rb_photo'){
                            update_family_rb_photo(sourceData);
                        }
                        else if(dropDownData.id === 'download_family_rb_photo'){
                            download_family_rb_photo(sourceData);
                        }
                    }
                    ,align: 'right' //右对齐弹出（v2.6.8 新增）
                    ,style: 'box-shadow: 1px 1px 10px rgb(0 0 0 / 12%);' //设置额外样式
                });
            }
            else if(layEvent === 'showOtherInfo'){
                dropdown.render({
                    elem: that
                    ,show: true //外部事件触发即显示
                    ,data: [{
                        title: '查看户口本首页'
                        ,id: 'show_family_rb_photo'
                    }]
                    ,click: function(data, othis){
                        if(data.id === 'show_family_rb_photo'){
                            show_family_rb_photo(sourceData);
                        }
                    }
                    ,align: 'right' //右对齐弹出（v2.6.8 新增）
                    ,style: 'box-shadow: 1px 1px 10px rgb(0 0 0 / 12%);' //设置额外样式
                });
            }
        });

        //监听查询模块提交事件
        form.on('submit(family_info_query)', function(data){
            var sourceData = data.field;

            //户号（户口本记录）
            var family_num = sourceData.family_num;
            //户名
            var family_name = sourceData.family_name;
            //户别（家庭类型）
            var family_type = sourceData.family_type;
            //住址
            var address = sourceData.address;
            //所属行政村编号id
            var village_id = sourceData.village;
            //所属自然村编号id
            var ucp_village_id = sourceData.ucp_village;
            //所属双联户id
            var union_family_id = sourceData.union_family;

            //重载表格
            family_info_table.reload({
                url: 'baseInfoServlet?action=querySomeFamilyInfo'
                ,where: {
                    family_num : family_num,
                    family_name : family_name,
                    family_type : family_type,
                    address : address,
                    village_id : village_id,
                    ucp_village_id : ucp_village_id,
                    union_family_id : union_family_id
                }
                ,page:true
                ,request: {
                    pageName: 'curr'
                    ,limitName: 'nums'
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
                ,parseData: function(res) {
                    //将本次查询的数据赋值给导出数据指定的变量
                    exportData = res.count;
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.msg, //解析提示文本
                        "count": res.count.length, //解析数据长度
                        "data": res.data //解析数据列表
                    };
                }
            });

            return false;
        });

    });
</script>

</body>
</html>

