<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>

<body>
    <%--主体信息--%>
    <div class="layui-bg-gray" >
        <%--信息查询--%>
        <div class="layui-row">
            <div class="layui-bg-gray" style="padding: 10px;">
                <div class="layui-row">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header layui-bg-blue">自然村信息查询</div>
                            <div class="layui-card-body">
                                <%--查询表单开始--%>
                                <form class="layui-form layui-form-pane">

                                    <div class="layui-form-item">
                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">自然村名</label>
                                            <div class="layui-input-inline"  style="width: 204px">
                                                <input type="text" name="ucp_village_name"
                                                       placeholder="请输入自然村名称"
                                                       autocomplete=“off”
                                                       class="layui-input">
                                            </div>
                                        </div>

                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">所属村</label>
                                            <div class="layui-input-inline"  style="width: 204px" >
                                                <select id="village" name="village" lay-search>
                                                    <option value=""></option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">距离村委会公里数</label>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="apart_from_village_min"
                                                       placeholder="公里" autocomplete="off" class="layui-input">
                                            </div>
                                            <div class="layui-form-mid">至</div>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="apart_from_village_max"
                                                       placeholder="公里" autocomplete="off" class="layui-input">
                                            </div>
                                        </div>

                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">耕地面积</label>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="plough_area_min"
                                                       placeholder="亩" autocomplete="off" class="layui-input">
                                            </div>
                                            <div class="layui-form-mid">至</div>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="plough_area_max"
                                                       placeholder="亩" autocomplete="off" class="layui-input">
                                            </div>
                                        </div>

                                    </div>

                                    <div class="layui-form-item">

                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">平均海拔</label>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="elevation_min"
                                                       placeholder="米" autocomplete="off" class="layui-input">
                                            </div>
                                            <div class="layui-form-mid">至</div>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="elevation_max"
                                                       placeholder="米" autocomplete="off" class="layui-input">
                                            </div>
                                        </div>


                                        <div class="layui-inline">
                                            <label class="layui-form-label" style="width: 150px">草场面积</label>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="meadow_area_min"
                                                       placeholder="亩" autocomplete="off" class="layui-input">
                                            </div>
                                            <div class="layui-form-mid">至</div>
                                            <div class="layui-input-inline" style="width: 85px;">
                                                <input type="text" name="meadow_area_max"
                                                       placeholder="亩" autocomplete="off" class="layui-input">
                                            </div>
                                        </div>

                                    </div>

                                    <%--按钮区--%>
                                    <div class="layui-form-item" style="padding-left: 70%">
                                        <div class="layui-input-block">
                                            <button type="submit" class="layui-btn" lay-submit
                                                    lay-filter="ucpvillage_info_query">查询</button>
                                            <button type="reset" class="layui-btn layui-btn-normal"
                                                    id="ucpvillage_info_query_reset" >重置</button>
                                        </div>
                                    </div>

                                </form>
                                <%--查询表单结束--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <%--表格展示--%>
        <div class="layui-row">
            <div class="layui-col-md12">
                <div class="layui-bg-gray" style="padding: 10px;">
                    <div class="layui-row">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header layui-bg-blue">自然村信息管理</div>
                                <div class="layui-card-body">

                                    <%--数据展示--%>
                                    <table class="layui-hide" id="UCPVillageinfo"
                                           lay-filter="UCPVillageinfo"></table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--隐藏域：接受村庄编号，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            name="village_id" id="village_id"/>
    <%--隐藏域：接受自然村编号，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            name="ucp_village_id" id="ucp_village_id"/>

    <%--表格上方工具栏--%>
    <script type="text/html" id="toolbar">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-primary layui-border-green layui-btn-sm"
                    lay-event="export" id="export" >导出当前查询数据</button>
            <button class="layui-btn layui-btn-sm" lay-event="addAnUCPVillage" id="addAnUCPVillage">新增自然村</button>
        </div>
    </script>

    <%--表格内部工具栏--%>
    <script type="text/html" id="baseInfo">
        <a class="layui-btn layui-btn-xs" lay-event="update_base_info" >
            修改基础信息
        </a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">
            删除
        </a>
    </script>
    <%--表格内部工具栏2：查看自然村图像--%>
    <script type="text/html" id="show_picture">

    {{# if(d.UCPVillage_picture.data.length <=0){ }}
        <a class="layui-btn layui-btn-xs layui-btn-primary layui-border-black"
           style="margin-top:1px">
            暂无图片
        </a>

        <a class="layui-btn layui-btn-xs layui-btn-danger"
           lay-event="add_UCPVillage_picture">
            添加图片
        </a>
    {{#  } }}

    {{# if(d.UCPVillage_picture.data.length > 0){ }}
        <a class="layui-btn layui-btn-xs layui-btn-primary layui-border-green"
           lay-event="show_UCPVillage_picture"
           style="margin-top:1px">
            显示图片
        </a>

        <a class="layui-btn layui-btn-xs layui-btn-primary layui-border-green"
           lay-event="download_UCPVillage_picture"
           style="margin-top:1px">
            下载图片
        </a>

        <a class="layui-btn layui-btn-xs"
           lay-event="update_UCPVillage_picture_info">
            修改图片信息
        </a>
    {{#  } }}

    </script>

<%--本页js--%>
<script>

    layui.use(['table','upload','laydate','common','element','form',
        'layer', 'util','laypage'], function(){
        var common = layui.common;
        var element = layui.element;
        var layer = layui.layer;
        var util = layui.util;
        var $ = layui.jquery;
        var table = layui.table;
        var form = layui.form;
        var laydate = layui.laydate;
        var laypage = layui.laypage;
        var upload = layui.upload;

        //获取数据库村庄数据并完成下拉框绑定
        bindVillageSelectData();

        //定义导出报表的数据
        let exportData = {};

        //表格数据读取参数
        var ucp_village_info_table = table.render({
            elem: '#UCPVillageinfo'
            ,url:'baseInfoServlet?action=queryAllUCPVillageInfo'
            ,toolbar: '#toolbar'
            ,defaultToolbar: []
            ,title: '自然村信息表'
            ,request: {
                pageName: 'curr'
                ,limitName: 'nums'
            }
            ,limit:5
            ,limits:[5,10,15]
            ,cols: [[
                {field:'ucp_village_id', title:'自然村编号',align:"center"}
                ,{field:'ucp_village_name', title:'自然村名称',align:"center"}
                ,{field:'village', title:'所属村',align:"center"}
                ,{field:'apart_from_village', title:'距离村委会公里数（公里）',width:200,align:"center"}
                ,{field:'plough_area', title:'耕地面积（亩）',align:"center"}
                ,{field:'elevation', title:'平均海拔（米）',align:"center"}
                ,{field:'meadow_area', title:'草场面积（亩）',align:"center"}
                ,{field:'UCPVillage_picture', title:'自然村图像', width:280,align:"center", toolbar: '#show_picture'}
                ,{fixed: 'right', title:'操作',align:"center", width:180,toolbar: '#baseInfo'}
            ]]
            ,page: true
            ,parseData: function(res) {
                //将本次查询的数据赋值给导出数据指定的变量
                exportData = res.count;
                return {
                    "code": res.code, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": res.count.length, //解析数据长度
                    "data": res.data //解析数据列表
                };
            }
        });

        /*设定表格工具事件*/
        table.on('toolbar(UCPVillageinfo)', function(obj){
            var body;
            switch(obj.event){
                case 'addAnUCPVillage':
                    layer.open({
                        type: 2,
                        title: '新增自然村',
                        maxmin: true,
                        area: ['900px', '680px'],
                        content: "pages/service/baseinfo/ucpVillage/_addAnUCPVillage.jsp",
                        anim:2,
                        resize:false,
                        id:'LAY_layuipro',
                        btn:['提交','重置'],
                        yes:function (index, layero) {
                            body = layer.getChildFrame('body', index); //提交按钮的回调
                            body.find('#addAnUCPVillageSubmit').click();// 找到隐藏的提交按钮模拟点击提交
                        },
                        btn2: function (index, layero) {
                            body = layer.getChildFrame('body', index); //重置按钮的回调
                            body.find('#addAnUCPVillageReset').click(); // 找到隐藏的提交按钮模拟点击提交
                            return false;// 开启该代码可禁止点击该按钮关闭
                        },
                        cancel: function () {
                        }
                    });

                    break;
                case 'export':
                    table.exportFile(ucp_village_info_table.config.id, exportData, 'xls');
                    break;
            };
        });

        /*设定行工具事件*/
        table.on('tool(UCPVillageinfo)', function(obj){
            //获得当前行数据
            var data = obj.data;

            //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var layEvent = obj.event;
            //获取当前页码
            var currentPage = $(".layui-laypage-skip .layui-input").val();

            //更新自然村信息
            if(layEvent === 'update_base_info'){
                //为隐藏域赋值
                $("#ucp_village_id:hidden").val(data.ucp_village_id);
                $("#village_id:hidden").val(data.village_id);

                var updateLayer = layer.open({
                    type: 2,
                    title: '更新自然村基础信息',
                    maxmin: true,
                    area: ['600px', '500px'],
                    content: "pages/service/baseinfo/ucpVillage/_updateUCPVillageInfo.jsp",
                    anim:2,
                    id:'LAY_layuipro',
                    resize:false,
                    btn:['更新','取消'],
                    success: function (layero, index) {
                        var body = layer.getChildFrame('body', index);
                        //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        var iframeWin = window[layero.find('iframe')[0]['name']];
                        //初始化表单数据的值
                        body.find("#ucp_village_name").val(data.ucp_village_name);
                        body.find("#apart_from_village").val(data.apart_from_village);
                        body.find("#plough_area").val(data.plough_area);
                        body.find("#elevation").val(data.elevation);
                        body.find("#meadow_area").val(data.meadow_area);
                        //为所属乡镇通过后台数据库拉取数据并绑定下拉框
                        $.ajax({
                            url: 'baseInfoServlet?action=queryAllVillageInfo',
                            dataType: 'json',
                            type: 'post',
                            success: function (sourceData) {
                                var SourceData = sourceData.data;
                                $.each(SourceData, function (index, item) {
                                    if (data.village_id == item.village_id) {
                                        body.find('[id=village]').append(
                                            "<option value='"+item.village_id+"' selected>"+item.village_name+"</option>"
                                        );
                                    } else {
                                        body.find('[id=village]').append(
                                            $("<option>").attr("value", item.village_id).text(item.village_name));
                                    }
                                });

                                //重新渲染，特别重要，不然写的不起作用
                                iframeWin.layui.form.render("select");
                            }
                        });
                    },
                    yes:function (index, layero) {
                        //更新按钮的回调
                        var body = layer.getChildFrame('body', index);
                        // 找到隐藏的提交按钮模拟点击提交
                        body.find('#updateAnUCPVillageSubmit').click();
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    btn2: function (index, layero) {
                        //取消按钮的回调
                        layer.close(updateLayer);
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    cancel: function () {
                        layer.close(updateLayer);
                        //右上角关闭回调
                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                });
            }
            //删除此自然村
            else if(layEvent === 'delete'){
                var deleteLayer = layer.confirm('确定删除该自然村吗？', function(index){
                    obj.del();
                    layer.close(index);
                    $.ajax({
                        type : 'POST',
                        url : 'baseInfoServlet?action=deleteAnUCPVillageByUCPVillageId',
                        data : {
                            ucp_village_id : data.ucp_village_id,
                        },
                        dataType : 'json',
                        success : function(data) {
                            if(data.code < 1){
                                layer.msg("删除失败："+data.msg, {
                                    icon : 5
                                });
                            }
                            else {
                                layer.msg('已删除', {
                                    icon : 6,
                                });
                                //重载表格
                                table.reload('UCPVillageinfo', {
                                    url: 'baseInfoServlet?action=queryAllUCPVillageInfo'
                                    ,page: {
                                        curr: currentPage //从数据发生所在页开始
                                    }
                                    ,request: {
                                        pageName: 'curr'
                                        ,limitName: 'nums'
                                    }
                                });
                                //关闭此页面
                                layer.close(deleteLayer);
                            }
                        },
                        error : function(data) {
                            // 异常提示
                            layer.msg('出现网络故障', {
                                icon : 5
                            });
                        }
                    });
                });
            }
            //查看自然村图像
            else if(layEvent === 'show_UCPVillage_picture'){
                layer.photos({
                    photos: data.UCPVillage_picture //格式见API文档手册页
                    ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机
                });
            }
            //新增自然村图像
            else if(layEvent === 'add_UCPVillage_picture'){
                //为隐藏域赋值
                $("#ucp_village_id:hidden").val(data.ucp_village_id);
                $("#village_id:hidden").val(data.village_id);
                var add_UCPVillage_picture_Layer = layer.open({
                    type: 2,
                    title: '添加自然村图片',
                    maxmin: true,
                    area: ['900px', '500px'],
                    content: "pages/service/baseinfo/ucpVillage/_addUCPVillagePhoto.jsp",
                    anim:2,
                    id:'LAY_layuipro',
                    resize:false,
                    btn:['添加','取消'],
                    success: function (layero, index) {
                        var body = layer.getChildFrame('body', index);
                        //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        var iframeWin = window[layero.find('iframe')[0]['name']];
                        //初始化表单数据的值
                        body.find("#ucp_village_name").val(data.ucp_village_name);
                        body.find("#apart_from_village").val(data.apart_from_village);
                        body.find("#plough_area").val(data.plough_area);
                        body.find("#elevation").val(data.elevation);
                        body.find("#meadow_area").val(data.meadow_area);
                        body.find("#village").val(data.village);
                    },
                    yes:function (index, layero) {
                        //更新按钮的回调
                        var body = layer.getChildFrame('body', index);
                        // 找到隐藏的提交按钮模拟点击提交
                        body.find('#addUCPVillagePhotoSubmit').click();
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    btn2: function (index, layero) {
                        //取消按钮的回调
                        layer.close(add_UCPVillage_picture_Layer);
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    cancel: function () {
                        layer.close(add_UCPVillage_picture_Layer);
                        //右上角关闭回调
                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                });
            }
            //更新自然村图像
            else if(layEvent === 'update_UCPVillage_picture_info'){
                $("#ucp_village_id:hidden").val(data.ucp_village_id);
                var updateLayer = layer.open({
                    type: 2,
                    title: '更新自然村图片信息',
                    maxmin: true,
                    area: ['900px', '700px'],
                    content: "pages/service/baseinfo/ucpVillage/_updateUCPVillagePhotoInfo.jsp",
                    anim:2,
                    id:'LAY_layuipro',
                    resize:false,
                    btn:['完成并退出'],
                    yes:function (index, layero) {
                        layer.close(updateLayer);
                    },
                    cancel: function () {}
                });
            }
            //下载自然村图像
            else if(layEvent === 'download_UCPVillage_picture'){
                var url = "baseInfoServlet?action=downloadUcpVillagePhoto&ucp_village_id="+data.ucp_village_id;
                window.open(url);
            }
        });


        //监听查询模块提交事件
        form.on('submit(ucpvillage_info_query)', function(data){
            var sourceData = data.field;

            //自然村名称
            var ucp_village_name = sourceData.ucp_village_name;
            var village_id = sourceData.village;
            var apart_from_village_min = sourceData.apart_from_village_min;
            var apart_from_village_max = sourceData.apart_from_village_max;
            var plough_area_min = sourceData.plough_area_min;
            var plough_area_max = sourceData.plough_area_max;
            var elevation_min = sourceData.elevation_min;
            var elevation_max = sourceData.elevation_max;
            var meadow_area_min = sourceData.meadow_area_min;
            var meadow_area_max = sourceData.meadow_area_max;

            //重载表格
            ucp_village_info_table.reload({
                url: 'baseInfoServlet?action=querySomeUCPVillageInfo'
                ,where: {
                    ucp_village_name : ucp_village_name,
                    village_id : village_id,
                    apart_from_village_min : apart_from_village_min,
                    apart_from_village_max : apart_from_village_max,
                    plough_area_min : plough_area_min,
                    plough_area_max : plough_area_max,
                    elevation_min : elevation_min,
                    elevation_max : elevation_max,
                    meadow_area_min : meadow_area_min,
                    meadow_area_max : meadow_area_max
                }
                ,page:true
                ,request: {
                    pageName: 'curr'
                    ,limitName: 'nums'
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
                ,parseData: function(res) {
                    //将本次查询的数据赋值给导出数据指定的变量
                    exportData = res.count;
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.msg, //解析提示文本
                        "count": res.count.length, //解析数据长度
                        "data": res.data //解析数据列表
                    };
                }
            });

            return false;
        });

    });
</script>

</body>
</html>
