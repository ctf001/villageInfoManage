<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%--更新自然村信息--%>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>
<div style="padding: 10px">

    <form class="layui-form layui-form-pane">

        <div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">自然村名称</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text" name="ucp_village_name"
                           lay-verify="required" id="ucp_village_name"
                           class="layui-input">
                </div>
             </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 200px">所属村</label>
                <div class="layui-input-inline"  style="width: 350px" >
                    <select id="village" name="village" lay-search lay-filter="village">
                        <option value=""></option>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">耕地面积（亩）</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text" name="plough_area"
                           lay-verify="number|required|positive"
                           id="plough_area"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">距离村委会公里数（公里）</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text" name="apart_from_village"
                           lay-verify="number|required|positive"
                           id="apart_from_village"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">平均海拔（米）</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text" name="elevation"
                           lay-verify="number|required|positive"
                           id="elevation"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">草场面积（亩）</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text" name="meadow_area"
                           lay-verify="number|required|positive"
                           id="meadow_area"
                           class="layui-input">
                </div>
            </div>

        </div>

        <button type="submit" class="layui-btn" style="display:none"
                id="updateAnUCPVillageSubmit" lay-submit
                lay-filter="updateAnUCPVillageSubmit"></button>
    </form>

</div>

<script type="text/javascript">

    layui.use(['laydate','form','common','table','upload','element'], function() {
        var laydate = layui.laydate;
        var form = layui.form;
        var $ = layui.jquery;
        var index = parent.layer.getFrameIndex(window.name);
        var common = layui.common;
        var upload = layui.upload;
        var element = layui.element;

        //所属村庄id
        var village_id = parent.$("#village_id:hidden").val();

        form.render();

        //非负数据验证
        form.verify({
            positive: [
                /([1-9]\d*\.?\d*)|(0\.\d*)|0/
                , '只能输入正数'
            ]
        });

        form.on('select(village)', function(data)   {
            //选中后为所属乡镇id赋值
            village_id = data.value;
        });

        form.on('submit(updateAnUCPVillageSubmit)', function(data){
            const sourceData = data.field;

            //自然村名称
            const ucp_village_name = sourceData.ucp_village_name;
            //距离村委会公里数（公里）
            const apart_from_village = sourceData.apart_from_village;
            //耕地面积（亩）
            const plough_area = sourceData.plough_area;
            //平均海拔（米）
            const elevation = sourceData.elevation;
            //草场面积（亩）
            const meadow_area = sourceData.meadow_area;
            //从父页面获取数据发生页所在页码
            var currentPage = parent.$(".layui-laypage-skip .layui-input").val();
            //从父页面自然村编号
            var ucp_village_id = parent.$("#ucp_village_id:hidden").val();

            $.ajax({
                type : 'POST',
                url : 'baseInfoServlet?action=updateUCPVillageInfo',
                data : {
                    ucp_village_id:ucp_village_id,
                    ucp_village_name:ucp_village_name,
                    village_id:village_id,
                    apart_from_village:apart_from_village,
                    plough_area:plough_area,
                    elevation:elevation,
                    meadow_area:meadow_area
                },
                dataType : 'json',
                success : function(data) {
                    if(data.statusCode != 1){
                        parent.layer.msg(data.msg, {
                            icon : 5
                        });
                    }
                    else {
                        // 基础信息插入成功
                        parent.layer.msg('基础信息更新成功', {
                            icon : 6,
                        });

                        //重载表格
                        parent.layui.table.reload('UCPVillageinfo', {
                            url: 'baseInfoServlet?action=queryAllUCPVillageInfo'
                            ,page: {
                                curr: currentPage
                            }
                            ,request: {
                                pageName: 'curr'
                                ,limitName: 'nums'
                            }
                        });

                        //关闭此页面
                        parent.layer.close(index);
                    }
                },
                error : function(data) {
                    // 异常提示
                    parent.layer.msg('出现网络故障', {
                        icon : 5
                    });
                    //关闭此页面
                    parent.layer.close(index);
                }
            });
            return false;
        });


    });
</script>
</body>
</html>
