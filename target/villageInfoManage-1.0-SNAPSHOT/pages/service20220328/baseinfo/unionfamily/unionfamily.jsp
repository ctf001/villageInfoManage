<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>
    <div class="layui-layout layui-layout-admin">
        <%--头部信息--%>
        <%@include file="/pages/common/header.jsp"%>

        <%--左侧导航--%>
        <%@include file="/pages/common/menu.jsp"%>

        <%--主体信息--%>
        <div class="layui-body layui-bg-gray" style="padding: 10px">
            <%--信息查询--%>
            <div class="layui-row">
                <div class="layui-bg-gray" style="padding: 10px;">
                    <div class="layui-row">
                        <div class="layui-col-md12">
                            <div class="layui-card">
                                <div class="layui-card-header layui-bg-blue">双联户信息查询</div>
                                <div class="layui-card-body">
                                    <%--查询表单开始--%>
                                    <form class="layui-form layui-form-pane">
                                        <div class="layui-form-item">
                                            <div class="layui-inline">
                                                <label class="layui-form-label" style="width:170px">联户编号</label>
                                                <div class="layui-input-inline" style="width:200px">
                                                    <input type="text"
                                                           name="unionfamily_id"
                                                           class="layui-input">
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <label class="layui-form-label" style="width:170px">联户单位性质</label>
                                                <div class="layui-input-inline" style="width:200px">
                                                    <input type="text"
                                                           name="uf_attribute"
                                                           class="layui-input">
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <label class="layui-form-label" style="width: 170px">所属村</label>
                                                <div class="layui-input-inline"  style="width: 200px" >
                                                    <select id="village" name="village" lay-search lay-filter="village">
                                                        <option value=""></option>
                                                    </select>
                                                </div>
                                            </div>


                                        </div>

                                        <div class="layui-form-item">

                                            <div class="layui-inline">
                                                <label class="layui-form-label" style="width:170px">双联户户长姓名</label>
                                                <div class="layui-input-inline" style="width:200px">
                                                    <input type="text"
                                                           name="uf_leader_name"
                                                           class="layui-input">
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <label class="layui-form-label" style="width:170px">双联户户长身份证号</label>
                                                <div class="layui-input-inline" style="width:200px">
                                                    <input type="text"
                                                           name="uf_leader_idcard_num"
                                                           class="layui-input">
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <label class="layui-form-label" style="width:170px">双联户户长联系电话</label>
                                                <div class="layui-input-inline" style="width:200px">
                                                    <input type="text"
                                                           name="uf_leader_phone_num"
                                                           class="layui-input">
                                                </div>
                                            </div>

                                            <div class="layui-inline">
                                                <label class="layui-form-label" style="width:170px">双联户户长银行卡号</label>
                                                <div class="layui-input-inline" style="width:200px">
                                                    <input type="text"
                                                           name="uf_leader_bankcard_num"
                                                           class="layui-input">
                                                </div>
                                            </div>
                                        </div>

                                        <%--按钮区--%>
                                        <div class="layui-form-item" style="padding-left: 70%">
                                            <div class="layui-input-block">
                                                <button type="submit" class="layui-btn" lay-submit
                                                        lay-filter="unionfamily_info_query">查询</button>
                                                <button type="reset" class="layui-btn layui-btn-normal"
                                                        id="unionfamily_info_query_reset" >重置</button>
                                            </div>
                                        </div>

                                    </form>
                                    <%--查询表单结束--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%--表格展示--%>
            <div class="layui-row">
                <div class="layui-col-md12">
                    <div class="layui-bg-gray" style="padding: 10px;">
                        <div class="layui-row">
                            <div class="layui-col-md12">
                                <div class="layui-card">
                                    <div class="layui-card-header layui-bg-blue">双联户信息管理</div>
                                    <div class="layui-card-body">

                                        <%--数据展示--%>
                                        <table class="layui-hide" id="union_family_info"
                                               lay-filter="union_family_info"></table>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <%--底部信息--%>
        <div class="layui-footer">
            <%@include file="/pages/common/footer.jsp" %>
        </div>

    </div>

    <%--隐藏域：接受村庄编号，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            id="village_id"/>
    <%--隐藏域：接受双联户编号，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            id="unionfamily_id"/>
    <%--隐藏域：接受双联户户长姓名，以便子页面获取--%>
    <input  type="hidden" class="layui-input" style="display:none"
            id="uf_leader_name"/>

    <%--表格上方工具栏--%>
    <script type="text/html" id="toolbar">
        <div class="layui-btn-container">
            <button class="layui-btn layui-btn-primary layui-border-green layui-btn-sm"
                    lay-event="export" id="export" >导出当前查询数据</button>
            <button class="layui-btn layui-btn-sm" lay-event="addAnUnionFamily"
                    id="addAnUnionFamily">新增双联户</button>
        </div>
    </script>

    <%--表格内部工具栏--%>
    <script type="text/html" id="baseInfo">
        <a class="layui-btn layui-btn-xs" lay-event="update_base_info" >
            修改基础信息
        </a>
        <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">
            删除
        </a>
    </script>
    <%--表格内部工具栏2：查看自然村图像--%>
    <script type="text/html" id="show_picture">

    {{# if(d.photo_uf_leader.data.length <=0){ }}
        <a class="layui-btn layui-btn-xs layui-btn-primary layui-border-black"
           style="margin-top:1px">
            暂无图片
        </a>

        <a class="layui-btn layui-btn-xs layui-btn-danger"
           lay-event="add_unionfamily_picture">
            添加图片
        </a>
    {{#  } }}

    {{# if(d.photo_uf_leader.data.length > 0){ }}
        <a class="layui-btn layui-btn-xs layui-btn-primary layui-border-green"
           lay-event="show_unionfamily_picture"
           style="margin-top:1px">
            显示图片
        </a>

        <a class="layui-btn layui-btn-xs layui-btn-primary layui-border-green"
           lay-event="download_unionfamily_picture"
           style="margin-top:1px">
            下载图片
        </a>

        <a class="layui-btn layui-btn-xs"
           lay-event="update_unionfamily_picture">
            修改图片信息
        </a>
    {{#  } }}

    </script>


    <script type="text/html" id="selectTpl">
        <select>
            <option value="" >请选择</option>
            <option value="1" lay-filter="lockDemo">下载双联户档案</option>
        </select>
    </script>
<%--本页js--%>
<script>

    layui.use(['table','upload','laydate','common','element','form',
        'layer', 'util','laypage'], function(){
        var common = layui.common;
        var element = layui.element;
        var layer = layui.layer;
        var util = layui.util;
        var $ = layui.jquery;
        var table = layui.table;
        var form = layui.form;
        var laydate = layui.laydate;
        var laypage = layui.laypage;
        var upload = layui.upload;

        //获取数据库村庄数据并完成下拉框绑定
        bindVillageSelectData();

        //定义导出报表的数据
        let exportData = {};

        //表格数据读取参数
        var union_family_info_table = table.render({
            elem: '#union_family_info'
            ,url:'baseInfoServlet?action=queryAllUnionFamilyInfo'
            ,toolbar: '#toolbar'
            ,defaultToolbar: []
            ,title: '双联户信息表'
            ,request: {
                pageName: 'curr'
                ,limitName: 'nums'
            }
            ,limit:5
            ,limits:[5,10,15]
            ,cols: [[
                {field:'unionfamily_id', title:'联户编号',width:100,align:"center"}
                ,{field:'uf_attribute', title:'联户单位性质',width:150,align:"center"}
                ,{field:'village', title:'所属村',width:120,align:"center"}
                ,{field:'uf_leader_name', title:'双联户户长姓名',width:160,align:"center"}
                ,{field:'uf_leader_idcard_num', title:'双联户户长身份证号',align:"center"}
                ,{field:'uf_leader_phone_num', title:'双联户户长联系电话',align:"center"}
                ,{field:'uf_leader_bankcard_num', title:'双联户户长银行卡号',align:"center"}
                ,{field:'photo_uf_leader', title:'双联户户长照片', width:280,align:"center", toolbar: '#show_picture'}
                ,{fixed: 'right', title:'操作',align:"center", width:180,toolbar: '#baseInfo'}
            ]]
            ,page: true
            ,parseData: function(res) {
                //将本次查询的数据赋值给导出数据指定的变量
                exportData = res.count;
                return {
                    "code": res.code, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": res.count.length, //解析数据长度
                    "data": res.data //解析数据列表
                };
            }
        });

        /*设定表格工具事件*/
        table.on('toolbar(union_family_info)', function(obj){
            var body;
            switch(obj.event){
                case 'addAnUnionFamily':
                    layer.open({
                        type: 2,
                        title: '新增双联户',
                        maxmin: true,
                        area: ['900px', '680px'],
                        content: "pages/service/baseinfo/unionfamily/_addAnUnionFamily.jsp",
                        anim:2,
                        resize:false,
                        id:'LAY_layuipro',
                        btn:['提交','重置'],
                        yes:function (index, layero) {
                            body = layer.getChildFrame('body', index); //提交按钮的回调
                            body.find('#addAnUnionFamilySubmit').click();// 找到隐藏的提交按钮模拟点击提交
                        },
                        btn2: function (index, layero) {
                            body = layer.getChildFrame('body', index); //重置按钮的回调
                            body.find('#addAnUnionFamilyReset').click(); // 找到隐藏的提交按钮模拟点击提交
                            return false;// 开启该代码可禁止点击该按钮关闭
                        },
                        cancel: function () {
                        }
                    });

                    break;
                case 'export':
                    table.exportFile(ucp_village_info_table.config.id, exportData, 'xls');
                    break;
            };
        });

        /*设定行工具事件*/
        table.on('tool(union_family_info)', function(obj){
            //获得当前行数据
            var data = obj.data;

            //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var layEvent = obj.event;
            //获取当前页码
            var currentPage = $(".layui-laypage-skip .layui-input").val();

            //为隐藏域赋值
            $("#unionfamily_id:hidden").val(data.unionfamily_id);
            $("#uf_leader_name:hidden").val(data.uf_leader_name);

            //更新双联户基本信息
            if(layEvent === 'update_base_info'){
                //为隐藏域赋值
                $("#ucp_village_id:hidden").val(data.ucp_village_id);
                $("#village_id:hidden").val(data.village_id);

                var updateLayer = layer.open({
                    type: 2,
                    title: '更新双联户基础信息',
                    maxmin: true,
                    area: ['600px', '500px'],
                    content: "pages/service/baseinfo/unionfamily/_updateUnionFamilyInfo.jsp",
                    anim:2,
                    id:'LAY_layuipro',
                    resize:false,
                    btn:['更新','取消'],
                    success: function (layero, index) {
                        var body = layer.getChildFrame('body', index);
                        //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        var iframeWin = window[layero.find('iframe')[0]['name']];
                        //初始化表单数据的值
                        body.find("#unionfamily_id").val(data.unionfamily_id);
                        body.find("#uf_attribute").val(data.uf_attribute);
                        body.find("#uf_leader_name").val(data.uf_leader_name);
                        body.find("#uf_leader_idcard_num").val(data.uf_leader_idcard_num);
                        body.find("#uf_leader_phone_num").val(data.uf_leader_phone_num);
                        body.find("#uf_leader_bankcard_num").val(data.uf_leader_bankcard_num);
                        //为所属乡镇通过后台数据库拉取数据并绑定下拉框
                        $.ajax({
                            url: 'baseInfoServlet?action=queryAllVillageInfo',
                            dataType: 'json',
                            type: 'post',
                            success: function (sourceData) {
                                var SourceData = sourceData.data;
                                $.each(SourceData, function (index, item) {
                                    if (data.village_id == item.village_id) {
                                        body.find('[id=village]').append(
                                            "<option value='"+item.village_id+"' selected>"+item.village_name+"</option>"
                                        );
                                    } else {
                                        body.find('[id=village]').append(
                                            $("<option>").attr("value", item.village_id).text(item.village_name));
                                    }
                                });

                                //重新渲染，特别重要，不然写的不起作用
                                iframeWin.layui.form.render("select");
                            }
                        });
                    },
                    yes:function (index, layero) {
                        //更新按钮的回调
                        var body = layer.getChildFrame('body', index);
                        // 找到隐藏的提交按钮模拟点击提交
                        body.find('#updateUnionFamilySubmit').click();
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    btn2: function (index, layero) {
                        //取消按钮的回调
                        layer.close(updateLayer);
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    cancel: function () {
                        layer.close(updateLayer);
                        //右上角关闭回调
                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                });
            }
            //删除此双联户
            else if(layEvent === 'delete'){
                var deleteLayer = layer.confirm('确定删除该双联户吗？', function(index){
                    obj.del();
                    layer.close(index);
                    $.ajax({
                        type : 'POST',
                        url : 'baseInfoServlet?action=deleteAnUnionFamilyByUnionFamilyID',
                        data : {
                            unionfamily_id : data.unionfamily_id,
                        },
                        dataType : 'json',
                        success : function(data) {
                            if(data.code < 1){
                                layer.msg("删除失败："+data.msg, {
                                    icon : 5
                                });
                            }
                            else {
                                layer.msg('已删除', {
                                    icon : 6,
                                });
                                //重载表格
                                table.reload('union_family_info', {
                                    url: 'baseInfoServlet?action=queryAllUnionFamilyInfo'
                                    ,page: {
                                        curr: currentPage //从数据发生所在页开始
                                    }
                                    ,request: {
                                        pageName: 'curr'
                                        ,limitName: 'nums'
                                    }
                                });
                                //关闭此页面
                                layer.close(deleteLayer);
                            }
                        },
                        error : function(data) {
                            // 异常提示
                            layer.msg('出现网络故障', {
                                icon : 5
                            });
                        }
                    });
                });
            }
            //查看双联户图像
            else if(layEvent === 'show_unionfamily_picture'){
                layer.photos({
                    photos: data.photo_uf_leader //格式见API文档手册页
                    ,anim: 5 //0-6的选择，指定弹出图片动画类型，默认随机
                });
            }
            //新增双联户图像
            else if(layEvent === 'add_unionfamily_picture'){
                //为隐藏域赋值
                $("#unionfamily_id:hidden").val(data.unionfamily_id);
                var add_UCPVillage_picture_Layer = layer.open({
                    type: 2,
                    title: '添加双联户图片',
                    maxmin: true,
                    area: ['900px', '500px'],
                    content: "pages/service/baseinfo/unionfamily/_addUnionFamilyPhoto.jsp",
                    anim:2,
                    id:'LAY_layuipro',
                    resize:false,
                    btn:['添加','取消'],
                    success: function (layero, index) {
                        var body = layer.getChildFrame('body', index);
                        //得到iframe页的窗口对象，执行iframe页的方法：iframeWin.method();
                        var iframeWin = window[layero.find('iframe')[0]['name']];
                        //初始化表单数据的值
                        body.find("#uf_leader_bankcard_num").val(data.uf_leader_bankcard_num);
                        body.find("#uf_leader_phone_num").val(data.uf_leader_phone_num);
                        body.find("#uf_leader_idcard_num").val(data.uf_leader_idcard_num);
                        body.find("#uf_leader_name").val(data.uf_leader_name);
                        body.find("#uf_attribute").val(data.uf_attribute);
                        body.find("#unionfamily_id").val(data.unionfamily_id);
                        body.find("#village").val(data.village);
                    },
                    yes:function (index, layero) {
                        //更新按钮的回调
                        var body = layer.getChildFrame('body', index);
                        // 找到隐藏的提交按钮模拟点击提交
                        body.find('#addUnionFamilyPhotoSubmit').click();
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    btn2: function (index, layero) {
                        //取消按钮的回调
                        layer.close(add_UCPVillage_picture_Layer);
                        //return false 开启该代码可禁止点击该按钮关闭
                    },
                    cancel: function () {
                        layer.close(add_UCPVillage_picture_Layer);
                        //右上角关闭回调
                        //return false 开启该代码可禁止点击该按钮关闭
                    }
                });
            }
            //更新双联户图像
            else if(layEvent === 'update_unionfamily_picture'){

                var updateLayer = layer.open({
                    type: 2,
                    title: '更新双联户图片信息',
                    maxmin: true,
                    area: ['900px', '700px'],
                    content: "pages/service/baseinfo/unionfamily/_updateUnionFamilyPhotoInfo.jsp",
                    anim:2,
                    id:'LAY_layuipro',
                    resize:false,
                    btn:['完成并退出'],
                    yes:function (index, layero) {
                        layer.close(updateLayer);
                    },
                    cancel: function () {}
                });
            }
            //下载双联户图像
            else if(layEvent === 'download_unionfamily_picture'){
                var url = "baseInfoServlet?action=downloadUFLeaderPhotoByUFID&unionfamily_id="+data.unionfamily_id;
                window.open(url);
            }
        });

        //监听查询模块提交事件
        form.on('submit(unionfamily_info_query)', function(data){
            var sourceData = data.field;

            //双联户户长银行卡号
            const uf_leader_bankcard_num = sourceData.uf_leader_bankcard_num;
            //双联户户长联系方式
            const uf_leader_phone_num = sourceData.uf_leader_phone_num;
            //双联户户长身份证号
            const uf_leader_idcard_num = sourceData.uf_leader_idcard_num;
            //双联户户长姓名
            const uf_leader_name = sourceData.uf_leader_name;
            //联户单位性质
            const uf_attribute = sourceData.uf_attribute;
            //联户编号
            const unionfamily_id = sourceData.unionfamily_id;
            //所属村id
            const village_id = sourceData.village;

            //重载表格
            union_family_info_table.reload({
                url: 'baseInfoServlet?action=querySomeUnionFamilyInfo'
                ,where: {
                    uf_leader_bankcard_num : uf_leader_bankcard_num,
                    uf_leader_phone_num : uf_leader_phone_num,
                    uf_leader_idcard_num : uf_leader_idcard_num,
                    uf_leader_name : uf_leader_name,
                    uf_attribute : uf_attribute,
                    unionfamily_id : unionfamily_id,
                    village_id : village_id
                }
                ,page:true
                ,request: {
                    pageName: 'curr'
                    ,limitName: 'nums'
                }
                ,page: {
                    curr: 1 //重新从第 1 页开始
                }
                ,parseData: function(res) {
                    //将本次查询的数据赋值给导出数据指定的变量
                    exportData = res.count;
                    return {
                        "code": res.code, //解析接口状态
                        "msg": res.msg, //解析提示文本
                        "count": res.count.length, //解析数据长度
                        "data": res.data //解析数据列表
                    };
                }
            });

            return false;
        });

    });
</script>

</body>
</html>
