<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%--新增自然村--%>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>
<div style="padding: 10px">

    <form class="layui-form layui-form-pane">

        <div style="margin: auto">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label" style="width:200px">自然村名称</label>
                    <div class="layui-input-inline" style="width:200px">
                        <input type="text" name="ucp_village_name"
                               lay-verify="required" id="ucp_village_name"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label" style="width: 200px">所属村</label>
                    <div class="layui-input-inline"  style="width: 200px" >
                        <select id="village" name="village" lay-search lay-filter="village">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label" style="width:200px">耕地面积（亩）</label>
                    <div class="layui-input-inline" style="width:200px">
                        <input type="text" name="plough_area"
                               lay-verify="number|required|positive"
                               id="plough_area"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label" style="width:200px">距离村委会公里数（公里）</label>
                    <div class="layui-input-inline" style="width:200px">
                        <input type="text" name="apart_from_village"
                               lay-verify="number|required|positive"
                               id="apart_from_village"
                               class="layui-input">
                    </div>
                </div>
            </div>

            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label" style="width:200px">平均海拔（米）</label>
                    <div class="layui-input-inline" style="width:200px">
                        <input type="text" name="elevation"
                               lay-verify="number|required|positive"
                               id="elevation"
                               class="layui-input">
                    </div>
                </div>
                <div class="layui-inline">
                    <label class="layui-form-label" style="width:200px">草场面积（亩）</label>
                    <div class="layui-input-inline" style="width:200px">
                        <input type="text" name="meadow_area"
                               lay-verify="number|required|positive"
                               id="meadow_area"
                               class="layui-input">
                    </div>
                </div>
            </div>
        </div>


        <div class="layui-upload">
            <button type="button" class="layui-btn layui-btn-normal"
                    id="ucpVillagePhotoList_btn">
                选择该自然村图片
            </button>

            <div class="layui-upload-list" style="max-width: 1000px;">
                <table class="layui-table">
                    <colgroup>
                        <col>
                        <col width="150">
                        <col width="260">
                        <col width="150">
                    </colgroup>
                    <thead>
                    <tr><th style="text-align:center">图像</th>
                        <th style="text-align:center">大小</th>
                        <th style="text-align:center">上传进度</th>
                        <th style="text-align:center">操作</th>
                    </tr></thead>
                    <tbody id="ucpVillagePhotoList"></tbody>
                </table>
            </div>

            <%--隐藏域：接受自然村编号，以便子页面获取--%>
            <input  type="hidden" class="layui-input" style="display:none"
                    name="ucp_village_id" id="ucp_village_id"/>

        </div>


        <button type="submit" class="layui-btn" style="display:none"
                id="addAnUCPVillageSubmit" lay-submit
                lay-filter="addAnUCPVillageSubmit"></button>

        <button type="reset" class="layui-btn" style="display:none"
                id="addAnUCPVillageReset"
                lay-filter="addAnUCPVillageReset"></button>

    </form>

</div>

<script type="text/javascript">

    layui.use(['laydate','form','common','table','upload','element'], function() {
        var laydate = layui.laydate;
        var form = layui.form;
        var $ = layui.jquery;
        var index = parent.layer.getFrameIndex(window.name);
        var common = layui.common;
        var upload = layui.upload;
        var element = layui.element;

        //所属村庄id
        var village_id;
        //给上传用的ucp_village_id
        var ucp_village_id;

        $(function () {
            //生成此次使用的ucp_village_id
            $.post('systemServlet?action=createID'
                ,{}
                ,function (data,status,xhr) {
                    $("#ucp_village_id:hidden").val(data);
                }
                ,'json')
        })

        //获取数据库村庄数据并完成下拉框绑定
        bindVillageSelectData();

        form.render();

        //非负数据验证
        form.verify({
            positive: [
                /([1-9]\d*\.?\d*)|(0\.\d*)|0/
                , '只能输入正数'
            ]
        });

        var choosePhotoCount = 0;
        //上传前定义上传文件数量
        var photoCount = 0;
        //图片已经上传成功的数量
        var photoUploadCount = 0;
        //多图片上传
        var upload_photo = upload.render({
            elem: '#ucpVillagePhotoList_btn'
            ,elemList: $('#ucpVillagePhotoList')
            ,url: 'baseInfoServlet?action=bindUCPVillagePhotoByID'
            ,data:{
                ucp_village_id:function () {
                    return $("#ucp_village_id:hidden").val();
                },
                ucp_village_name:function () {
                    return $("#ucp_village_name").val();
                },
            }
            ,method: 'post'
            ,accept: 'images'
            ,multiple: true
            ,number: 5
            ,auto: false
            ,bindAction: '#addAnUCPVillageSubmit'
            ,choose: function(obj){
                var that = this;
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function(index, file, result){
                    //选择图片：图片数量+1
                    photoCount++;
                    var tr = $(['<tr id="upload-'+ index +'">'
                       /* ,'<td>'+ file.name +'</td>'*/
                        ,'<td style="text-align:center"> <img class="layui-upload-img" src="'+result+'"> </td>'
                        ,'<td style="text-align:center">'+ (file.size/1014).toFixed(1) +'kb</td>'
                        ,'<td style="text-align:center">' +
                            '<div class="layui-progress" lay-filter="progress-demo-'+ index +'">' +
                            '<div class="layui-progress-bar" lay-percent=""></div></div>' +
                        '</td>'
                        ,'<td style="text-align:center">'
                            ,'<button class="layui-btn layui-btn-xs layui-hide demo-reload">重传</button>'
                            ,'<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                        ,'</td>'
                        ,'</tr>'].join(''));

                    //单个重传
                    tr.find('.demo-reload').on('click', function(){
                        obj.upload(index, file);
                    });

                    //删除
                    tr.find('.demo-delete').on('click', function(){
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        //删除图片：图片数量-1
                        photoCount--;
                        upload_photo.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    that.elemList.append(tr);
                    choosePhotoCount+=1;
                    element.render('progress'); //渲染新加的进度条组件
                });
            }
            ,done: function(res, index, upload){ //成功的回调
                var that = this;
                if(res.code == 1){
                    //上传成功
                    var tr = that.elemList.find('tr#upload-'+ index)
                        ,tds = tr.children();
                    tds.eq(3).html(''); //清空操作
                    photoUploadCount++;
                    delete this.files[index]; //删除文件队列已经上传成功的文件
                    return;
                }
                //否则为错误，则调用.error中的方法
                this.error(index, upload,res.msg);
            }
            ,allDone: function(obj){
                //多文件上传完毕后的状态回调
                if(photoUploadCount == photoCount){
                    //全部成功
                    //重载表格
                    parent.layui.table.reload('UCPVillageinfo', {
                        url: 'baseInfoServlet?action=queryAllUCPVillageInfo'
                        ,page: {
                            curr: 1
                        }
                        ,request: {
                            pageName: 'curr'
                            ,limitName: 'nums'
                        }
                    });
                    //关闭此页面
                    parent.layer.close(index);
                }
            }
            ,error: function(index, upload,msg){ //错误回调
                var that = this;
                var tr = that.elemList.find('tr#upload-'+ index)
                    ,tds = tr.children();
                parent.layer.msg("上传失败:"+msg, {
                    icon : 5
                });
                tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
            }
            ,progress: function(n, elem, e, index){
                //注意：index 参数为 layui 2.6.6 新增
                element.progress('progress-demo-'+ index, n + '%'); //执行进度条。n 即为返回的进度百分比
            }
        });

        form.on('select(village)', function(data)   {
            //选中后为所属乡镇id赋值
            village_id = data.value;
        });

        form.on('submit(addAnUCPVillageSubmit)', function(data){
            const sourceData = data.field;

            //自然村名称
            const ucp_village_name = sourceData.ucp_village_name;
            //距离村委会公里数（公里）
            const apart_from_village = sourceData.apart_from_village;
            //耕地面积（亩）
            const plough_area = sourceData.plough_area;
            //平均海拔（米）
            const elevation = sourceData.elevation;
            //草场面积（亩）
            const meadow_area = sourceData.meadow_area;

            $.ajax({
                type : 'POST',
                url : 'baseInfoServlet?action=addAnUCPVillage',
                data : {
                    ucp_village_id:$("#ucp_village_id:hidden").val(),
                    ucp_village_name:ucp_village_name,
                    village_id:village_id,
                    apart_from_village:apart_from_village,
                    plough_area:plough_area,
                    elevation:elevation,
                    meadow_area:meadow_area
                },
                dataType : 'json',
                success : function(data) {
                   if(data.statusCode != 1){
                        parent.layer.msg(data.msg, {
                            icon : 5
                        });
                    }
                   else {
                        // 基础信息插入成功
                        parent.layer.msg('基础信息添加成功', {
                            icon : 6,
                        });

                        if(choosePhotoCount>0){
                            //选择了图片
                            upload_photo.reload();
                        }else {
                            //重载表格
                            parent.layui.table.reload('UCPVillageinfo', {
                                url: 'baseInfoServlet?action=queryAllUCPVillageInfo'
                                ,page: {
                                    curr: 1
                                }
                                ,request: {
                                    pageName: 'curr'
                                    ,limitName: 'nums'
                                }
                            });
                            //关闭此页面
                            parent.layer.close(index);
                        }

                    }
                },
                error : function(data) {
                    // 异常提示
                    parent.layer.msg('出现网络故障', {
                        icon : 5
                    });
                    //关闭此页面
                    parent.layer.close(index);
                }
            });
            return false;
        });

    });
</script>
</body>
</html>
