<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%--更新自然村图片信息--%>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>
<div style="padding: 10px">

    <%--图片信息--%>
    <div class="layui-row">
        <div class="layui-col-md12">
            <div class="layui-bg-gray" style="padding: 10px;">
                <div class="layui-row">
                    <div class="layui-col-md12">
                        <div class="layui-card">
                            <div class="layui-card-header layui-bg-blue" id="ucp_village_name_div"></div>
                            <div class="layui-card-body">
                              <%--数据展示--%>
                              <table class="layui-hide" id="ucp_village_picture_info" lay-filter="ucp_village_picture_info"></table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <form class="layui-form layui-form-pane">
        <div class="layui-upload">

            <button type="button"
                    class="layui-btn layui-btn-sm layui-btn-normal<%-- layui-btn-radius--%>"
                    id="ucpVillagePhotoList_btn">
                <i class="layui-icon">&#xe64a;  <span>选择新图片(最多选择5张图片)</span></i>
            </button>

            <button type="button"
                    class="layui-btn layui-btn-sm layui-btn-normal <%--layui-btn-radius--%>"
                    id="addUCPVillagePhotoSubmit">
                <i class="layui-icon">&#xe624;  <span>开始上传</span></i>

            </button>

            <div class="layui-upload-list" style="max-width: 1000px;">
                <table class="layui-table">
                    <colgroup>
                        <col width="450">
                        <col width="150">
                        <col width="350">
                        <col width="150">
                    </colgroup>
                    <thead>
                        <tr><th style="text-align:center">图像</th>
                            <th style="text-align:center">大小</th>
                            <th style="text-align:center">上传进度</th>
                            <th style="text-align:center">操作</th>
                        </tr>
                    </thead>
                    <tbody id="ucpVillagePhotoList"></tbody>
                </table>
            </div>
        </div>


        <button type="submit" class="layui-btn" style="display:none"
                id="addAnUCPVillageSubmit" lay-submit
                lay-filter="addAnUCPVillageSubmit"></button>

        <button type="reset" class="layui-btn" style="display:none"
                id="addAnUCPVillageReset"
                lay-filter="addAnUCPVillageReset"></button>

    </form>

    <script id="show_ucp_village_name" type="text/html">
        <span>自然村名称：{{=d.ucp_village_name }}</span>
    </script>

    <script type="text/html" id="baseInfo">
        <a class="layui-btn layui-btn-danger layui-btn-xs"
           lay-event="delete" style="margin-top:2px">
            删除
        </a>
    </script>

</div>

<script type="text/javascript">

    layui.use(['laydate','form','common','table','upload','element','laytpl'], function() {
        var laydate = layui.laydate;
        var form = layui.form;
        var $ = layui.jquery;
        var index = parent.layer.getFrameIndex(window.name);
        var common = layui.common;
        var upload = layui.upload;
        var element = layui.element;
        var table = layui.table;
        var laytpl = layui.laytpl;

        //初始化自然村图片表格
        var ucp_village_picture_info_table =table.render({
            elem: '#ucp_village_picture_info'
            ,url:'baseInfoServlet?action=queryUCPVillagePhoto'
            ,title: '自然村图片信息表'
            ,where: {
                ucp_village_id: parent.$("#ucp_village_id:hidden").val()
            }
            ,cols: [[
                {field:'photoID', title:'图片编号',width:100,align:"center"}
                ,{field:'photo_name', title:'图片名称',width:450,align:"center"}
                ,{field:'UCPVillage_picture_src'
                    ,templet:
                        '<div> ' +
                            '<img class="layui-upload-img" ' +
                                'src="/image{{=d.UCPVillage_picture_src}}"' +
                                'height="28px" width="50px" '+
                                'onclick="showPicture(this.src)"'+
                        '>'+
                        '</div>',
                    title:'图片信息',align:"center"}
                ,{fixed: 'right', title:'操作',align:"center", width:100,toolbar: '#baseInfo'}
            ]]
            ,parseData: function(res) {
                //将本次查询的数据赋值给导出数据指定的变量
                return {
                    "code": res.code, //解析接口状态
                    "msg": res.msg, //解析提示文本
                    "count": res.count.length, //解析数据长度
                    "data": res.data //解析数据列表
                };
            }
            ,done:function (res, curr, count) {
                //只要有图像，最少有1个，所有取第一张图片的ucp_village_name即可
                var data = res.data[0];
                var getTpl = $("#show_ucp_village_name").html()
                    ,view = document.getElementById('ucp_village_name_div');
                laytpl(getTpl).render(data, function(html){
                    view.innerHTML = html;
                });
            }
        });

        /*设定行工具事件*/
        table.on('tool(ucp_village_picture_info)', function(obj){
            //获得当前行数据
            var data = obj.data;

            //获得 lay-event 对应的值（也可以是表头的 event 参数对应的值）
            var layEvent = obj.event;
            //获取当前页码
            var currentPage = $(".layui-laypage-skip .layui-input").val();

            //删除此图片
            if(layEvent === 'delete'){
                var deleteLayer = parent.layer.confirm('确定删除该图片吗？', function(index){
                    //关闭confirm框
                    parent.layer.close(index);
                    $.ajax({
                        type : 'POST',
                        url : 'baseInfoServlet?action=deleteUCPVillagePhotoByUCPVillageIdAndPhotoId',
                        data : {
                            photoID:data.photoID,
                            ucp_village_id:data.ucp_village_id
                        },
                        dataType : 'json',
                        success : function(data) {
                            if(data.code < 1){
                                parent.layer.msg("删除失败："+data.msg, {
                                    icon : 5
                                });
                            }
                            else {
                                //前端删除表中的这一行
                                obj.del();
                                parent.layer.msg('已删除', {
                                    icon : 6,
                                });
                                //重载表格
                                parent.table.reload('ucp_village_picture_info', {
                                    url:'baseInfoServlet?action=queryUCPVillagePhoto'
                                    ,title: '自然村图片信息表'
                                    ,where: {
                                        ucp_village_id:data.ucp_village_id
                                    }
                                    ,page: {
                                        curr: currentPage //从数据发生所在页开始
                                    }
                                    ,request: {
                                        pageName: 'curr'
                                        ,limitName: 'nums'
                                    }
                                });
                            }
                        },
                        error : function(data) {
                            // 异常提示
                            parent.layer.msg('出现网络故障', {
                                icon : 5
                            });
                        }
                    });
                });
            }

        });

        //多图片上传
        //上传前定义上传文件数量
        var photoCount = 0;
        //图片已经上传成功的数量
        var photoUploadCount = 0;
        var upload_photo = upload.render({
            elem: '#ucpVillagePhotoList_btn'
            ,elemList: $('#ucpVillagePhotoList')
            ,url: 'baseInfoServlet?action=bindUCPVillagePhotoByID'
            ,data:{
                ucp_village_id:function () {
                    return parent.$("#ucp_village_id:hidden").val();
                }
            }
            ,method: 'post'
            ,accept: 'images'
            ,multiple: true
            ,number: 5
            ,auto: false
            ,bindAction: '#addUCPVillagePhotoSubmit'
            ,choose: function(obj){
                var that = this;
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function(index, file, result){
                    //选择图片：图片数量+1
                    photoCount++;

                    var tr = $(['<tr id="upload-'+ index +'">'
                        ,'<td style="text-align:center"> <img class="layui-upload-img" src="'+result+'"> </td>'
                        ,'<td style="text-align:center">'+ (file.size/1014).toFixed(1) +'kb</td>'
                        ,'<td style="text-align:center">' +
                            '<div class="layui-progress" lay-filter="progress-demo-'+ index +'">' +
                            '<div class="layui-progress-bar" lay-percent=""></div></div>' +
                        '</td>'
                        ,'<td style="text-align:center">'
                            ,'<button class="layui-btn layui-btn-xs layui-hide demo-reload">重传</button>'
                            ,'<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                        ,'</td>'
                        ,'</tr>'].join(''));

                    //单个重传
                    tr.find('.demo-reload').on('click', function(){
                        obj.upload(index, file);
                    });

                    //删除
                    tr.find('.demo-delete').on('click', function(){
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        //删除图片：图片数量-1
                        photoCount--;
                        upload_photo.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    that.elemList.append(tr);
                    element.render('progress'); //渲染新加的进度条组件
                });
            }
            ,done: function(res, index, upload){ //成功的回调
                var that = this;
                if(res.code == 1){
                    //上传成功
                    var tr = that.elemList.find('tr#upload-'+ index)
                        ,tds = tr.children();
                    tds.eq(3).html('<span>上传成功</span>'); //清空操作
                    photoUploadCount++;
                    /*delete this.files[index]; //删除文件队列已经上传成功的文件*/
                    return;
                }
                //否则为错误，则调用.error中的方法
                this.error(index, upload,res.msg);
            }
            ,allDone: function(obj){
                if(photoUploadCount == photoCount){
                    //全部上传成功
                    parent.layer.msg("图片全部上传成功，共上传"+photoUploadCount+"张图片", {
                        icon : 6
                    });
                    //刷新本页面
                    self.location.reload();
                }
            }
            ,error: function(index, upload, msg){ //错误回调
                parent.layer.msg("上传失败:"+msg, {
                    icon : 5
                });
                var that = this;
                var tr = that.elemList.find('tr#upload-'+ index)
                    ,tds = tr.children();
                tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
            }
            ,progress: function(n, elem, e, index){
                //注意：index 参数为 layui 2.6.6 新增
                element.progress('progress-demo-'+ index, n + '%'); //执行进度条。n 即为返回的进度百分比
            }
        });

    });
</script>
</body>
</html>
