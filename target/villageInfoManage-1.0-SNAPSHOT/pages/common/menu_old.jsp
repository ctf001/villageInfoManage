<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layui-side layui-bg-black">
    <div class="layui-side-scroll">
        <ul class="layui-nav layui-nav-tree" lay-filter="test">

            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">基础信息</a>
                <dl class="layui-nav-child">
                    <dd><a href="pages/service/askForLeave/askForLeave.jsp">村基本信息</a></dd>
                    <dd><a href="pages/service/rest/rest.jsp">自然村信息</a></dd>
                    <dd><a href="pages/service/rest/rest.jsp">双联户信息</a></dd>
                    <dd><a href="pages/service/rest/rest.jsp">家庭信息</a></dd>
                    <dd><a href="pages/service/approvalLeave/approvalLeave.jsp">人员信息</a></dd>
                    <dd></dd>
                </dl>
            </li>

            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">拓展信息</a>
                <dl class="layui-nav-child">
                    <dd><a href="pages/service/askForLeave/askForLeave.jsp">党群团信息</a></dd>
                    <dd><a href="pages/service/rest/rest.jsp">国民经济数据维护</a></dd>
                    <dd><a href="pages/service/rest/rest.jsp">双联户信息</a></dd>
                    <dd><a href="pages/service/rest/rest.jsp">家庭信息</a></dd>
                    <dd><a href="pages/service/approvalLeave/approvalLeave.jsp">人员信息</a></dd>
                    <dd></dd>
                </dl>
            </li>

            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">合作社管理</a>
                <dl class="layui-nav-child">
                    <dd><a href="pages/service/askForLeave/askForLeave.jsp">合作社基本信息</a></dd>
                    <dd><a href="pages/service/rest/rest.jsp">社员信息</a></dd>
                    <dd><a href="pages/service/rest/rest.jsp">经营信息</a></dd>
                    <dd></dd>
                </dl>
            </li>

            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">业务办理</a>
                <dl class="layui-nav-child">
                    <dd><a href="pages/service/askForLeave/askForLeave.jsp">证明开具</a></dd>
                    <dd></dd>
                </dl>
            </li>

            <li class="layui-nav-item layui-nav-itemed">
                <a href="javascript:;">系统设置</a>
                <dl class="layui-nav-child">
                    <dd><a href="pages/service/systemInfo/systemInfo.jsp">业务参数</a></dd>
                    <dd><a href="pages/service/systemInfo/setWords.jsp">系统字段</a></dd>
                    <dd><a href="pages/service/systemInfo/accountInfo.jsp">账号管理</a></dd>
                </dl>
            </li>

        </ul>
    </div>
</div>

<script>

</script>