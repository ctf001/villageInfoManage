<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header">
        <div class="layui-logo layui-hide-xs layui-bg-black">夏麦村村级信息管理系统</div>
        <!-- 头部区域（可配合layui 已有的水平导航） -->
        <ul class="layui-nav layui-layout-left">
            <!-- 移动端显示 -->
            <li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm" lay-header-event="menuLeft">
                <i class="layui-icon layui-icon-spread-left"></i>
            </li>

            <li class="layui-nav-item layui-hide-xs"><a href="">测试功能点1</a></li>
            <li class="layui-nav-item layui-hide-xs"><a href="">测试功能点2</a></li>
            <li class="layui-nav-item">
                <a href="javascript:;">测试功能集1</a>
                <dl class="layui-nav-child">
                    <dd><a href="">测试功能点1</a></dd>
                    <dd><a href="">测试功能点2</a></dd>
                    <dd><a href="">测试功能点3</a></dd>
                </dl>
            </li>
        </ul>
        <ul class="layui-nav layui-layout-right">
            <li class="layui-nav-item layui-hide layui-show-md-inline-block">
                <a href="javascript:;">
                    <%--<img src="//tva1.sinaimg.cn/crop.0.0.118.118.180/5db11ff4gw1e77d3nqrv8j203b03cweg.jpg" class="layui-nav-img">--%>
                    ${sessionScope.user.operator}
                </a>
            </li>
            <li class="layui-nav-item" lay-header-event="menuRight" lay-unselect>
                <a class="demo1">
                    <i class="layui-icon layui-icon-more-vertical"></i>
                </a>
            </li>
        </ul>
    </div>
</div>

<script>
    layui.use(['dropdown', 'util', 'layer', 'table'], function() {
        var dropdown = layui.dropdown
            , util = layui.util
            , layer = layui.layer
            , table = layui.table
            , $ = layui.jquery;

        //初演示
        dropdown.render({
            elem: '.demo1'
            , data: [{
                title: '账户信息'
                , id: 100
            }, {
                title: '系统设置'
                , id: 101
            }, {
                title: '退出登录'
                , id: 102
            }]
            , click: function (obj) {
                switch (obj.title) {
                    case "退出登录":
                        window.location.href = "loginServlet?action=loginOut";
                    case "系统设置":
                        window.location.href = "#";
                    case "账户信息":
                        window.location.href = "#";
                }
            }
        });
    })
</script>