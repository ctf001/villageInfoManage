<%--
  Created by IntelliJ IDEA.
  User: tianfeichen
  Date: 2021/8/21
  Time: 16:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%--新增乡镇--%>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>
<div style="padding: 10px">

    <form class="layui-form layui-form-pane">

        <div class="layui-form-item">
            <label class="layui-form-label" style="width:150px">乡镇名称</label>
            <div class="layui-input-inline" style="width:400px">
                <input type="text" name="town_name"
                       autocomplete=“off”
                       lay-verify="required" id="town_name"
                       class="layui-input">
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-inline">
                <label class="layui-form-label" style="width: 150px">所属县区</label>
                <div class="layui-input-inline" style="width: 120px">
                    <select name="province" data-area="西藏自治区"
                            lay-verify="required"
                            lay-filter="province" lay-search>
                        <option value="">选择省</option>
                    </select>
                </div>
                <div class="layui-input-inline" style="width: 120px">
                    <select name="city" data-area="日喀则市"
                            lay-verify="required"
                            lay-filter="city" lay-search>
                        <option value="">选择市</option>
                    </select>
                </div>
                <div class="layui-input-inline" style="width: 120px">
                    <select name="district" data-area="谢通门县"
                            lay-filter="district"
                            lay-verify="required"
                            lay-search>
                        <option value="">选择区</option>
                    </select>
                </div>
            </div>
        </div>

        <button type="submit" class="layui-btn" style="display:none"
                id="addTownSubmit" lay-submit lay-filter="addTownSubmit"></button>
        <button type="reset" class="layui-btn" style="display:none"
                id="addTownReset" lay-submit lay-filter="addTownReset"></button>

    </form>
</div>

<script type="text/javascript">

    layui.use(['laydate','form','common','table'], function() {
        var laydate = layui.laydate;
        var form = layui.form;
        var $ = layui.jquery;
        var index = parent.layer.getFrameIndex(window.name);
        var common = layui.common;

        form.render();

        form.on('submit(addTownSubmit)', function(data){
            const sourceData = data.field;

            //解析解析框中的地址内容
            const city = sourceData.city;
            const district = sourceData.district;
            const province = sourceData.province;
            // 通过地址code码获取地址名称
            const address = common.getCity({
                province,
                city,
                district
            });
            const provinceName = address.provinceName;
            const cityName = address.cityName;
            const districtName = address.districtName;
            //解析解析框中的地址内容
            const county = provinceName + ' ' + cityName + ' ' + districtName;
            //乡镇名称
            const town_name = sourceData.town_name;

            $.ajax({
                type : 'POST',
                url : 'baseInfoServlet?action=addATown',
                data : {
                    town_name : town_name,
                    county : county
                },
                dataType : 'json',
                success : function(data) {
                    if(data.code != 1){
                        parent.layer.msg(data.msg, {
                            icon : 5
                        });
                    }
                    else {
                        // 插入成功
                        parent.layer.msg('添加成功', {
                            icon : 6,
                        });
                        //重载表格
                        parent.layui.table.reload('towninfo', {
                            url: 'baseInfoServlet?action=queryAllTownInfo'
                            ,page: {
                                curr: 1 //重新从第 1 页开始
                            }
                            ,request: {
                                pageName: 'curr'
                                ,limitName: 'nums'
                            }
                        });
                        //关闭此页面
                        parent.layer.close(index);
                    }
                },
                error : function(data) {
                    console.log(data);
                    // 异常提示
                    parent.layer.msg('出现网络故障', {
                        icon : 5
                    });
                    //关闭此页面
                    parent.layer.close(index);
                }
            });
            return false;
        });
    });
</script>
</body>
</html>
