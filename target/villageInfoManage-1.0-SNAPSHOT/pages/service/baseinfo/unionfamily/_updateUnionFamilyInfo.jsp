<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%--更新自然村信息--%>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>
<div style="padding: 10px">

    <form class="layui-form layui-form-pane">

        <div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">联户编号</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text"
                           readonly="readonly"
                           name="unionfamily_id"
                           id="unionfamily_id"
                           lay-verify="number|required|positive"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">联户单位性质</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text"
                           name="uf_attribute"
                           id="uf_attribute"
                           lay-verify="required"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 200px">所属村</label>
                <div class="layui-input-inline"  style="width: 350px" >
                    <select id="village" name="village" lay-search lay-filter="village">
                        <option value=""></option>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">双联户户长姓名</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text"
                           name="uf_leader_name"
                           id="uf_leader_name"
                           lay-verify="required"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">双联户户长身份证号</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text"
                           name="uf_leader_idcard_num"
                           id="uf_leader_idcard_num"
                           lay-verify="required|identity"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">双联户户长联系电话</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text"
                           name="uf_leader_phone_num"
                           id="uf_leader_phone_num"
                           lay-verify="required|phone"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">双联户户长银行卡号</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text"
                           name="uf_leader_bankcard_num"
                           id="uf_leader_bankcard_num"
                           lay-verify="required|number"
                           class="layui-input">
                </div>
            </div>

        </div>

        <button type="submit" class="layui-btn" style="display:none"
                id="updateUnionFamilySubmit" lay-submit
                lay-filter="updateUnionFamilySubmit"></button>
    </form>

</div>

<script type="text/javascript">

    layui.use(['laydate','form','common','table','upload','element'], function() {
        var laydate = layui.laydate;
        var form = layui.form;
        var $ = layui.jquery;
        var index = parent.layer.getFrameIndex(window.name);
        var common = layui.common;
        var upload = layui.upload;
        var element = layui.element;

        //所属村庄id
        var village_id = parent.$("#village_id:hidden").val();

        form.render();

        //非负数据验证
        form.verify({
            positive: [
                /([1-9]\d*\.?\d*)|(0\.\d*)|0/
                , '只能输入正数'
            ]
        });

        form.on('select(village)', function(data)   {
            //选中后为所属村赋值
            village_id = data.value;
        });

        form.on('submit(updateUnionFamilySubmit)', function(data){
            const sourceData = data.field;

            //双联户户长银行卡号
            const uf_leader_bankcard_num = sourceData.uf_leader_bankcard_num;
            //双联户户长联系方式
            const uf_leader_phone_num = sourceData.uf_leader_phone_num;
            //双联户户长身份证号
            const uf_leader_idcard_num = sourceData.uf_leader_idcard_num;
            //双联户户长姓名
            const uf_leader_name = sourceData.uf_leader_name;
            //联户单位性质
            const uf_attribute = sourceData.uf_attribute;
            //联户编号
            const unionfamily_id = sourceData.unionfamily_id;

            //从父页面获取数据发生页所在页码
            var currentPage = parent.$(".layui-laypage-skip .layui-input").val();

            $.ajax({
                type : 'POST',
                url : 'baseInfoServlet?action=updateUnionFamilyBaseInfo',
                data : {
                    unionfamily_id:unionfamily_id,
                    uf_attribute:uf_attribute,
                    village_id:village_id,
                    uf_leader_name:uf_leader_name,
                    uf_leader_idcard_num:uf_leader_idcard_num,
                    uf_leader_phone_num:uf_leader_phone_num,
                    uf_leader_bankcard_num:uf_leader_bankcard_num
                },
                dataType : 'json',
                success : function(data) {
                    if(data.statusCode != 1){
                        parent.layer.msg(data.msg, {
                            icon : 5
                        });
                    }
                    else {
                        // 基础信息插入成功
                        parent.layer.msg('基础信息更新成功', {
                            icon : 6,
                        });

                        //重载表格
                        parent.layui.table.reload('union_family_info', {
                            url: 'baseInfoServlet?action=queryAllUnionFamilyInfo'
                            ,page: {
                                curr: currentPage
                            }
                            ,request: {
                                pageName: 'curr'
                                ,limitName: 'nums'
                            }
                        });

                        //关闭此页面
                        parent.layer.close(index);
                    }
                },
                error : function(data) {
                    // 异常提示
                    parent.layer.msg('出现网络故障', {
                        icon : 5
                    });
                    //关闭此页面
                    parent.layer.close(index);
                }
            });
            return false;
        });


    });
</script>
</body>
</html>
