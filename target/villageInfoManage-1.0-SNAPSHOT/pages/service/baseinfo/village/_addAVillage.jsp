<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%--新增村庄--%>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>

    <form class="layui-form layui-form-pane">

    <div class="layui-form-item">
        <label class="layui-form-label" style="width:200px">村庄名称</label>
        <div class="layui-input-inline" style="width:350px">
            <input type="text" name="village_name"
                   lay-verify="required" id="village_name"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-inline">
            <label class="layui-form-label" style="width: 200px">所属乡</label>
            <div class="layui-input-inline"  style="width: 350px" >
                <select id="town" name="town" lay-search lay-filter="town">
                    <option value=""></option>
                </select>
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label" style="width:200px">距离乡政府公里数（公里）</label>
        <div class="layui-input-inline" style="width:350px">
            <input type="text" name="apart_from_town"
                   lay-verify="number|required|positive"
                   id="apart_from_town"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label" style="width:200px">耕地面积（亩）</label>
        <div class="layui-input-inline" style="width:350px">
            <input type="text" name="plough_area"
                   lay-verify="number|required|positive"
                   id="plough_area"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label" style="width:200px">平均海拔（米）</label>
        <div class="layui-input-inline" style="width:350px">
            <input type="text" name="elevation"
                   lay-verify="number|required|positive"
                   id="elevation"
                   class="layui-input">
        </div>
    </div>

    <div class="layui-form-item">
        <label class="layui-form-label" style="width:200px">草场面积（亩）</label>
        <div class="layui-input-inline" style="width:350px">
            <input type="text" name="meadow_area"
                   lay-verify="number|required|positive"
                   id="meadow_area"
                   class="layui-input">
        </div>
    </div>


    <button type="submit" class="layui-btn" style="display:none"
            id="addAVillageSubmit" lay-submit
            lay-filter="addAVillageSubmit"></button>

    <button type="reset" class="layui-btn" style="display:none"
            id="addAVillageReset" lay-submit
            lay-filter="addAVillageReset"></button>

</form>

    <script type="text/javascript">

        layui.use(['laydate','form','common','table'], function() {
            var laydate = layui.laydate;
            var form = layui.form;
            var $ = layui.jquery;
            var index = parent.layer.getFrameIndex(window.name);
            var common = layui.common;

            bindTownSelectData();

            form.render();

            //非负数据验证
            form.verify({
                positive: [
                    /([1-9]\d*\.?\d*)|(0\.\d*)|0/
                    , '只能输入正数'
                ]
            });

            //所属乡镇id
            var town_id;

            form.on('select(town)', function(data){
                //选中后为所属乡镇id赋值
                town_id = data.value;
            });

            form.on('submit(addAVillageSubmit)', function(data){
                const sourceData = data.field;

                //村庄名称
                const village_name = sourceData.village_name;
                //距离乡政府公里数（公里）
                const apart_from_town = sourceData.apart_from_town;
                //耕地面积（亩）
                const plough_area = sourceData.plough_area;
                //平均海拔（米）
                const elevation = sourceData.elevation;
                //草场面积（亩）
                const meadow_area = sourceData.meadow_area;

                $.ajax({
                    type : 'POST',
                    url : 'baseInfoServlet?action=addAVillage',
                    data : {
                        village_name:village_name,
                        town_id:town_id,
                        apart_from_town:apart_from_town,
                        plough_area:plough_area,
                        elevation:elevation,
                        meadow_area:meadow_area
                    },
                    dataType : 'json',
                    success : function(data) {
                        if(data.code != 1){
                            parent.layer.msg(data.msg, {
                                icon : 5
                            });
                        }
                        else {
                            // 插入成功
                            parent.layer.msg('添加成功', {
                                icon : 6,
                            });
                            //重载表格
                            parent.layui.table.reload('villageinfo', {
                                url: 'baseInfoServlet?action=queryAllVillageInfo'
                                ,page: {
                                    curr: 1
                                }
                                ,request: {
                                    pageName: 'curr'
                                    ,limitName: 'nums'
                                }
                            });
                            //关闭此页面
                            parent.layer.close(index);
                        }
                    },
                    error : function(data) {
                        console.log(data);
                        // 异常提示
                        parent.layer.msg('出现网络故障', {
                            icon : 5
                        });
                        //关闭此页面
                        parent.layer.close(index);
                    }
                });
                return false;
            });
        });
    </script>

</body>
</html>
