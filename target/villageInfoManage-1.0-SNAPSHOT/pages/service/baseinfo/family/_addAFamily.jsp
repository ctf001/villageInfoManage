<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%--新增一户--%>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>
<div style="padding: 10px">

    <form class="layui-form layui-form-pane">

        <div style="margin: auto">
            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label" style="width:200px">户号（户口本记录）</label>
                    <div class="layui-input-inline" style="width:200px">
                        <input type="text"
                               name="family_num"
                               id="family_num"
                               lay-verify="number|required|positive"
                               class="layui-input">
                    </div>
                </div>

                <div class="layui-inline">
                    <label class="layui-form-label" style="width:200px">户名</label>
                    <div class="layui-input-inline" style="width:200px">
                        <input type="text"
                               name="family_name"
                               id="family_name"
                               lay-verify="required"
                               class="layui-input">
                    </div>
                </div>
            </div>

            <div class="layui-form-item">

                <div class="layui-inline">
                    <label class="layui-form-label" style="width:200px">户别（家庭类型）</label>
                    <div class="layui-input-inline" style="width:200px">
                        <input type="text"
                               name="family_type"
                               id="family_type"
                               lay-verify="required"
                               class="layui-input">
                    </div>
                </div>

                <div class="layui-inline">
                    <label class="layui-form-label" style="width:200px">住址</label>
                    <div class="layui-input-inline" style="width:200px">
                        <input type="text"
                               name="address"
                               id="address"
                               lay-verify="required"
                               class="layui-input">
                    </div>
                </div>

            </div>

            <div class="layui-form-item">

                <div class="layui-inline">
                    <label class="layui-form-label" style="width: 200px">所属村</label>
                    <div class="layui-input-inline"  style="width: 200px" >
                        <select id="village" name="village"
                                lay-search lay-filter="village">
                            <option value=""></option>
                        </select>
                    </div>
                </div>

                <div class="layui-inline">
                    <label class="layui-form-label" style="width: 200px">所在自然村</label>
                    <div class="layui-input-inline"  style="width: 200px" >
                        <select id="ucp_village" name="ucp_village"
                                lay-search lay-filter="ucp_village">
                            <option value=""></option>
                        </select>
                    </div>
                </div>

            </div>

            <div class="layui-form-item">
                <div class="layui-inline">
                    <label class="layui-form-label" style="width: 200px">所属双联户</label>
                    <div class="layui-input-inline"  style="width: 200px" >
                        <select id="union_family" name="union_family"
                                lay-search lay-filter="union_family">
                            <option value=""></option>
                        </select>
                    </div>
                </div>
            </div>

        </div>

        <div class="layui-upload">
        <button type="button" class="layui-btn layui-btn-normal"
                id="FamilyPhotoList_btn">
            选择户长图片
        </button>
        <div class="layui-upload-list" style="max-width: 1000px;">
            <table class="layui-table">
                <colgroup>
                    <col>
                    <col width="150">
                    <col width="260">
                    <col width="150">
                </colgroup>
                <thead>
                <tr><th style="text-align:center">图像</th>
                    <th style="text-align:center">大小</th>
                    <th style="text-align:center">上传进度</th>
                    <th style="text-align:center">操作</th>
                </tr></thead>
                <tbody id="FamilyPhotoList"></tbody>
            </table>
        </div>
      </div>

        <button type="submit" class="layui-btn" style="display:none"
            id="addAFamilySubmit" lay-submit
            lay-filter="addAFamilySubmit"></button>

        <button type="reset" class="layui-btn" style="display:none"
            id="addAFamilyReset"
            lay-filter="addAFamilyReset"></button>

    </form>

</div>

<script type="text/javascript">

    layui.use(['laydate','form','common','table','upload','element'], function() {
        var laydate = layui.laydate;
        var form = layui.form;
        var $ = layui.jquery;
        var index = parent.layer.getFrameIndex(window.name);
        var common = layui.common;
        var upload = layui.upload;
        var element = layui.element;

        //所属村庄id
        var village_id;
        //所属自然村id
        var ucp_village_id;
        //所属双联户id
        var union_family_id;

        form.render();
        //获取数据库村庄数据并完成下拉框绑定
        bindVillageSelectData();

        //非负数据验证
        form.verify({
            positive: [
                /([1-9]\d*\.?\d*)|(0\.\d*)|0/
                , '只能输入正数'
            ]
        });

        //下拉框选择后为相关变量赋值
        form.on('select(village)', function(data){
            //选中后为所属村id赋值
            village_id = data.value;
<<<<<<< HEAD
            bindUnionFamilySelectDataByVillageID(village_id);
            bindUCPVillageSelectDataByVillageID(village_id);
=======

            //为自然村下拉框赋值
            $.ajax({
                url: 'baseInfoServlet?action=queryUCPVillageByVillageID',
                dataType: 'json',
                data: {
                    village_id: village_id
                },
                type: 'post',
                success: function (data) {
                    var validData = data.data;
                    $("#ucp_village").empty();
                    if (validData.length !== 0) {
                        $("#ucp_village").append("<option value=''>请选择</option>");
                        $.each(validData, function (index, item) {
                            $('#ucp_village').append(new Option(item.ucp_village_name,item.ucp_village_id));
                        });
                    } else {
                        $("#ucp_village").append(new Option("该行政村无自然村", ""));
                    }
                    //重新渲染
                    form.render("select");
                }
            })

            //为双联户下拉框赋值
            $.ajax({
                url: 'baseInfoServlet?action=queryUnionFamilesByVillageID',
                dataType: 'json',
                data: {
                    village_id: village_id
                },
                type: 'post',
                success: function (data) {
                    var validData = data.data;
                    $("#union_family").empty();
                    if (validData.length !== 0) {
                        $("#union_family").append("<option value=''>请选择</option>");
                        $.each(validData, function (index, item) {
                            var showName = "联户编号："+item.unionfamily_id + "||户长姓名：" + item.uf_leader_name;
                            $('#union_family').append(new Option(showName,item.unionfamily_id));

                        })
                    } else {
                        $("#union_family").append(new Option("该行政村无双联户", ""));
                    }
                    //重新渲染
                    form.render("select");
                }
            })
>>>>>>> origin/master
        });
        form.on('select(ucp_village)', function(data){
            //选中后为所属自然村id赋值
            ucp_village_id = data.value;
        });
        form.on('select(union_family)', function(data){
            //选中后为所属双联户id赋值
            union_family_id = data.value;
        });

        //上传前定义上传文件数量
        var photoCount = 0;
        //图片已经上传成功的数量
        var photoUploadCount = 0;

        var upload_photo = upload.render({
            elem: '#FamilyPhotoList_btn'
            ,elemList: $('#FamilyPhotoList')
            ,url: 'baseInfoServlet?action=bindFamilyRBPhotoByFamilyNum'
            ,data:{
                family_num:function () {
                    return $("#family_num").val();
                },
                family_name:function () {
                    return $("#family_name").val();
                }
            }
            ,method: 'post'
            ,accept: 'images'
            ,multiple: true
            ,number: 1
            ,auto: false
            ,bindAction: '#addAFamilySubmit'
            ,choose: function(obj){
                var that = this;
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function(index, file, result){
                    //选择图片：图片数量+1
                    photoCount++;
                    var tr = $(['<tr id="upload-'+ index +'">'
                        ,'<td style="text-align:center"> ' +
                        '   <img class="layui-upload-img" src="'+result+'"' +
                                'onclick="showPicture(this.src)"'+
                            '> ' +
                        '</td>'
                        ,'<td style="text-align:center">'+ (file.size/1014).toFixed(1) +'kb</td>'
                        ,'<td style="text-align:center">' +
                        '<div class="layui-progress" lay-filter="progress-demo-'+ index +'">' +
                        '<div class="layui-progress-bar" lay-percent=""></div></div>' +
                        '</td>'
                        ,'<td style="text-align:center">'
                        ,'<button class="layui-btn layui-btn-xs layui-hide demo-reload">重传</button>'
                        ,'<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                        ,'</td>'
                        ,'</tr>'].join(''));

                    //单个重传
                    tr.find('.demo-reload').on('click', function(){
                        obj.upload(index, file);
                    });

                    //删除
                    tr.find('.demo-delete').on('click', function(){
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        //删除图片：图片数量-1
                        photoCount--;
                        upload_photo.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    that.elemList.append(tr);
                    element.render('progress'); //渲染新加的进度条组件
                });
            }
            ,done: function(res, index, upload){ //成功的回调
                var that = this;
                if(res.code == 1){
                    //上传成功
                    var tr = that.elemList.find('tr#upload-'+ index)
                        ,tds = tr.children();
                    tds.eq(3).html(''); //清空操作
                    photoUploadCount++;
                    delete this.files[index]; //删除文件队列已经上传成功的文件
                    return;
                }
                //否则为错误，则调用.error中的方法
                this.error(index, upload,res.msg);
            }
            ,allDone: function(obj){
                //多文件上传完毕后的状态回调
                if(photoUploadCount == photoCount){
                    //全部成功
                    //重载表格
                    parent.layui.table.reload('family_info', {
                        url: 'baseInfoServlet?action=queryAllFamilyInfo'
                        ,page: {
                            curr: 1
                        }
                        ,request: {
                            pageName: 'curr'
                            ,limitName: 'nums'
                        }
                    });
                    //关闭此页面
                    parent.layer.close(index);
                }
            }
            ,error: function(index, upload,msg){ //错误回调
                var that = this;
                var tr = that.elemList.find('tr#upload-'+ index)
                    ,tds = tr.children();
                parent.layer.msg("上传失败:"+msg, {
                    icon : 5
                });
                tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
            }
            ,progress: function(n, elem, e, index){
                //注意：index 参数为 layui 2.6.6 新增
                element.progress('progress-demo-'+ index, n + '%'); //执行进度条。n 即为返回的进度百分比
            }
        });

        form.on('submit(addAFamilySubmit)', function(data){
            const sourceData = data.field;

            //户号（户口本记录）
            var family_num = sourceData.family_num;
            //户名
            var family_name = sourceData.family_name;
            //户别（家庭类型）
            var family_type = sourceData.family_type;
            //住址
            var address = sourceData.address


            $.ajax({
                type : 'POST',
                url : 'baseInfoServlet?action=addAFamily',
                data : {
                    family_num:family_num,
                    family_name:family_name,
                    family_type:family_type,
                    address:address,
                    unionfamily_id:union_family_id,
                    village_id:village_id,
                    ucp_village_id:ucp_village_id,
                },
                dataType : 'json',
                success : function(data) {
                   if(data.statusCode != 1){
                        parent.layer.msg(data.msg, {
                            icon : 5
                        });
                    }
                   else {
                        // 基础信息插入成功
                        parent.layer.msg('基础信息添加成功', {
                            icon : 6,
                        });

                        if(photoCount>0){
                            //选择了图片
                            upload_photo.reload();
                        }else {
                            //重载表格
                            parent.layui.table.reload('family_info', {
                                url: 'baseInfoServlet?action=queryAllFamilyInfo'
                                ,page: {
                                    curr: 1
                                }
                                ,request: {
                                    pageName: 'curr'
                                    ,limitName: 'nums'
                                }
                            });
                            //关闭此页面
                            parent.layer.close(index);
                        }

                    }
                },
                error : function(data) {
                    // 异常提示
                    parent.layer.msg('出现网络故障', {
                        icon : 5
                    });
                    //关闭此页面
                    parent.layer.close(index);
                }
            });
            return false;
        });

    });
</script>
</body>
</html>
