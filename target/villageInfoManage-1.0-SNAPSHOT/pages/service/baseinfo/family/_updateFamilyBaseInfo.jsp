<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<%--更新家庭基本信息--%>
<head>
    <%--基础引入--%>
    <%@include file="/pages/common/baseinfo.jsp"%>
</head>
<body>
<div style="padding: 10px">

    <form class="layui-form layui-form-pane">

        <div>
            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">户号（户口本记录）</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text"
                           name="family_num"
                           id="family_num"
                           lay-verify="number|required|positive"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">户名</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text"
                           name="family_name"
                           id="family_name"
                           lay-verify="required"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">户别（家庭类型）</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text"
                           name="family_type"
                           id="family_type"
                           lay-verify="required"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width:200px">住址</label>
                <div class="layui-input-inline" style="width:350px">
                    <input type="text"
                           name="address"
                           id="address"
                           lay-verify="required"
                           class="layui-input">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 200px">所属村</label>
                <div class="layui-input-inline"  style="width: 350px" >
                    <select id="village" name="village"
                            lay-search lay-filter="village">
                        <option value=""></option>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 200px">所在自然村</label>
                <div class="layui-input-inline"  style="width: 350px" >
                    <select id="ucp_village" name="ucp_village"
                            lay-search lay-filter="ucp_village">
                        <option value=""></option>
                    </select>
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label" style="width: 200px">所属双联户</label>
                <div class="layui-input-inline"  style="width: 350px" >
                    <select id="union_family" name="union_family"
                            lay-search lay-filter="union_family">
                        <option value=""></option>
                    </select>
                </div>
            </div>
        </div>

        <button type="submit" class="layui-btn" style="display:none"
                id="updateFamilySubmit" lay-submit
                lay-filter="updateFamilySubmit"></button>
    </form>

</div>

<script type="text/javascript">

    layui.use(['laydate','form','common','table','upload','element'], function() {
        var laydate = layui.laydate;
        var form = layui.form;
        var $ = layui.jquery;
        var index = parent.layer.getFrameIndex(window.name);
        var common = layui.common;
        var upload = layui.upload;
        var element = layui.element;

        //初始化所属村庄id
        var village_id = parent.$("#village_id:hidden").val();
        //初始化所属自然村id
        var ucp_village_id = parent.$("#ucp_village_id:hidden").val();
        //初始化所属双联户id
        var union_family_id = parent.$("#unionfamily_id:hidden").val();

        form.render();

        //非负数据验证
        form.verify({
            positive: [
                /([1-9]\d*\.?\d*)|(0\.\d*)|0/
                , '只能输入正数'
            ]
        });

        //下拉框选择后为相关变量赋值
        form.on('select(village)', function(selectdata){
<<<<<<< HEAD
            //选中后为所属村id赋值
            village_id = selectdata.value;
            bindUnionFamilySelectDataByVillageID(village_id);
            bindUCPVillageSelectDataByVillageID(village_id);
=======

            //选中后为所属村id赋值
            village_id = selectdata.value;

            //为自然村下拉框赋值
            $.ajax({
                url: 'baseInfoServlet?action=queryUCPVillageByVillageID',
                dataType: 'json',
                data: {
                    village_id: village_id
                },
                type: 'post',
                success: function (data) {
                    var validData = data.data;
                    $("#ucp_village").empty();
                    if (validData.length !== 0) {
                        $("#ucp_village").append("<option value=''>请选择</option>");
                        $.each(validData, function (index, item) {
                            $('#ucp_village').append(new Option(item.ucp_village_name,item.ucp_village_id));
                        });
                    } else {
                        $("#ucp_village").append(new Option("该行政村无自然村", ""));
                    }
                    //重新渲染
                    form.render("select");
                }
            })

            //为双联户下拉框赋值
            $.ajax({
                url: 'baseInfoServlet?action=queryUnionFamilesByVillageID',
                dataType: 'json',
                data: {
                    village_id: village_id
                },
                type: 'post',
                success: function (data) {
                    var validData = data.data;
                    $("#union_family").empty();
                    if (validData.length !== 0) {
                        $("#union_family").append("<option value=''>请选择</option>");
                        $.each(validData, function (index, item) {
                            var showName = "联户编号："+item.unionfamily_id + "||户长姓名：" + item.uf_leader_name;
                            $('#union_family').append(new Option(showName,item.unionfamily_id));
                        })
                    } else {
                        $("#union_family").append(new Option("该行政村无双联户", ""));
                    }
                    //重新渲染
                    form.render("select");
                }
            })
>>>>>>> origin/master
        });
        form.on('select(ucp_village)', function(selectdata){
            //选中后为所属自然村id赋值
            ucp_village_id = selectdata.value;
<<<<<<< HEAD
        });
        form.on('select(union_family)', function(selectdata){
            //选中后为所属双联户id赋值
            union_family_id = selectdata.value;
=======
>>>>>>> origin/master
        });
        form.on('select(union_family)', function(selectdata){
            //选中后为所属双联户id赋值
            union_family_id = selectdata.value;
        });


        form.on('submit(updateFamilySubmit)', function(data){
            const sourceData = data.field;

            //数据域
            //户号（户口本记录）
            var family_num = sourceData.family_num;
            var family_name = sourceData.family_name;
            var family_type = sourceData.family_type;
            var address = sourceData.address;
            var village_id = sourceData.village;
            var ucp_village_id = sourceData.ucp_village;
            var union_family_id = sourceData.union_family;
            var family_id = parent.$("#family_id:hidden").val();
<<<<<<< HEAD
=======

>>>>>>> origin/master

            //从父页面获取数据发生页所在页码
            var currentPage = parent.$(".layui-laypage-skip .layui-input").val();

            $.ajax({
                type : 'POST',
                url : 'baseInfoServlet?action=updateFamilyBaseInfo',
                data : {
                    village_id : village_id,
                    ucp_village_id : ucp_village_id,
                    unionfamily_id : union_family_id,
                    family_num : family_num,
                    family_name : family_name,
                    family_type : family_type,
                    address : address,
<<<<<<< HEAD
                    family_id : family_id
=======
                    family_id:family_id
>>>>>>> origin/master
                },
                dataType : 'json',
                success : function(data) {
                    if(data.statusCode != 1){
                        parent.layer.msg(data.msg, {
                            icon : 5
                        });
                    }
                    else {
                        // 基础信息插入成功
                        parent.layer.msg('基础信息更新成功', {
                            icon : 6,
                        });

                        //重载表格
<<<<<<< HEAD
                        parent.layui.table.reload('family_info', {
=======
                        parent.layui.table.reload('family_info_table', {
>>>>>>> origin/master
                            url: 'baseInfoServlet?action=queryAllFamilyInfo'
                            ,page: {
                                curr: currentPage
                            }
                            ,request: {
                                pageName: 'curr'
                                ,limitName: 'nums'
                            }
                        });

                        //关闭此页面
                        parent.layer.close(index);
                    }
                },
                error : function(data) {
                    // 异常提示
                    parent.layer.msg('出现网络故障', {
                        icon : 5
                    });
                    //关闭此页面
                    parent.layer.close(index);
                }
            });
            return false;
        });


    });
</script>
</body>
</html>
